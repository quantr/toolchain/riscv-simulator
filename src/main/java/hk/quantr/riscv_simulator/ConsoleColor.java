package hk.quantr.riscv_simulator;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ConsoleColor {

	public static String reset = "\u001B[0m";
	public static String red = "\033[31;1m";
	public static String green = "\033[32;1m";
	public static String yellow = "\033[33;1m";
	public static String blue = "\033[34;1m";
	public static String purple = "\033[35;1m";
	public static String cyan = "\033[36;1m";
}
