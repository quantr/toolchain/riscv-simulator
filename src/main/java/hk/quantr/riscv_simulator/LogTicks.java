package hk.quantr.riscv_simulator;

import java.util.ArrayList;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class LogTicks {

	public static ArrayList<Pair<Integer, Integer>> data = new ArrayList();

	public static boolean check(long cpuTick) {
		if (data.isEmpty()) {
			return true;
		}
		for (Pair<Integer, Integer> pair : data) {
			if (pair.getLeft() <= cpuTick && cpuTick < pair.getRight()) {
				return true;
			}
		}
		return false;
	}
}
