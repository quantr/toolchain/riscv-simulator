//package hk.quantr.riscv_simulator;
//
//import hk.quantr.assembler.RISCVDisassembler;
//import hk.quantr.assembler.exception.NoOfByteException;
//import java.util.HashMap;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
//public class Cache {
//
//	static HashMap<Integer, Integer> cacheGetNoOfByte = new HashMap();
//	static HashMap<Integer, String> cacheType = new HashMap();
//
//	public static int getNoOfByte(int b) throws NoOfByteException {
//		Integer x = cacheGetNoOfByte.get(b);
//		if (x == null) {
//			int temp = RISCVDisassembler.getNoOfByte(b);
//			cacheGetNoOfByte.put(b, temp);
//			return temp;
//		} else {
//			return x;
//		}
//	}
//
//	public static String getType(int b) {
//		String x = cacheType.get(b);
//		if (x == null) {
//			String temp = RISCVDisassembler.getType(b);
//			cacheType.put(b, temp);
//			return temp;
//		} else {
//			return x;
//		}
//	}
//
//}
