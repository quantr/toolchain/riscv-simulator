package hk.quantr.riscv_simulator;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import hk.quantr.javalib.CommonLib;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class LongConverter implements Converter {

	@Override
	public boolean canConvert(Class arg0) {
		return Long.class == arg0;
	}

	@Override
	public void marshal(Object obj, HierarchicalStreamWriter writer, MarshallingContext context) {
		writer.setValue("0x" + Long.toHexString((Long) obj));
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		return CommonLib.string2long(reader.getValue());
	}
}
