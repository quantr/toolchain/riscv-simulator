package hk.quantr.riscv_simulator.log;

import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.javalib.CommonLib;
import hk.quantr.riscv_simulator.LogTicks;
import hk.quantr.riscv_simulator.Simulator;
import hk.quantr.riscv_simulator.cpu.Register;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class FileLogThread implements LogInterface {

	File file;
	FileOutputStream fo;
	final LinkedList<Object[]> data = new LinkedList();
	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	final Object sharedLock = new Object();
	boolean stopped;

	public FileLogThread(String filename) {
		try {
			file = new File(filename);
			fo = new FileOutputStream(file);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(FileLogThread.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void run() {
		while (!stopped) {
			synchronized (sharedLock) {
				try {
					for (int z = 0; z < data.size() && z < 100000; z++) {
						Object[] temp;
						synchronized (data) {
							temp = data.pollLast();
						}
						for (int x = 0; x < temp.length; x++) {
							if (temp[x] instanceof BigInteger) {
								fo.write(CommonLib.getByteArray(((BigInteger) temp[x]).longValue()));
							}
						}
					}
				} catch (IOException ex) {
					Logger.getLogger(FileLogThread.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException ex) {
				Logger.getLogger(FileLogThread.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	@Override
	public void add(ArrayList<Dwarf> dwarfArrayList, long pc, long cpuTick, String code, String ins, boolean mem, String memOperation, boolean memRead, long memAddr, long memValue, int memSize, int priv, LinkedHashMap<String, Register> registers) throws SQLException {
		if (!LogTicks.check(cpuTick)) {
			return;
		}
		Object[] temp = new Object[registers.size() + 4];
		temp[0] = code;
		temp[1] = ins;
		temp[2] = "";
		temp[3] = priv;
		int x = 4;
		for (String registerName : registers.keySet()) {
			temp[x++] = registers.get(registerName).getValue();
		}

		synchronized (data) {
			data.push(temp);
		}
	}

	@Override
	public void stop() {
		synchronized (sharedLock) {
			stopped = true;
			try {
				System.out.println(sdf.format(new Date()) + " log to file stopping, remaining " + data.size() + " records");
				while (!data.isEmpty()) {
					Object[] temp = data.pop();
					fo.write("123".getBytes());
				}
				fo.close();
				System.out.println(sdf.format(new Date()) + " log to file stopped");
			} catch (IOException ex) {
				Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
}
