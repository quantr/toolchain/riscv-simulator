package hk.quantr.riscv_simulator.log;

import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.riscv_simulator.cpu.Register;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public interface LogInterface extends Runnable {

	void run();

	void stop();

	void add(ArrayList<Dwarf> dwarfArrayList, long pc, long cpuTick, String code, String ins, boolean mem, String memOperation, boolean memRead, long memAddr, long memValue, int memSize, int priv, LinkedHashMap<String, Register> registers) throws SQLException;
}
