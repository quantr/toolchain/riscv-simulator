package hk.quantr.riscv_simulator.log;

import hk.quantr.dwarf.QuantrDwarf;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.helper.VariableWithAddress;
import hk.quantr.riscv_simulator.LogTicks;
import hk.quantr.riscv_simulator.Simulator;
import hk.quantr.riscv_simulator.cpu.Register;
import java.lang.management.ManagementFactory;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class H2Thread implements LogInterface {

	Connection conn;
	String connectionString;
	PreparedStatement stmt;
	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	final Object sharedLock = new Object();
	boolean stopped;
	final LinkedList<Object[]> data = new LinkedList();
	public static long memorySize = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalMemorySize() / 1024 / 1024 / 1024;
//	static int LIMIT = 1000000;
	public static DecimalFormat decimalFormatter = new DecimalFormat("#,###");
	Simulator simulator;
	public static final LinkedHashMap<Long, VariableWithAddress> cache = new LinkedHashMap<Long, VariableWithAddress>() {
		@Override
		protected boolean removeEldestEntry(Map.Entry<Long, VariableWithAddress> eldest) {
			return size() > 100000;
		}
	};
	public boolean dbIsInited = false;

	public H2Thread(Simulator simulator, String filenameOnly) {
		this.simulator = simulator;

		try {
			connectionString = "jdbc:h2:" + filenameOnly + ";PAGE_SIZE=4096000;CACHE_SIZE=1310720;";
			System.out.println("connectionString=" + connectionString);
			conn = DriverManager.getConnection(connectionString, "sa", "");
		} catch (SQLException ex) {
			Logger.getLogger(H2Thread.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void initDB() {
		try {
			conn.createStatement().execute("drop table `quantr` if exists;");
			conn.createStatement().execute("create table `quantr`(id bigint auto_increment primary key, date datetime);");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `sequence` bigint NOT NULL;");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `line` varchar(100) NOT NULL;");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `bytes` varchar(40) NOT NULL;");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `mem` boolean;");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `memOperation` varchar(200);");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `memRead` boolean");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `memAddr` bigint");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `memValue` bigint");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `memSize` int");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `priv` int NULL;");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `filename` varchar(100);");
			conn.createStatement().execute("ALTER TABLE `quantr` ADD `codeLineNo` int");
//			System.out.println(" - Adding columns to table `quantr`");
			for (String registerName : simulator.registers.keySet()) {
				String sql = "ALTER TABLE `quantr` ADD `" + registerName + "` bigint NOT NULL;";
				conn.createStatement().execute(sql);
//				conn.createStatement().execute("create index if not exists `quantr_" + registerName + "` on quantr(`" + registerName + "`);");
			}

//			System.out.println(" - creating index");
			conn.createStatement().execute("create index quantr_sequence on `quantr`(sequence);");
			conn.createStatement().execute("create index quantr_pc on `quantr`(pc);");

			String sql = "INSERT INTO `quantr` VALUES (default, CURRENT_TIMESTAMP(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?";
			sql += ",?".repeat(simulator.registers.size());
			sql += ")";
			stmt = conn.prepareStatement(sql);
			dbIsInited = true;
		} catch (SQLException ex) {
			Logger.getLogger(H2Thread.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void run() {
		while (!stopped) {
//			System.out.println("H2Thread::run " + stmt + ", " + data.isEmpty());
			if (stmt == null && !data.isEmpty()) {
				initDB();
			}
			if (!data.isEmpty()) {
				synchronized (sharedLock) {
					try {
//					System.out.println("data.size() =" + data.size());
//					System.out.println("a="+data.size());
						for (int z = 0; z < data.size() && z < 100000; z++) {
							Object[] temp;
							synchronized (data) {
								temp = data.pollLast();
							}
							kickRecordToStmt(stmt, temp);
							stmt.addBatch();
						}
//					System.out.println("b="+data.size());
						if (data.size() % 100000 == 0 || data.size() < 100000) {
//						System.out.println("f1");
							stmt.executeBatch();
							conn.commit();
							stmt.clearBatch();
//						System.out.println("f2");
						}
					} catch (SQLException ex) {
						Logger.getLogger(H2Thread.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException ex) {
				Logger.getLogger(H2Thread.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		System.out.println("H2 thread quit " + dbIsInited);
	}

	@Override
	public void add(ArrayList<Dwarf> dwarfArrayList, long pc, long cpuTick, String code, String ins, boolean mem, String memOperation, boolean memRead, long memAddr, long memValue, int memSize, int priv, LinkedHashMap<String, Register> registers) throws SQLException {
		if (!LogTicks.check(cpuTick)) {
			return;
		}
		Object[] temp = new Object[registers.size() + 12];
		temp[0] = cpuTick;
		temp[1] = code;
		temp[2] = ins;
		temp[3] = mem;
		temp[4] = memOperation;
		temp[5] = memRead;
		temp[6] = memAddr;
		temp[7] = memValue;
		temp[8] = memSize;
		temp[9] = priv;

		VariableWithAddress v;
		synchronized (cache) {
			v = cache.get(pc);
			if (v == null) {
				v = QuantrDwarf.getFilePathAndLineNo(dwarfArrayList, BigInteger.valueOf(pc));
				cache.put(pc, v);
			}
		}
		temp[10] = v == null ? null : v.file.getName();
		temp[11] = v == null ? -1 : v.line_num;

		int x = 12;
		for (String registerName : registers.keySet()) {
			temp[x++] = registers.get(registerName).getValue();
		}

		long heapSize = Runtime.getRuntime().totalMemory() / 1024 / 1024 / 1024;
		long freeHeapSize = Runtime.getRuntime().freeMemory() / 1024 / 1024 / 1024;
//		System.out.println((heapSize - freeHeapSize) + " / " + heapSize + " = " + (float) (heapSize - freeHeapSize) / heapSize);
		if (!data.isEmpty() && heapSize > 10 && (float) (heapSize - freeHeapSize) / heapSize > 0.9) {
			while (!data.isEmpty() && heapSize > 10) {
				try {
					Thread.sleep(10000);
					System.gc();
					System.out.println(Simulator.sdf.format(new Date()) + ", sleeping, not enough memory " + Math.round(((float) (heapSize - freeHeapSize) / heapSize) * 100) + "%, data.size = " + decimalFormatter.format(data.size()) + ", be patient, h2 need time to kick records to db and release memory");
				} catch (InterruptedException ex) {
					Logger.getLogger(H2Thread.class.getName()).log(Level.SEVERE, null, ex);
				}
				heapSize = Runtime.getRuntime().totalMemory() / 1024 / 1024 / 1024;
				freeHeapSize = Runtime.getRuntime().freeMemory() / 1024 / 1024 / 1024;
			}
		}
		synchronized (data) {
			data.push(temp);
		}
	}

	@Override
	public void stop() {
		synchronized (sharedLock) {
			if (conn != null) {
				try {
					while (!data.isEmpty() && !dbIsInited) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException ex) {
							Logger.getLogger(H2Thread.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
					stopped = true;
					System.out.println(sdf.format(new Date()) + " h2 stopping, remaining " + data.size() + " records");
					while (!dbIsInited) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException ex) {
							Logger.getLogger(H2Thread.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
					if (stmt != null) {
						while (!data.isEmpty()) {
							Object[] temp;
							synchronized (data) {
								temp = data.pollLast();
							}
							kickRecordToStmt(stmt, temp);
							stmt.addBatch();
							if (data.size() % 100000 == 0) {
								System.out.println(decimalFormatter.format(data.size()) + " records left");
								stmt.executeBatch();
								conn.commit();
							}
						}
						stmt.executeBatch();
					}
					conn.commit();
					conn.close();
					System.out.println(sdf.format(new Date()) + " h2 stopped");
				} catch (SQLException ex) {
					Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
				}
			} else {
				stopped = true;
			}
		}
	}

	@Override
	public String toString() {
		String str = "H2Thread{" + "connectionString=" + connectionString + "}, data length=" + decimalFormatter.format(data.size());
		try {
			Statement statement = conn.createStatement();
			ResultSet rs;
			try {
				rs = statement.executeQuery("select count(*) from `qemu`;");
				rs.next();
				str += "\n\tqemu  : " + decimalFormatter.format(rs.getInt("count(*)"));
			} catch (SQLException ex) {
				str += "\n\\tqemu  : 0";
			}
			try {
				rs = statement.executeQuery("select count(*) from `quantr`;");
				rs.next();
				str += "\n\tquantr: " + decimalFormatter.format(rs.getInt("count(*)"));
			} catch (SQLException ex) {
				str += "\n\tquantr: 0";
			}
		} catch (SQLException ex) {
			Logger.getLogger(H2Thread.class.getName()).log(Level.SEVERE, null, ex);
		}
		return str;
	}

	private void kickRecordToStmt(PreparedStatement stmt, Object[] temp) throws SQLException {
		int x = 1;
		stmt.setLong(x++, (Long) temp[0]);
		stmt.setString(x++, (String) temp[1]);
		stmt.setString(x++, (String) temp[2]);
		stmt.setBoolean(x++, (Boolean) temp[3]);
		stmt.setString(x++, (String) temp[4]);
		stmt.setBoolean(x++, (Boolean) temp[5]);
		stmt.setLong(x++, (long) temp[6]);
		stmt.setLong(x++, (long) temp[7]);
		stmt.setInt(x++, (int) temp[8]);
		stmt.setInt(x++, (int) temp[9]);
		stmt.setString(x++, (String) temp[10]);
		stmt.setInt(x++, (int) temp[11]);
		for (int tempX = 0; tempX < simulator.registers.size(); tempX++) {
			stmt.setLong(x, ((BigInteger) temp[x - 1]).longValue());
			x++;
		}
	}

}
