package hk.quantr.riscv_simulator.cpu;

import hk.quantr.javalib.CommonLib;
import hk.quantr.riscv_simulator.MemoryOperand;
import hk.quantr.riscv_simulator.Simulator;
import hk.quantr.riscv_simulator.exception.AccessFaultException;
import hk.quantr.riscv_simulator.exception.IRQException;
import hk.quantr.riscv_simulator.exception.PageFaultException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Memory {

	public TreeMap<Long, MemoryMap> memoryMaps = new TreeMap<>();
	public ArrayList<MemoryOperand> lastOperands = new ArrayList();
	public ArrayList<MemoryOperand> operands = new ArrayList();
	HashSet<MemoryHandler> memoryHandlers;
//	public static HashSet<Long> pagingEntry = new HashSet();
	Simulator simulator;
	int limit = 1000;
	private HashMap<Long, MemoryHandler> cache = new HashMap();

	public Memory(Simulator simulator, HashSet<MemoryHandler> memoryHandlers) {
		this.simulator = simulator;
		this.memoryHandlers = memoryHandlers;
	}

	private void addLastOperands(MemoryOperand memoryOperand) {
		synchronized (lastOperands) {
			lastOperands.add(memoryOperand);
		}
		if (lastOperands.size() > limit) {
			lastOperands.remove(0);
		}
		synchronized (operands) {
			operands.add(memoryOperand);
		}
		if (operands.size() > limit) {
			operands.remove(0);
		}
	}

	public void resetLastOperands() {
		synchronized (lastOperands) {
			lastOperands.clear();
		}
	}

	// GodMode Write. Won't invoke any write failure and will not cause any side effects that may occur on write.
	public void writeByte(long address, byte b, boolean physical, boolean shouldLog) throws PageFaultException {
		Pair<Boolean, Integer> pair = invokeMemoryHandler(address, b, true);
		if (pair == null) {
			long mappedAddress = physical ? address : getMappedAddress(address, false);
			MemoryMap memoryMap = getRam(mappedAddress);
			byte ram[] = memoryMap.ram;
			ram[(int) (mappedAddress - memoryMap.start)] = b;
			if (shouldLog) {
				addLastOperands(new MemoryOperand("write", address, mappedAddress, b));
			}
		}
	}

	//writeMemory will invoke write access check. If there are any CSR constrains, PMP constrains, check here before write.
	public void write(long cpuTick, long address, long value, int numOfBytes, boolean physical, boolean shouldLog) throws PageFaultException, AccessFaultException {
		if (PMP.checkPermission(address, numOfBytes, 'w')) {
			for (int i = 0; i < numOfBytes; i++) {
				writeByte(address + i, (byte) (value >> (i * 8)), physical, shouldLog);
			}
		} else {
			System.out.println("Error: Store Access-Fault Exception");
			throw new AccessFaultException();
		}
		if (address == 0x2004000) {
			simulator.mtimecmp = value;
		}
	}

	// GodMode read. Won't invoke any read failure and will not cause any side effects that may occur on read.
	public byte read(long address, boolean physical, boolean logToLastOperands) throws PageFaultException {
		Pair<Boolean, Integer> pair = invokeMemoryHandler(address, (byte) -1, false);
		if (pair != null && pair.getLeft()) {
			int temp = pair.getRight();
			return (byte) temp;
		}
		long mappedAddress = physical ? address : getMappedAddress(address, false);
		if (mappedAddress == 0x7000) {
			int x = 12;
		}
		MemoryMap memoryMap = getRam(mappedAddress);
		if (memoryMap == null) {
			System.out.println("memoryMap is null :0x" + Long.toHexString(address));
		}
		byte ram[] = memoryMap.ram;
		if (logToLastOperands) {
			addLastOperands(new MemoryOperand("read", address, mappedAddress));
		}
		if (address < 0) {
			if (logToLastOperands) {
				addLastOperands(new MemoryOperand("exception read", address, mappedAddress, (byte) 0));
			}
		}
		return ram[(int) (mappedAddress - memoryMap.start)];
	}

	public long readGodMode(long address, int numOfBytes) throws PageFaultException, IRQException {
		long res = 0;
		for (int i = 0; i < numOfBytes; i++) {
			res |= (long) (read(address + i, true, false) & 0xff) << (i * 8);
		}
		return res;
	}

	//readMemory will invoke read access check. If there are any CSR constrains, PMP constrains, check here before read.
	public long read(long address, int numOfBytes, boolean shouldLog, boolean physical) throws PageFaultException, AccessFaultException {
		if (PMP.checkPermission(address, numOfBytes, 'r')) {
			BigInteger res = BigInteger.ZERO;
			for (int i = 0; i < numOfBytes; i++) {
				res = res.or(BigInteger.valueOf((long) (read(address + i, physical, shouldLog) & 0xff) << (i * 8)));
			}
			return res.longValue();
		} else {
			System.out.println("Error: Load Access-Fault Exception");
			throw new AccessFaultException();
		}
	}

	//bypass only when you would like to get mapped paging address in Machine mode
	public long getMappedAddress(long address, boolean bypass) throws PageFaultException {
		if (bypass /*&& CPUState.priv == 3*/) {
			return address;
		}
		if (0x2000000l <= address && address < 0x2010000l) {
			return address;
		}
		long satp = simulator.registers.get("satp").getValue().longValue();
		if (satp >> 60 == -8) {
			long result;
			long pageOffset = address & 0xfff;
			long vpn0 = (address >> 12) & 0b111111111;
			long vpn1 = (address >> 21) & 0b111111111;
			long vpn2 = (address >> 30) & 0b111111111;
			long satpBase = (satp & 0xfffffffffffL) << 12;
			//level 2
			long pte2 = vpn2 * 8 + satpBase;
			MemoryMap memoryMap = getRam(pte2);
			byte ram[] = memoryMap.ram;
			byte[] pte2Bytes = new byte[8];
			System.arraycopy(ram, (int) (pte2 - memoryMap.start), pte2Bytes, 0, 8);
			long pte2Long = CommonLib.get64BitsInt(pte2Bytes, 0);
			if ((pte2Long & 1) == 0) {
				throw new PageFaultException("Page Fault: Page Table Entry Invalid at 0x" + Long.toHexString(pte2));
			}
			if ((pte2Long & 0xe) != 0) {
				result = (pte2Long & 0x3ffffff0000000L) << 2 | (vpn1 << 21) | (vpn0 << 12) | pageOffset;
//				System.out.println("level 2 result: " + Long.toHexString(result));
				return result;
			}
			//level 1
			long level1Base = ((pte2Long >> 10) & 0x3fffffffffL) << 12;
			long pte1 = vpn1 * 8 + level1Base;
			memoryMap = getRam(pte1);
			ram = memoryMap.ram;
			byte[] pte1Bytes = new byte[8];
			System.arraycopy(ram, (int) (pte1 - memoryMap.start), pte1Bytes, 0, 8);
			long pte1Long = CommonLib.get64BitsInt(pte1Bytes, 0);
			if ((pte1Long & 1) == 0) {
				throw new PageFaultException("Page Fault: Page Table Entry Invalid at 0x" + Long.toHexString(pte1));
			}
			if ((pte1Long & 0xe) != 0) {
				result = (pte1Long & 0x3ffffffff80000L) << 2 | (vpn0 << 12) | pageOffset;
//				System.out.println("level 1 result: " + Long.toHexString(result));
				return result;
			}
			//level 0
			long level0Base = ((pte1Long >> 10) & 0x3fffffffffL) << 12;
			long pte0 = vpn0 * 8 + level0Base;
			memoryMap = getRam(pte0);
			ram = memoryMap.ram;
			byte[] pte0Bytes = new byte[8];
			System.arraycopy(ram, (int) (pte0 - memoryMap.start), pte0Bytes, 0, 8);
			long pte0Long = CommonLib.get64BitsInt(pte0Bytes, 0);
			if ((pte0Long & 0xf) == 0) {
				throw new PageFaultException("Page Fault: Page Table Entry Invalid at 0x" + Long.toHexString(pte0));
			}
			result = (((pte0Long >> 10) & 0x3fffffffffL) << 12) + pageOffset;
//			System.out.println("level 0 result: " + Long.toHexString(result));
			return result;
		}
		return address;
	}

	public MemoryMap getRam(long address) {
		for (Map.Entry<Long, MemoryMap> entry : memoryMaps.entrySet()) {
			MemoryMap memoryMap = entry.getValue();
			if (memoryMap.start <= address && address < memoryMap.start + memoryMap.size) {
				return memoryMap;
			}
		}
		System.err.printf("Cannot access memory at 0x%x\n", address);
		return null;
	}

	private Pair<Boolean, Integer> invokeMemoryHandler(long address, byte value, boolean isWrite) {
//		if (cache.size() % 10000 == 0) {
//			System.out.println(cache.size());
//		}
//		MemoryHandler handler = cache.get(address);
		MemoryHandler handler = null;
//		if (handler == null) {
		for (MemoryHandler temp : this.memoryHandlers) {
			if (temp.getAddress() <= address && address < temp.getAddress() + temp.getSize()) {
				handler = temp;
				break;
			}
		}
//		}
		if (handler != null) {
//			cache.put(address, handler);
			if (isWrite && handler.isWrite()) {
				handler.write(address, value);
				return new MutablePair<>(true, -1);
			} else if (!isWrite && handler.isRead()) {
				int b = handler.read(address);
				return new MutablePair<>(true, b);
			}
		}
		return null;
	}

}
