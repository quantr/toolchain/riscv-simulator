package hk.quantr.riscv_simulator.cpu;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class UART implements MemoryHandler {

	final long THR = 0x10000000L;
	final long IER = 0x10000001L;
	final long ISR = 0x10000002L;
	final long LCR = 0x10000003L;
	final long MCR = 0x10000004L;
	final long LSR = 0x10000005L;
	final long MSR = 0x10000006L;
	final long SPR = 0x10000007L;

	private byte thr;
	private byte ier;
	private byte isr;
	private byte lcr;
	private byte mcr;
	private byte lsr;
	private byte msr;
	private byte spr;

	private boolean isRead = false;
	private boolean isWrite = false;
	private long address = 0;
	private int size = 0;

	public UART(boolean isRead, boolean isWrite, long address, int size) {
		this.isRead = isRead;
		this.isWrite = isWrite;
		this.address = address;
		this.size = size;
	}

	@Override
	public int read(long address) {
		if (address == LSR) {
			return 0x60;
		}
		return -1;
	}

	@Override
	public void write(long address, int value) {
		if (address == THR && (lcr & 0xff) >> 7 == 0 && (ier & 2) == 2) {
//			System.out.println(ier + " == " + value);
			System.out.print((char) value);
			System.out.flush();
		} else if (address == LCR) {
			lcr = (byte) value;
		} else if (address == IER) {
			ier = (byte) value;
		} else if (address == THR) {
			thr = (byte) value;
		} else if (address == ISR) {
			isr = (byte) value;
		} else if (address == MCR) {
			mcr = (byte) value;
		} else if (address == LSR) {
			lsr = (byte) value;
		} else if (address == MSR) {
			msr = (byte) value;
		} else if (address == SPR) {
			spr = (byte) value;
		} else {
			System.err.println("UART write error: 0x" + Long.toHexString(address) + ", value=" + Integer.toHexString(value));
			System.exit(5000);
		}
	}

//	@Override
//	public void write(long address, long value) {
//		if (address == THR && (lcr & 0xff) >> 7 == 0) {
//			System.out.print((char) value);
//			System.out.flush();
//		} else if (address == LCR) {
//			lcr = (byte) value;
//		} else if (address == IER) {
//			ier = (byte) value;
//		} else if (address == THR) {
//			thr = (byte) value;
//		} else if (address == ISR) {
//			isr = (byte) value;
//		} else {
//			System.err.println("UART write error: 0x" + Long.toHexString(address) + ", value=" + Long.toHexString(value));
//			System.exit(5000);
//		}
//	}
	@Override
	public boolean isRead() {
		return isRead;
	}

	@Override
	public boolean isWrite() {
		return isWrite;
	}

	@Override
	public long getAddress() {
		return address;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public String toString() {
		return "UART{" + ", address=0x" + Long.toHexString(address) + ", size=0x" + Long.toHexString(size) + "THR=" + THR + ", IER=" + IER + ", ISR=" + ISR + ", LCR=" + LCR + ", MCR=" + MCR + ", LSR=" + LSR + ", MSR=" + MSR + ", SPR=" + SPR + ", thr=" + thr + ", ier=" + ier + ", isr=" + isr + ", lcr=" + lcr + ", isRead=" + isRead + ", isWrite=" + isWrite + '}';
	}

}
