package hk.quantr.riscv_simulator.cpu;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class PLIC implements MemoryHandler {

	private boolean isRead = false;
	private boolean isWrite = false;
	private long address = 0;
	private int size = 0;

//	private long mClaim = 0xc201004;
//	private byte priority;
	private int claimed;
	private int enable;
	private int pending;
	private int claim;
	private int priority;
	private int sourcePriority[] = new int[32];
	private int targetPriority[] = new int[2];
	private final int numSources = 96;

//	public int[] registers;
	public PLIC(boolean isRead, boolean isWrite, long address, int size) {
		this.isRead = isRead;
		this.isWrite = isWrite;
		this.address = address;
		this.size = size;

		sourcePriority[1] = 1;
		sourcePriority[10] = 1;
	}

	@Override
	public boolean isRead() {
		return isRead;
	}

	@Override
	public boolean isWrite() {
		return isWrite;
	}

	@Override
	public long getAddress() {
		return address;
	}

	@Override
	public int getSize() {
		return size;
	}

//	@Override
//	public int read(long address) {
//		System.out.println("read() = " + Long.toHexString(address));
//		if (address == 0xc201000l) {
//			return targetPriority[1];
//		} else if (address == 0xc201004l) {
//			int maxIRQ = claimed(1);
//			if (maxIRQ > 0) {
//				setPending(maxIRQ, false);
//				setClaimed(maxIRQ, true);
//			}
////			sifive_plic_update(plic);
//			System.out.println("read c201004=" + maxIRQ);
//			return maxIRQ;
//		}
//		update();
////		int index = getIndex(address);
////		return registers[index];
//		return 0;
//	}
	public int read(long a) {
		long addr = a & 0xffffffffl;
		if (addr == address + 0x1000) {
			return pending;
		} else if (addr == address + 0x2080) {
			return enable;
		} else if (addr == address + 0x201000) {
			return priority;
		} else if (addr == address + 0x201004) {
//			System.out.println(claim);
			return 0; // force return 0 because read mean clear, refer to https://ithelp.ithome.com.tw/articles/10277204?sc=hot
			//return claim;
		} else {
			return 0;
		}
	}

	private int claimed(int id) {
		System.out.println("claimed() = " + id);
		// if id=0, mode=3
		// if id=1, mode=1
		int maxIRQ = 0;
		int maxPrio = targetPriority[id];

		int pendingEnabledNotClaimed = (pending & ~claimed) & enable;
		System.out.printf("pendingEnabledNotClaimed=%x, pending=%x, ~claimed~=%x, enable=$x\n", pendingEnabledNotClaimed, pending, ~claimed, enable);
		for (int j = 0; j < 32; j++) {
			int irq = j;
			int prio = sourcePriority[irq];
			int enabled = pendingEnabledNotClaimed & (1 << j);

			System.out.println("enabled=" + enabled + ", irq=" + irq + ", prio=" + prio);

			if (enabled > 0 && prio > maxPrio) {
				maxIRQ = irq;
				maxPrio = prio;
			}
		}
		return maxIRQ;
	}

//	private int getIndex(long address) {
//		return (int) ((address - baseAddress));
//	}
//	@Override
//	public void write(long address, int value) {
//		System.out.println("write() = " + Long.toHexString(address) + " = " + value);
//		if (address == 0xc201000l) {
//			System.out.printf("write %d %x\n", CPUState.priv, value);
//			targetPriority[1] = value % 8;
//			update();
//		} else if (address == 0xc201004l) {
//			if (value < numSources) {
//				setClaimed(value, false);
//				update();
//			}
//		}
//	}
	@Override
	public void write(long targetAddress, int value) {
		long addr = targetAddress & 0xffffffffl;
//		System.out.println("called write PLIC: " + Long.toHexString(addr) + ", " + Long.toHexString(targetAddress));
		if (addr == address + 0x1000) {
			pending = value;
		} else if (addr == address + 0x2080) {
			enable = value;
		} else if (addr == address + 0x201000) {
			priority = value;
		} else if (addr == address + 0x201004) {

			//pending to use dynamic way to write bytes
			claim = value;
//			System.out.println("written: " + claim);
		} else if (addr == address + 0x201005) {
			claim = claim | (value << 8);
//			System.out.println("written: " + claim);
		} else if (addr == address + 0x201006) {
			claim = claim | (value << 16);
//			System.out.println("written: " + claim);
		} else if (addr == address + 0x201007) {
			claim = claim | (value << 24);
//			System.out.println("written: " + claim);
		}
	}

	private void setPending(int irq, boolean b) {
//		System.out.println("setPending " + irq + " = " + b);
		if (b) {
			pending = pending | (1 << irq);
		} else {
			pending = pending & ~(1 << irq);
		}
	}

	private void setClaimed(int irq, boolean b) {
		if (b) {
			claim = claim | (1 << irq);
		} else {
			claim = claim & ~(1 << irq);
		}
	}

	private void update() {

	}

	@Override
	public String toString() {
		return "PLIC{address=0x" + Long.toHexString(address) + ", size=0x" + Long.toHexString(size) + ",  isRead=" + isRead + ", isWrite=" + isWrite + ", claimed=" + claimed + ", enable=" + enable + ", pending=" + pending + ", claim=" + claim + ", priority=" + priority + ", sourcePriority=" + sourcePriority + ", targetPriority=" + targetPriority + ", numSources=" + numSources + '}';
	}

}
