package hk.quantr.riscv_simulator.cpu;

//import com.thoughtworks.xstream.annotations.XStreamOmitField;
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MemoryMap {

	public String name;
	public long start;
	public int size;

//	@XStreamOmitField
	public byte ram[];

	public MemoryMap(String name, long start, int size, byte[] ram) {
		this.name = name;
		this.start = start;
		this.size = size;
		this.ram = ram;
	}

}
