package hk.quantr.riscv_simulator.cpu;

import hk.quantr.riscv_simulator.Simulator;
import hk.quantr.riscv_simulator.exception.AccessFaultException;
import hk.quantr.riscv_simulator.exception.IRQException;
import hk.quantr.riscv_simulator.exception.PageFaultException;
import hk.quantr.riscv_simulator.exception.RiscvInterrupt;
import hk.quantr.riscv_simulator.setting.FireInterrupt;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Virtio implements MemoryHandler {

	Simulator simulator;
	private boolean isRead = false;
	private boolean isWrite = false;
	private long address = 0;
	private int size = 0;

	final int VIRTIO_BLK_T_IN = 0;
	final int VIRTIO_BLK_T_OUT = 1;

	final int VIRTIO_MMIO_MAGIC_VALUE = 0x000; // 0x74 72 69 76
	final int VIRTIO_MMIO_VERSION = 0x004; // version; should be 2
	final int VIRTIO_MMIO_DEVICE_ID = 0x008; // device type; 1 is net, 2 is disk
	final int VIRTIO_MMIO_VENDOR_ID = 0x00c; // 0x55 4d 45 51
	final int VIRTIO_MMIO_DEVICE_FEATURES = 0x010;
	final int VIRTIO_MMIO_DRIVER_FEATURES = 0x020;
	final int VIRTIO_MMIO_QUEUE_NUM_MAX = 0x034;
	final int VIRTIO_MMIO_QUEUE_NOTIFY = 0x50;
	final int VIRTIO_MMIO_QUEUE_STATUS = 0x070;
	final int VIRTIO_MMIO_QUEUE_DESC_LOW = 0x080;// physical address for descriptor table, write-only
	final int VIRTIO_MMIO_QUEUE_DESC_HIGH = 0x084;
	final int VIRTIO_MMIO_DRIVER_DESC_LOW = 0x090; // physical address for available ring, write-only
	final int VIRTIO_MMIO_DRIVER_DESC_HIGH = 0x094;
	final int VIRTIO_MMIO_DEVICE_DESC_LOW = 0x0a0;// physical address for used ring, write-only
	final int VIRTIO_MMIO_DEVICE_DESC_HIGH = 0x0a4;

	int driver_features;

	Memory memory;
	public int[] virtioRegisters;
	File fsImg;
	RandomAccessFile randomAccessFile;

	public Virtio(Simulator simulator, boolean isRead, boolean isWrite, long address, int size, Memory memory, File fsImg) throws FileNotFoundException {
		this.simulator = simulator;
		this.isRead = isRead;
		this.isWrite = isWrite;
		this.address = address;
		this.size = size;
		this.memory = memory;
		this.fsImg = fsImg;
		randomAccessFile = new RandomAccessFile(fsImg, "rw");
		virtioRegisters = new int[size];
	}

	@Override
	public boolean isRead() {
		return isRead;
	}

	@Override
	public boolean isWrite() {
		return isWrite;
	}

	@Override
	public long getAddress() {
		return address;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public int read(long addr) {
		int index = (int) (addr - address);
		if (index == VIRTIO_MMIO_MAGIC_VALUE) {
			return 0x76;
		} else if (index == VIRTIO_MMIO_MAGIC_VALUE + 1) {
			return 0x69;
		} else if (index == VIRTIO_MMIO_MAGIC_VALUE + 2) {
			return 0x72;
		} else if (index == VIRTIO_MMIO_MAGIC_VALUE + 3) {
			return 0x74;
		} else if (index == VIRTIO_MMIO_VERSION) {
			return 2;
		} else if (index == VIRTIO_MMIO_VERSION + 1) {
			return 0;
		} else if (index == VIRTIO_MMIO_VERSION + 2) {
			return 0;
		} else if (index == VIRTIO_MMIO_VERSION + 3) {
			return 0;
		} else if (index == VIRTIO_MMIO_DEVICE_ID) {
			return 2;
		} else if (index == VIRTIO_MMIO_DEVICE_ID + 1) {
			return 0;
		} else if (index == VIRTIO_MMIO_DEVICE_ID + 2) {
			return 0;
		} else if (index == VIRTIO_MMIO_DEVICE_ID + 3) {
			return 0;
		} else if (index == VIRTIO_MMIO_VENDOR_ID) {
			return 0x51;
		} else if (index == VIRTIO_MMIO_VENDOR_ID + 1) {
			return 0x45;
		} else if (index == VIRTIO_MMIO_VENDOR_ID + 2) {
			return 0x4d;
		} else if (index == VIRTIO_MMIO_VENDOR_ID + 3) {
			return 0x55;
		} else if (index == VIRTIO_MMIO_DEVICE_FEATURES) {
			return virtioRegisters[index];
		} else if (index == VIRTIO_MMIO_DEVICE_FEATURES + 1) {
			return virtioRegisters[index];
		} else if (index == VIRTIO_MMIO_DEVICE_FEATURES + 2) {
			return virtioRegisters[index];
		} else if (index == VIRTIO_MMIO_DEVICE_FEATURES + 3) {
			return virtioRegisters[index];
		} else if (index == VIRTIO_MMIO_DRIVER_FEATURES) {
			return driver_features & 0xff;
		} else if (index == VIRTIO_MMIO_DRIVER_FEATURES + 1) {
			return (driver_features >> 8) & 0xff;
		} else if (index == VIRTIO_MMIO_DRIVER_FEATURES + 2) {
			return (driver_features >> 16) & 0xff;
		} else if (index == VIRTIO_MMIO_DRIVER_FEATURES + 3) {
			return (driver_features >> 24) & 0xff;
		} else if (index == VIRTIO_MMIO_QUEUE_NUM_MAX) {
			return 0;
		} else if (index == VIRTIO_MMIO_QUEUE_NUM_MAX + 1) {
			return 0x4;
		} else if (index == VIRTIO_MMIO_QUEUE_NUM_MAX + 2) {
			return 0;
		} else if (index == VIRTIO_MMIO_QUEUE_NUM_MAX + 3) {
			return 0;
		} else if (index < virtioRegisters.length) {
			return virtioRegisters[index];
		} else {
			System.out.println("virtio register index too large " + index);
			System.exit(6000);
		}
		return -1;
	}

	@Override
	public void write(long addr, int value) {
		try {
			int index = (int) ((addr - address));
			if (index < virtioRegisters.length) {
//				System.out.printf("write virtio %x <= %x\n", address, value);
//				virtioRegisters[index] = value;

//				int quotient = index / 4;
//				int remainder = index % 4;
//
//				if (remainder == 0) {
				virtioRegisters[index] = value & 0xFF; // a byte
//				} else {
//					virtioRegisters[index - index % 4] |= value << remainder * 8;
//				}

//				System.out.println(Long.toHexString(virtioRegisters[index - index % 4]));
//				if (index == VIRTIO_MMIO_QUEUE_PFN) {
//					System.out.println("");
//				}
				if (index == VIRTIO_MMIO_QUEUE_NOTIFY) {
					//for avail
//					long temp = (virtioRegisters[VIRTIO_MMIO_QUEUE_PFN + 3] << 24) + (virtioRegisters[VIRTIO_MMIO_QUEUE_PFN + 2] << 16) + (virtioRegisters[VIRTIO_MMIO_QUEUE_PFN + 1] << 8) + virtioRegisters[VIRTIO_MMIO_QUEUE_PFN];
//					long virtio_mmio_queue_pfn = temp << 12;
//					if (virtio_mmio_queue_pfn != 0) {
					long descriptorAddress = ((virtioRegisters[VIRTIO_MMIO_QUEUE_DESC_LOW + 3] << 24) + (virtioRegisters[VIRTIO_MMIO_QUEUE_DESC_LOW + 2] << 16) + (virtioRegisters[VIRTIO_MMIO_QUEUE_DESC_LOW + 1] << 8) + virtioRegisters[VIRTIO_MMIO_QUEUE_DESC_LOW]) & 0xffffffffl;
					long VIRTIO_BLK_REQ_ADDR = memory.read(descriptorAddress, 8, true, true);
					long type = memory.read(VIRTIO_BLK_REQ_ADDR, 4, true, true);
					long sectorNo = memory.read(VIRTIO_BLK_REQ_ADDR + 8, 8, true, true);
//					System.out.println("type=" + type);
//					System.out.println("sector=" + sector);
					if (type == VIRTIO_BLK_T_IN) {
						byte[] bs = new byte[512];
						randomAccessFile.seek(sectorNo * 512);
						randomAccessFile.readFully(bs);
//						randomAccessFile.close();
						long targetAddress = memory.read(descriptorAddress + 16, 8, true, true); //0x80006000 in descriptor[1]
						//System.out.println("value =" + memory.readHWord(Address+0x1c));
						//System.out.println("d0 value ="+memory.readDWord(VIRTIO_BLK_REQ_ADDR));
//						long DA = memory.read(Address + 0x10, 8, true, true);
//						long descriptorLength = memory.read(Address + 0x18, 4, true, true);
						for (int i = 0; i < bs.length; i++) {
							memory.writeByte(targetAddress + i, bs[i], true, true);
						}
//						System.out.println("end" + memory.read(DA, 8, true, true));

						//virtioRegisters[(VIRTIO_MMIO_QUEUE_PFN<<3) + 0x10] = value;
						//descriptor 1
						//throw new IRQException();
//                        int i = fis.read(bs, (int) (sector * 512), 512);
//                        System.out.println("Test = " + i);
						FireInterrupt interrupt = new FireInterrupt();
						interrupt.cause = RiscvInterrupt.MACHINE_EXTERNAL_INTERRUPT;
						simulator.pendingInterrupt = interrupt;
					} else if (type == VIRTIO_BLK_T_OUT) {
						long targetAddress = memory.read(descriptorAddress + 16, 8, true, true); //0x80006000 in descriptor[1]
						int len = (int) memory.read(descriptorAddress + 24, 4, true, true);
						byte[] bs = new byte[len];
						//System.out.println("value =" + memory.readHWord(Address+0x1c));
						//System.out.println("d0 value ="+memory.readDWord(VIRTIO_BLK_REQ_ADDR));
//						long DA = memory.read(Address + 0x10, 8, true, true);
//						long descriptorLength = memory.read(Address + 0x18, 4, true, true);
						for (int i = 0; i < bs.length; i++) {
							bs[i] = (byte) memory.read(targetAddress + i, 1, true, true);
						}

						randomAccessFile.seek(sectorNo * 512);
						randomAccessFile.write(bs, 0, len);
						randomAccessFile.readFully(bs);
					} else {
						System.out.println("virtio type must be either VIRTIO_BLK_T_IN or VIRTIO_BLK_T_OUT");
					}
					//} else {
					//for used 
					int DESC_len = 8; // to be verified
					long descSize = 0x8L; // descriptor size
					long addr_virtq_avail = ((virtioRegisters[VIRTIO_MMIO_DRIVER_DESC_LOW + 3] << 24) + (virtioRegisters[VIRTIO_MMIO_DRIVER_DESC_LOW + 2] << 16) + (virtioRegisters[VIRTIO_MMIO_DRIVER_DESC_LOW + 1] << 8) + virtioRegisters[VIRTIO_MMIO_DRIVER_DESC_LOW]) & 0xffffffffl;
					long addr_virtq_used = ((virtioRegisters[VIRTIO_MMIO_DEVICE_DESC_LOW + 3] << 24) + (virtioRegisters[VIRTIO_MMIO_DEVICE_DESC_LOW + 2] << 16) + (virtioRegisters[VIRTIO_MMIO_DEVICE_DESC_LOW + 1] << 8) + virtioRegisters[VIRTIO_MMIO_DEVICE_DESC_LOW]) & 0xffffffffl;
					for (int i = 0; i < DESC_len; i++) {
						long desc_avail = memory.read(addr_virtq_avail + i * descSize, 8, true, true);
						memory.write(-1, addr_virtq_used + i * descSize, desc_avail, 8, true, true);
						//move avail to used 
					}

					// device writes 0 to status on success
					long virtio_mmio_queue_desc = descriptorAddress;//memory.read(address + VIRTIO_MMIO_QUEUE_DESC_LOW, 8, true, true);
					// sizeof(struct virtq_desc)=16
					long VIRTIO_STATUS = virtio_mmio_queue_desc + 16 * 2;
					VIRTIO_STATUS = memory.read(VIRTIO_STATUS, 8, true, true);
					memory.write(-1, VIRTIO_STATUS, 0, 1, true, true);

//						memory.write(VIRTIO_STATUS, 0, 1, true, true);
					// end device writes 0 to status on success
					//}
				}
			}
		} catch (PageFaultException ex) {
			System.out.println(ex.message);
			Logger.getLogger(Virtio.class.getName()).log(Level.SEVERE, null, ex);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Virtio.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Virtio.class.getName()).log(Level.SEVERE, null, ex);
		} catch (AccessFaultException ex) {
			Logger.getLogger(Virtio.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public String toString() {
		return "Virtio{" + "address=0x" + Long.toHexString(address) + ", size=0x" + Long.toHexString(size) + ", isRead=" + isRead + ", isWrite=" + isWrite + ", VIRTIO_BLK_T_IN=" + VIRTIO_BLK_T_IN + ", VIRTIO_BLK_T_OUT=" + VIRTIO_BLK_T_OUT + ", VIRTIO_MMIO_MAGIC_VALUE=" + VIRTIO_MMIO_MAGIC_VALUE + ", VIRTIO_MMIO_VERSION=" + VIRTIO_MMIO_VERSION + ", VIRTIO_MMIO_DEVICE_ID=" + VIRTIO_MMIO_DEVICE_ID + ", VIRTIO_MMIO_VENDOR_ID=" + VIRTIO_MMIO_VENDOR_ID + ", VIRTIO_MMIO_DEVICE_FEATURES=" + VIRTIO_MMIO_DEVICE_FEATURES + ", VIRTIO_MMIO_DRIVER_FEATURES=" + VIRTIO_MMIO_DRIVER_FEATURES + ", VIRTIO_MMIO_QUEUE_NUM_MAX=" + VIRTIO_MMIO_QUEUE_NUM_MAX + ", VIRTIO_MMIO_QUEUE_NOTIFY=" + VIRTIO_MMIO_QUEUE_NOTIFY + ", VIRTIO_MMIO_QUEUE_STATUS=" + VIRTIO_MMIO_QUEUE_STATUS + ", driver_features=" + driver_features + ", memory=" + memory + ", virtioRegisters=" + virtioRegisters + ", fsImg=" + fsImg + '}';
	}

}
