package hk.quantr.riscv_simulator.cpu;

import hk.quantr.riscv_simulator.Simulator;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 *
 * @author Darren
 */
class PMPSettings {

	long pmpaddr, pmpcfg, start, end, L, R, W, X;
	String A;

	public PMPSettings(long addr, long conf, long start, long end, long L, String A, long R, long W, long X) {
		this.pmpaddr = addr;
		this.pmpcfg = conf;
		this.start = start;
		this.end = end;
		this.L = L;
		this.A = A;
		this.R = R;
		this.W = W;
		this.X = X;
	}
}

public class PMP {

	static boolean enable = false;
	public static boolean implemented = false;
	static ArrayList<PMPSettings> pmpList = new ArrayList<>();

	public static void init() {
		PMPSettings pmp = new PMPSettings(0, 0, 0, 0, 0, "NULL", 0, 0, 0);
		for (int i = 0; i < 16; i++) {
			pmpList.add(pmp);
		}
	}

	public static void updateAllPmp(Simulator simulator) { //Hard-Core update
		for (int i = 0; i < 16; i++) {
			updatePMPIndex(simulator, i);
		}
	}

	public static void updatePmp(Simulator simulator, String pmpreg) { //Specific update, higher efficiency
		if (pmpreg.matches("pmpaddr([0-9]|1[0-5])")) {
			int index = Integer.parseInt(pmpreg.substring(7));
			updatePMPIndex(simulator, index);
		} else if (pmpreg.matches("pmpcfg([0-9]|1[0-5])")) {
			int index = Integer.parseInt(pmpreg.substring(6)), start = index == 0 ? 0 : 8, end = index == 0 ? 8 : 16;
			for (int i = start; i < end; i++) {
				updatePMPIndex(simulator, i);
			}
		}
	}

	private static void updatePMPIndex(Simulator simulator, int i) {
		long pmpaddr = simulator.registers.get("pmpaddr" + i).getValue().longValue(), address = (pmpaddr & 0x3fffffffffffffL) << 2, startAddr = 0, endAddr = 0;
		long pmpcfg = (i < 8) ? (simulator.registers.get("pmpcfg0").getValue().longValue() >> (i * 8)) & 0xff : (simulator.registers.get("pmpcfg2").getValue().longValue() >> ((i - 8) * 8)) & 0xff;
		long A = (pmpcfg >> 3) & 0x3, R = pmpcfg & 1, W = (pmpcfg >> 1) & 1, X = (pmpcfg >> 2) & 1, L = (pmpcfg >> 7) & 1;
		String a = "NULL";
		if (A == 1) { // Top of Range (TOR)
			startAddr = (i != 0) ? (simulator.registers.get("pmpaddr" + (i - 1)).getValue().longValue() & 0x3fffffffffffffL) << 2 : 0;
			endAddr = address;
			a = "TOR";
		} else if (A == 2) { // Naturally aligned four-byte region (NA4)
			startAddr = address;
			endAddr = address + 4;
			a = "NA4";
		} else if (A == 3) { //Naturally aligned power-of-two region, >= 8 bytes (NAPOT)
			int num = getConsecutiveOne(pmpaddr);
			startAddr = removeConsecutiveOne(pmpaddr) << 2;
			endAddr = startAddr + (2 << (num + 3 - 1));
			a = "NAPOT";
		} //else A == 0, no need to do anything as everything is preset already.
		if ((address | pmpcfg) != 0) {
			implemented = true;
		}
		pmpList.set(i, new PMPSettings(pmpaddr, pmpcfg, startAddr, endAddr, L, a, R, W, X));
	}

	public static boolean checkPermission(long address, int numberOfBytes, char access) { //access can be 'r' 'w' 'x', numberOfBytes range: 0-8
		if (!enable) {
			return true;
		}
		implemented = false;
		for (PMPSettings settings : pmpList) {
			/*	Let a1 = accessStart, a2 = accessEnd, p1 = pmp block start, p2 = pmp block end.
				Cases to check permission: (p1 a1 a2 p2)
				Three cases to access fault: (p1 a1 p2 a2), (a1 p1 a2 p2), (a1 p1 p2 a2)*/
			long a1 = address, a2 = address + numberOfBytes;
			if (a1 >= settings.start && a2 < settings.end) { //Matches all bytes of an access
				implemented = true;
				if (settings.L == 0 && CPUState.priv == 3) {
					return true; //L bit 0 in Machine mode, access succeeds
				}
				return !((access == 'r' && settings.R == 0) || (access == 'w' && settings.W == 0) || (access == 'x' && settings.X == 0));
			} else if ((a1 >= settings.start && a1 < settings.end) || (a2 >= settings.start && a2 < settings.end) || (a1 < settings.start && a2 >= settings.end)) {
				return false;
			} else if (settings.start != 0 || settings.end != 0) {
				implemented = true;
			}
		}
		//If no PMP entry matches an S-mode or U-mode access, but at least one PMP entry is implemented, access failes.
		//No entry match in M-mode, access succeeds.
		return !(implemented && CPUState.priv < 2);
	}

	public static void printAllPMPSettings() {
		for (int i = 0; i < 16; i++) {
			printPMP(i);
		}
	}

	public static void printPMP(int x) {
		PMPSettings pmp = pmpList.get(x);
		System.out.println(String.format("pmpaddr%-2d : %016x | pmpcfg%-2d : %02x | mode: %-5s | Protecting %016x-%016x with Lrwx=%d%d%d%d", x, pmp.pmpaddr, x, pmp.pmpcfg, pmp.A, pmp.start, pmp.end != 0 ? pmp.end - 1 : 0, pmp.L, pmp.R, pmp.W, pmp.X));
	}

	private static int getConsecutiveOne(long address) {
		int count = 0;
		String str = Long.toBinaryString(address);
		for (int i = str.length() - 1; i >= 0; i--) {
			if (str.charAt(i) == '1') {
				count++;
			} else {
				break;
			}
		}
		return count;
	}

	private static long removeConsecutiveOne(long address) {
		char[] str = Long.toBinaryString(address).toCharArray();
		for (int i = str.length - 1; i >= 0; i--) {
			if (str[i] == '1') {
				str[i] = '0';
			} else {
				break;
			}
		}
		return new BigInteger(String.valueOf(str), 2).longValue();
	}
}
