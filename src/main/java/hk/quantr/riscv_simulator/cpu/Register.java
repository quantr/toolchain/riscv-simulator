package hk.quantr.riscv_simulator.cpu;

import java.math.BigInteger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public interface Register {

	String getHexString();

	void setValue(long value);

	void setValue(BigInteger value);

	@Override
	String toString();
	
	BigInteger getValue();
	
	public String getName();
}
