package hk.quantr.riscv_simulator.cpu;

import hk.quantr.riscv_simulator.exception.IRQException;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public interface MemoryHandler {

	public boolean isRead();

	public boolean isWrite();

	public long getAddress();

	public int getSize();

	public int read(long address);

	public void write(long address, int value);
	
//	public void write(long address, long value) throws IRQException; // 64-bits
}
