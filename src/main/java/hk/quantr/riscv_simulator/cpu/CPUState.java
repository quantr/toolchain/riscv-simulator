package hk.quantr.riscv_simulator.cpu;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CPUState {

	/*	Privilege Modes: 
		00 = User 
		01 = Supervisor 
		10 = Hypervisor 
		11 = Machine 
	 */
	public final static int USER_MODE = 0;
	public final static int SUPERVISOR_MODE = 1;
	public final static int HYPERVISOR_MODE = 2;
	public final static int MACHINE_MODE = 3;

	public static int priv = MACHINE_MODE;

	public static void setPriv(int p) {
		if (p < 4 && p >= 0) {
			priv = p;
		} else {

		}
	}

	public static void printMode() {

	}
}
