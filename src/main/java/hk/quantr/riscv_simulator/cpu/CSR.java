package hk.quantr.riscv_simulator.cpu;

import hk.quantr.riscv_simulator.Simulator;

public class CSR {

	public static long MEDELEG_MASK = 0xbfff;
	public static long MIDELEG_MASK = 0x2666;
//	public static long MIDELEG_MASK = 0x3666;
	public static long NO_MASK = 0xffffffffffffffffl;
	public static long SSTATUS_MASK = 0x3c00721888l;
	public static long SIP_MASK = 0x888l;

	public static long getMask(String csr) {
		if (csr.equals("medeleg")) {
			return MEDELEG_MASK;
		} else if (csr.equals("mideleg")) {
			return MIDELEG_MASK;
		} else if (csr.equals("sstatus")) {
			return SSTATUS_MASK;
		} else if (csr.equals("sip")) {
			return SIP_MASK;
		} else {
			return NO_MASK;
		}
	}

	public static void checkWritePermission(Simulator simulator, String csr) {
		long perm = Long.parseLong(simulator.registers.get(csr).getName(), 16);
		//According to privileged spec page 21
		if (((perm >> 10) & 3) == 3) {
			System.out.println("Error: Writing to a Read-only Register.");
			//Throws exception, but dont know what exception yet.
		}
	}

//	public static String csrToUse(String csr) {
//		if (CPUState.priv == CPUState.MACHINE_MODE) { //A template for further implementation
//			switch (csr) {
//				case "sie": return "mie";
//				case "sip": return "mip";
//			}
//		} else if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
//
//		} else if (CPUState.priv == CPUState.USER_MODE) {
//
//		} else if (CPUState.priv == CPUState.HYPERVISOR_MODE) {
//
//		}
//		return csr;
//	}
}
