package hk.quantr.riscv_simulator.cpu;

import hk.quantr.riscv_simulator.exception.IRQException;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Clint implements MemoryHandler {

	private long address = 0;
	private int size = 0;

	public Clint(long address, int size) {
		this.address = address;
		this.size = size;
	}

	@Override
	public boolean isRead() {
		return true;
	}

	@Override
	public boolean isWrite() {
		return true;
	}

	@Override
	public long getAddress() {
		return address;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public int read(long address) {
//		System.out.println("CLINT read 0x" + Long.toHexString(address));
		return -1;
	}

	@Override
	public void write(long address, int value) {
//		System.out.println("CLINT write 0x" + Long.toHexString(address) + " = " + Integer.toHexString(value));
	}

	@Override
	public String toString() {
		return "Clint{" + "address=0x" + Long.toHexString(address) + ", size=0x" + Long.toHexString(size) + '}';
	}

}
