package hk.quantr.riscv_simulator.setting;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class FireInterrupt {

	public long sequence;
	public long cause;
	public String desc;

	public long getSequence() {
		return sequence;
	}

}
