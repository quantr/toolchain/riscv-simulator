package hk.quantr.riscv_simulator.setting;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MemoryHiJack {

	public long sequence;
	public boolean memRead;
	public long memAddr;
	public long memValue;
	public int memSize;

	public long getSequence() {
		return sequence;
	}
}
