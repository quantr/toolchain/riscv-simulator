package hk.quantr.riscv_simulator.setting;

import hk.quantr.riscv_simulator.cpu.MemoryMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.commons.io.IOUtils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.security.AnyTypePermission;
import hk.quantr.riscv_simulator.LongConverter;
import hk.quantr.riscv_simulator.cpu.GeneralRegister;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;

public class Setting {

	private static Setting setting = null;

	@XStreamOmitField
	public String filename;

	public HashSet<MemoryMap> memoryMaps = new HashSet();
	public HashSet<GeneralRegister> registers = new HashSet();
	public HashSet<Memory> memory = new HashSet();
//	public HashSet<FireInterrupt> fireInterrupts = new HashSet();
//	public HashSet<MemoryHiJack> memoryHiJacks = new HashSet();

	public ArrayList<FireInterrupt> fireInterrupts = new ArrayList();
	public ArrayList<MemoryHiJack> memoryHiJacks = new ArrayList();

	public Setting() {

	}

	public static Setting getInstance() {
		return getInstance("setting.xml");
	}

	public static Setting getInstance(String filename) {
//        System.out.println("Instance : " + filename);
		if (setting == null) {
			setting = load(filename);
		}
		return setting;
	}

	public void save() {
		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.registerConverter(new LongConverter());
		xstream.alias("Setting", Setting.class);
		xstream.alias("Register", GeneralRegister.class);
		xstream.alias("MemoryMap", MemoryMap.class);
		xstream.alias("Memory", Memory.class);
		xstream.alias("FireInterrupt", FireInterrupt.class);
		xstream.alias("MemoryHiJack", MemoryHiJack.class);
		xstream.alias("GeneralRegister", GeneralRegister.class);
		String xml = xstream.toXML(this);
		try {
			IOUtils.write(xml, new FileOutputStream(new File(filename)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Setting load(String filename) {
		try {
			File file = new File(filename);
			if (!file.exists()) {
				System.err.println(filename + " not exists, simulator unable to run");
				System.exit(12);
			}
			XStream xstream = new XStream();
			xstream.addPermission(AnyTypePermission.ANY);
			xstream.autodetectAnnotations(true);
			xstream.registerConverter(new LongConverter());
			xstream.alias("Setting", Setting.class);
			xstream.alias("Register", GeneralRegister.class);
			xstream.alias("MemoryMap", MemoryMap.class);
			xstream.alias("Memory", Memory.class);
			xstream.alias("FireInterrupt", FireInterrupt.class);
			xstream.alias("MemoryHiJack", MemoryHiJack.class);
			xstream.alias("GeneralRegister", GeneralRegister.class);
			Setting setting = (Setting) xstream.fromXML(new FileInputStream(file));
			setting.filename = filename;
			if (setting.memoryMaps == null) {
				setting.memoryMaps = new HashSet();
			}
			if (setting.registers == null) {
				setting.registers = new HashSet();
			}
			if (setting.fireInterrupts == null) {
				setting.fireInterrupts = new ArrayList();
			} else {
				setting.fireInterrupts.sort(Comparator.comparingLong(FireInterrupt::getSequence));
			}
			if (setting.memoryHiJacks == null) {
				setting.memoryHiJacks = new ArrayList();
			} else {
				setting.memoryHiJacks.sort(Comparator.comparingLong(MemoryHiJack::getSequence));
			}
			return setting;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public FireInterrupt getFireInterrupt(long cpuTick) {
		for (FireInterrupt temp : fireInterrupts) {
			if (temp.sequence > cpuTick) {
				return null;
			}
//			System.out.println(temp.sequence + " " + cpuTick);

			if (temp.sequence == cpuTick) {
				fireInterrupts.remove(temp);
//				System.out.println("looped: " + i);
				return temp;
			}
		}
		return null;
	}

	public void removeFireInterrupt(long cpuTick) {
		for (FireInterrupt temp : fireInterrupts) {
			if (temp.sequence == cpuTick) {
				fireInterrupts.remove(temp);
				return;
			}
		}
	}

}
