//package hk.quantr.riscv_simulator;
//
//import com.thoughtworks.xstream.converters.basic.LongConverter;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
//public class SettingConverter extends LongConverter {
//
//	@Override
//	public boolean canConvert(Class clazz) {
//		return clazz.equals(Setting.class);
//	}
//
//	@Override
//	public Object fromString(String str) {
//		System.out.println("str=" + str);
//		Setting person = new Setting();
//		return person;
//	}
//}
