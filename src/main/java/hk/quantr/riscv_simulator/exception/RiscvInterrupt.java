package hk.quantr.riscv_simulator.exception;

/**
 *
 * @author Derek
 */
public class RiscvInterrupt extends Exception {
	
	
    public static final int SUPERVISOR_SOFTWARE_INTERRUPT = 1;
    public static final int MACHINE_SOFTWARE_INTERRUPT = 3;
    public static final int SUPERVISOR_TIMER_INTERRUPT = 5;
    public static final int MACHINE_TIMER_INTERRUPT = 7;
    public static final int USER_EXTERNAL_INTERRUPT = 8;
    public static final int SUPERVISOR_EXTERNAL_INTERRUPT = 9;
    public static final int MACHINE_EXTERNAL_INTERRUPT = 11;
	
	public int ExceptionNo = -1;
	
	public RiscvInterrupt(int exceptionNo, String msg) {
		super(msg);
		ExceptionNo = exceptionNo;
	}
}
