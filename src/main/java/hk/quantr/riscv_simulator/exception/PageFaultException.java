package hk.quantr.riscv_simulator.exception;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class PageFaultException extends Exception {
	public String message;

	public PageFaultException(String message) {
		this.message = message;
	}
	
}
