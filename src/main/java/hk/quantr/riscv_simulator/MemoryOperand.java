package hk.quantr.riscv_simulator;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MemoryOperand {

	String operand;
	long offset;
	long offsetAfterMapping;
	long b;

	public MemoryOperand(String operand, long offset, long offsetAfterMapping, long b) {
		this.operand = operand;
		this.offset = offset;
		this.offsetAfterMapping = offsetAfterMapping;
		this.b = b;
	}

	public MemoryOperand(String operand, long offset, long offsetAfterMapping) {
		this.operand = operand;
		this.offset = offset;
		this.offsetAfterMapping = offsetAfterMapping;
	}

	@Override
	public String toString() {
		return String.format("%s %x -> %x = %x", operand, offset, offsetAfterMapping, b);
	}
}
