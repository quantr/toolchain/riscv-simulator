/*
 * Copyright 2020 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.riscv_simulator;

import hk.quantr.riscv_simulator.log.H2Thread;
import hk.quantr.riscv_simulator.setting.Setting;
import hk.quantr.assembler.exception.NoOfByteException;
import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.RISCVDisassembler.DecodeType;
import hk.quantr.assembler.RISCVDisassembler.InstructionType;
import hk.quantr.assembler.exception.WrongInstructionException;
import hk.quantr.assembler.riscv.il.Line;
import hk.quantr.assembler.riscv.il.Registers;
import hk.quantr.dwarf.QuantrDwarf;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.elf.Elf_Phdr;
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.PropertyUtil;
import hk.quantr.riscv_simulator.cpu.CPUState;
import hk.quantr.riscv_simulator.cpu.CSR;
import hk.quantr.riscv_simulator.cpu.CSRRegister;
import hk.quantr.riscv_simulator.cpu.Memory;
import hk.quantr.riscv_simulator.cpu.MemoryHandler;
import hk.quantr.riscv_simulator.cpu.MemoryMap;
import hk.quantr.riscv_simulator.cpu.PMP;
import hk.quantr.riscv_simulator.cpu.GeneralRegister;
import hk.quantr.riscv_simulator.cpu.PLIC;
import hk.quantr.riscv_simulator.cpu.Register;
import hk.quantr.riscv_simulator.cpu.UART;
import hk.quantr.riscv_simulator.exception.PageFaultException;
import hk.quantr.riscv_simulator.cpu.Virtio;
import hk.quantr.riscv_simulator.exception.AccessFaultException;
import hk.quantr.riscv_simulator.exception.EndSimulatorException;
import hk.quantr.riscv_simulator.exception.IRQException;
import hk.quantr.riscv_simulator.exception.IllegalInstructionException;
import hk.quantr.riscv_simulator.exception.RiscvInterrupt;
import hk.quantr.riscv_simulator.log.FileLogThread;
import hk.quantr.riscv_simulator.log.LogInterface;
import hk.quantr.riscv_simulator.setting.FireInterrupt;
import hk.quantr.riscv_simulator.setting.MemoryHiJack;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public final class Simulator {

	private static final Logger logger = Logger.getLogger(Simulator.class.getName());
	//System structuring
	public LinkedHashMap<String, Register> registers = new LinkedHashMap<>();
	HashSet<MemoryHandler> memoryHandlers = new HashSet<>();
	public Memory memory = new Memory(this, memoryHandlers);
	public int Harts = 1;
	//System variables
	RISCVDisassembler riscvDisassembler = new RISCVDisassembler();
	LinkedHashMap<String, Integer> bp = new LinkedHashMap<>();
	public CommandLine cmd;
	int bp_flag = 0, startAddress;
	String dumpJson;
	boolean isXml, startCont = false, interrupt = false;
	Thread c2;
	long cpuTick = 0;
	ArrayList<Dwarf> dwarfArrayList;
	private boolean printDisassembleLine;
	public long mtime;
	public long mtimecmp;
	LogInterface logThread;
	public static DecimalFormat decimalFormatter = new DecimalFormat("#,###");
	public static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	public FireInterrupt pendingInterrupt;

	/* last mem */
	boolean mem = false;
	String memOperation = null;
	boolean memRead = false;
	long memAddr = 0;
	long memValue = 0;
	int memSize = 0;

	/* end last mem */
	static {
		InputStream stream = Simulator.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String args[]) throws Exception {
		new Simulator(args);
	}

	public Simulator(String[] args) throws Exception {
		if (args != null) {
			CommandLineParser parser = new DefaultParser();
			Options options = new Options();
			try {
				options.addOption("v", "version", false, "display version info");
				options.addOption(Option.builder("b")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("load binary file")
						.longOpt("binary")
						.build());
				options.addOption(Option.builder("e")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("load elf file")
						.longOpt("elf")
						.build());
				options.addOption(Option.builder("s")
						.required(false)
						.hasArg()
						.argName("address")
						.desc("start memory address")
						.longOpt("start")
						.build());
				options.addOption(Option.builder("d")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("dump each execution into json")
						.longOpt("dump")
						.build());
				options.addOption(Option.builder("f")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("file system image")
						.longOpt("file")
						.build());
				options.addOption(Option.builder("i")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("load init register values from file")
						.longOpt("init")
						.build());
				options.addOption(Option.builder("2")
						.required(false)
						.hasArg()
						.argName("h2 db file")
						.desc("store execution records into h2")
						.longOpt("h2")
						.build());
				options.addOption(Option.builder("g")
						.required(false)
						.hasArg()
						.argName("log file")
						.desc("store execution records into file")
						.longOpt("logfile")
						.build());
				options.addOption(Option.builder("c")
						.required(false)
						.hasArg()
						.argName("commands")
						.desc("commands separated by comma")
						.longOpt("commands")
						.build());
				options.addOption(Option.builder()
						.required(false)
						.hasArg()
						.argName("cputicks")
						.desc("only log for cpu ticks")
						.longOpt("cputicks")
						.build());
				if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
					HelpFormatter formatter = new HelpFormatter();
					formatter.printHelp("java -jar riscv_simulator-xx.jar [OPTION]", options);
					return;
				}

				if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
					System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));
					return;
				}

				cmd = parser.parse(options, args);

				if (!cmd.hasOption("b") && !cmd.hasOption("e")) {
					System.out.println("Please specify --binary or --elf");
					return;
				}
				if (cmd.hasOption("cputicks")) {
					String temp = cmd.getOptionValue("cputicks");
					for (String s : temp.split(",")) {
						s = s.trim();
						if (s.contains("-")) {
							int from = Integer.parseInt(s.split("-")[0].trim());
							int to = Integer.parseInt(s.split("-")[1].trim());
							System.out.println("cpu ticks : " + from + " - " + to);
							LogTicks.data.add(Pair.of(from, to));
						}
					}
				}

				if (cmd.hasOption("dump")) {
					String temp = cmd.getOptionValue("dump");
					dumpJson = temp;
					if (new File(dumpJson).isFile()) {
						new File(dumpJson).delete();
					}

					FileUtils.writeStringToFile(new File(dumpJson), "[", "utf8", true);
				}
				if (cmd.hasOption("start")) {
					startAddress = (int) CommonLib.convertFilesize(cmd.getOptionValue("start"));
				}
				if (cmd.hasOption("init")) {
					String filename = cmd.getOptionValue("init");
					Setting setting = Setting.getInstance(filename);
				}
			} catch (ParseException | IOException ex) {
				logger.log(Level.SEVERE, ExceptionUtils.getFullStackTrace(ex));
				System.exit(1);
			}

			initRegistersAndMemory();

			if (cmd.hasOption("2")) {
				String temp = cmd.getOptionValue("h2");
//				File h2 = new File(temp);
//				if (h2.isFile()) {
//					h2.delete();
//				}
//				String filenameOnly = h2.getName().split("\\.")[0];
				if (temp.endsWith(".mv.db")) {
					temp = temp.replaceAll(".mv.db", "");
				}
				logThread = new H2Thread(this, temp);
				new Thread(logThread, "Log Thread").start();
			} else if (cmd.hasOption("logfile")) {
				logThread = new FileLogThread(cmd.getOptionValue("logfile"));
				new Thread(logThread, "Log Thread").start();
			}

			initKernel();
			initMemoryHandler();
			startSimulation();
		}
	}

	public void startSimulation() throws IOException, InterruptedException {
		String prevCommand = "";
		int x_n = 1;
		char x_u = 'b', x_f = 'x';
//		if (!Startcont) {
//			if (dumpJson != null) {
//				dumpToJson();
//			}
//			if (logThread != null) {
//				dumpToLog(cpuTick);
//			}
//			cpuTick++;
//		}
//		Scanner in = new Scanner(System.in);
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		AtomicBoolean shouldStop = new AtomicBoolean();
		outer:
		while (true) {
			System.out.print(registers.get("pc").getHexString() + " >");
			while (!in.ready() && !shouldStop.get()) {
				Thread.sleep(200);
			}
			if (shouldStop.get()) {
				break outer;
			}
			String commands[] = cmd.hasOption("commands") ? cmd.getOptionValue("commands").split(";") : in.readLine().trim().split(";");
			try {
				for (String command : commands) {
					if (command.equals("q")) {
						break outer;
					}
					if (!command.isBlank()) {
						prevCommand = command;
					} else {
						command = prevCommand;
					}
					if (bp_flag == 1) {//disable breakpoint ,pc jump to next instruction
						bp_flag = 0;
					}
					command = command.replaceAll("( )+", " ");
					if (command.equals("v")) {
						if (isXml) {
							System.out.println("<version>\n\t " + PropertyUtil.getProperty("main.properties", "version") + "\n</version>");
						} else {
							System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));
						}
					} else if (command.equals("priv")) {
						if (isXml) {
							System.out.println(String.format("<Privilege>\n<priv>%d</priv>\n<mode>%s</mode>\n</Privilege>", CPUState.priv, CPUState.priv == 3 ? "Machine" : CPUState.priv == 2 ? "Hypervisor" : CPUState.priv == 1 ? "Supervisor" : "User"));
						} else {
							System.out.println(String.format("Privilege: %d [%s]", CPUState.priv, CPUState.priv == 3 ? "Machine" : CPUState.priv == 2 ? "Hypervisor" : CPUState.priv == 1 ? "Supervisor" : "User"));
						}
					} else if (command.matches("xml( (on|off))?")) {
						if (command.equals("xml")) {
							isXml = !isXml;
						} else if (command.split(" ")[1].equals("on")) {
							isXml = true;
						} else {
							isXml = false;
						}
						System.out.println(isXml ? "on" : "off");
					} else if (command.startsWith("r")) {
						String[] registerNames;
						if (command.equals("r")) {
							registerNames = registers.keySet().stream().filter(x -> (!x.startsWith("x") && !x.startsWith("fs") && !x.startsWith("fa") && !x.startsWith("ft"))).collect(Collectors.toList()).toArray(new String[0]);
						} else if (command.startsWith("r info")) { //command: r info [csr ...]
							String[] reg = command.replaceAll("[ ,]+", " ").substring(7).split(" ");
							for (String r : reg) {
								if (registers.get(r) == null) {
									System.out.println(String.format("%15s = %16s ", r, "Unrecognized reg"));
									continue;
								}
								System.out.println(String.format("%15s = %016x ", r, registers.get(r).getValue().longValue()));
								printRegisterInfo(r);
							}
							continue;
						} else {
							command = command.replaceAll("[ ,]+", " ").substring(2);
							registerNames = command.split(" ");
						}
						int perCol = 5;
						if (isXml) {
							System.out.println("<registers>");
							for (int x = 0; x < registerNames.length; x++) {
								String registerName = registerNames[x];
								if (registers.get(registerName) == null) {
									System.out.println(String.format("\t<%s>%s<%s>", registerName, "Register does not exist.", registerName));
									continue;
								}
								System.out.print(String.format("\t<%s>%016x</%s>\n", registerName, registers.get(registerName).getValue().longValue(), registerName));
							}
							System.out.println("</registers>");
						} else {
							for (int x = 0; x < registerNames.length; x += perCol) {
								for (int y = 0; y < perCol; y++) {
									if (x + y >= registerNames.length) {
										break;
									}
									String registerName = registerNames[x + y];
									if (registers.get(registerName) == null) {
										System.out.print(String.format("%15s = %16s ", registerName, "Unrecognized reg"));
										continue;
									}
									System.out.print(String.format("%15s = %016x ", registerName, registers.get(registerName).getValue().longValue()));
								}
								System.out.println();
							}
						}

					} else if (command.equals("s") || command.matches("s [0-9]+[kmb]?")) {
						int steps = 1;
						if (command.endsWith("k")) {
							command = command.substring(0, command.length() - 1);
							steps = Integer.parseInt(command.split(" ")[1]) * 1000;
						} else if (command.endsWith("m")) {
							command = command.substring(0, command.length() - 1);
							steps = Integer.parseInt(command.split(" ")[1]) * 1000000;
						} else if (command.endsWith("b")) {
							command = command.substring(0, command.length() - 1);
							steps = Integer.parseInt(command.split(" ")[1]) * 1000000000;
						} else if (command.matches("s [0-9]+")) {
							steps = Integer.parseInt(command.split(" ")[1]);
						}
						for (int i = 0; i < steps; i++) {
							if (bp_flag == 0) {
								executeCPU();
							}
						}
					} else if (command.startsWith("x")) {
						String[] split_text = command.split(" ");
						String addr = null;

						if (split_text.length >= 2) {
							String second_text = split_text[1];
							if (split_text.length > 2) {
								addr = split_text[2];
							}
							if (second_text.charAt(0) == '/') {
								if (second_text.matches("/[0-9]+[bhwg]{1}[xduot]{1}")) {
									x_n = Integer.parseInt(second_text.replaceAll("\\D", ""));
									x_u = getUnitsize(second_text);
									x_f = getPrintformat(second_text);
								} else if (second_text.matches("/[0-9]+")) {
									x_n = Integer.parseInt(second_text.replaceAll("\\D", ""));
								} else if (second_text.matches("/[bhwg]{1}")) {
									x_u = getUnitsize(second_text);
								} else if (second_text.matches("/[xduot]{1}")) {
									x_f = getPrintformat(second_text);
								} else if (second_text.matches("/[0-9]+[bhwg]{1}")) {
									x_n = Integer.parseInt(second_text.replaceAll("\\D", ""));
									x_u = getUnitsize(second_text);
								} else if (second_text.matches("/[0-9]+[xduot]{1}")) {
									x_n = Integer.parseInt(second_text.replaceAll("\\D", ""));
									x_f = getPrintformat(second_text);
								} else if (second_text.matches("/[bhwg]{1}[xduot]{1}")) {
									x_u = getUnitsize(second_text);
									x_f = getPrintformat(second_text);
								} else {
									System.out.println("illegal instruction x  /nuf addr\nn ( Count of how many units to display)\nu - Unit size [b Individual bytes ,h Halfwords (2 bytes) ,w Words (4 bytes) , g Giant words (8 bytes)]\nf - Printing format[ x (Print in hexadecimal) ,d (Print in decimal) ,u (Print in unsigned decimal) ,o (Print in octalt) ,t (Print in binary)]\n");
									continue;
								}
							} else if (second_text.startsWith("0x")) {
								addr = split_text[1];
							} else {
								System.out.println("illegal instruction x  /nuf addr\nn ( Count of how many units to display)\nu - Unit size [b Individual bytes ,h Halfwords (2 bytes) ,w Words (4 bytes) , g Giant words (8 bytes)]\nf - Printing format[ x (Print in hexadecimal) ,d (Print in decimal) ,u (Print in unsigned decimal) ,o (Print in octalt) ,t (Print in binary)]\n");
								continue;
							}
							if (addr != null) {
								handleManipulatingMemory(CommonLib.string2long(addr), x_n, x_u, x_f);
							}
						}
						if (addr == null) {
							handleManipulatingMemory(registers.get("pc").getValue().longValue(), x_n, x_u, x_f);
						}
						System.out.println();
					} else if (command.matches("break 0x[[0-9][a-f][A-F]]+")) {
						String break_memory = command.split(" ")[1].substring(2);
						bp.put(break_memory, 1);
						System.out.println("Set breakpoint successfully");
					} else if (command.equals("info break")) {
						Iterator iterator = bp.entrySet().iterator();
						int count = 0;
						while (iterator.hasNext()) {
							Map.Entry entry = (Map.Entry) iterator.next();
							String key = (String) entry.getKey();
							System.out.println("b" + count + ". 0x" + key);
							count++;
						}
						if (count == 0) {
							System.out.println("There is no breakpoint");
						}
					} else if (command.matches("delete [0-9]+")) {
						Iterator iterator = bp.entrySet().iterator();
						int count = 0;
						int num = Integer.parseInt(command.split(" ")[1]);

						while (iterator.hasNext()) {
							Map.Entry entry = (Map.Entry) iterator.next();
							String key = (String) entry.getKey();
							if (num == count) {
								bp.remove(key);
								System.out.println("Delete breakpoint successfully");
								break;
							}
							count++;
						}
						if (bp.size() == 0) {
							System.out.println("There is no breakpoint");
						}
					} else if (command.startsWith("set")) {
						//Two kinds of usage, set registers or memory
						String[] split_text = command.replaceAll("[ ,]+", " ").split(" ");
						if (split_text.length <= 2) {
							System.out.println("Incorrect format: Try again with \"set <register | memory> [value]\"\n");
							continue;
						}
						if (!Character.isDigit(split_text[1].charAt(0))) { //Set register if second text isnt memory region
							long val = new BigInteger(split_text[2].startsWith("0x") ? split_text[2].substring(2) : split_text[2], 16).longValue();
							registers.get(split_text[1]).setValue(val);
							if (split_text[1].startsWith("pmp")) {
								PMP.updatePmp(this, split_text[1]);
							}
							System.out.println("register " + split_text[1] + " is set!");
						} else {
							ArrayList<Long> data = new ArrayList<>();
							if (split_text.length >= 3) {
								long addr = (split_text[1].startsWith("0x")) ? new BigInteger(split_text[1].substring(2), 16).longValue() : new BigInteger(split_text[1], 10).longValue();
								for (int i = 2; i < split_text.length; i++) {
									split_text[i] = (split_text[i].startsWith("0x")) ? split_text[i].substring(2) : Long.toHexString(new BigInteger(split_text[i]).longValue());
									if (split_text[i].length() % 2 == 1) {
										split_text[i] = "0" + split_text[i];
									}
									int length = split_text[i].length();
									while (length != 0) {
										data.add(Long.parseLong(split_text[i].substring(length - 2, length), 16));
										length -= 2;
									}
								}
								for (int i = 0; i < data.size(); i++) {
									memory.writeByte(addr + i, (byte) (data.get(i) & 0xff), true, false);
								}
							}
						}
					} else if (command.startsWith("fill")) {
						String[] split_text = command.split(" ");
						long start = Long.parseLong(split_text[1].substring(2), 16);
						long dest = Long.parseLong(split_text[2].substring(2), 16);
						long size = dest - start;
						long value = Long.parseLong(split_text[3].substring(2), 16);
						for (int i = 0; i < size; i++) {
							memory.writeByte(start + i, (byte) (value & 0xff), true, false);
						}
					} else if (command.startsWith("compare")) {
						String[] split_text = command.split(" ");
						long compare_length = 64;
						if (split_text.length > 3) {
							if (split_text[3].startsWith("0x")) {
								compare_length = new BigInteger(split_text[3].substring(2), 16).longValue();
							} else {
								compare_length = new BigInteger(split_text[3], 10).longValue();
							}
						}
						long res1 = memory.readGodMode(CommonLib.string2long(split_text[1]), 8);
						long res2 = memory.readGodMode(CommonLib.string2long(split_text[2]), 8);
						if (compare_length != 64) {
							if ((res1 & ((1L << compare_length) - 1)) == (res2 & ((1L << compare_length) - 1))) {
								System.out.println("equal");
							} else {
								System.out.println("not equal");
							}
						} else {
							if (res1 == res2) {
								System.out.println("equal");
							} else {
								System.out.println("not equal");
							}
						}
					} else if (command.equals("cont") || command.equals("c")) {
						bp_flag = 0;
						System.out.println("Continued. Type \"pause\" or \"p\" anytime to stop.");

						c2 = new Thread() {
							@Override
							public void run() {
								try {
									while (bp_flag == 0) {
										executeCPU();
									}
								} catch (EndSimulatorException ex) {
									shouldStop.set(true);
								}
							}
						};
						c2.start();
					} else if (command.equals("pause") || command.equals("p")) {
						bp_flag = 1;
						System.out.println("Paused.");
					} else if (command.matches("(disasm|d)( 0x[0-9a-fA-F]+[,]?)?( [0-9]+)?")) {
						String[] split_text = command.replaceAll("[ ,]+", " ").split(" ");
						RISCVDisassembler riscv = new RISCVDisassembler();
						int n = 1;
						long address = registers.get("pc").getValue().longValue();
						if (split_text.length > 1) {
							if (split_text[1].startsWith("0x")) {
								address = new BigInteger(split_text[1].substring(2), 16).longValue();
								if (split_text.length > 2) {
									n = Integer.parseInt(split_text[2]);
								}
							} else if (split_text[1].matches("\\d+")) {
								n = Integer.parseInt(split_text[1]);
							}
						}
						if (isXml) {
							System.out.println("<disassembler>");
						}
						for (int i = 0; i < n; i++) {
							String out = Long.toHexString(address);
							if (isXml) {
								System.out.print("\t<name>" + "0".repeat(8 - out.length()) + out + "!");
							} else {
								System.out.print("0x" + "0".repeat(8 - out.length()) + out + ": ");
							}

							int noOfByte = RISCVDisassembler.getNoOfByte(memory.read(address, true, false) & 0x7f);
							riscv.setCurrentOffset(address);
							long t;
							int[] ins;
							if (noOfByte == 4) {
								ins = new int[4];
								ins[0] = memory.read(address, true, false) & 0xff;
								ins[1] = memory.read(address + 1, true, false) & 0xff;
								ins[2] = memory.read(address + 2, true, false) & 0xff;
								ins[3] = memory.read(address + 3, true, false) & 0xff;
								t = address + 4;
							} else {
								ins = new int[2];
								ins[0] = memory.read(address, true, false) & 0xff;
								ins[1] = memory.read(address + 1, true, false) & 0xff;
								t = address + 2;
							}
							if (isXml) {
								System.out.print(CommonLib.arrayToHexString(ins) + "!");
							}
							disassembleAddress(address, ins);
							address = t;
							if (isXml) {
								System.out.print("</name_" + "0".repeat(8 - out.length()) + out + ">\n");
							} else {
								System.out.println();
							}
						}
						if (isXml) {
							System.out.println("</disassembler>");
						}
					} else if (command.startsWith("find")) {
						String[] threeSegment = command.replaceAll("[ ,]+", " ").substring(5).split(" ");

						long start = Long.parseLong(threeSegment[0].substring(2), 16);
						long end = Long.parseLong(threeSegment[1].substring(2), 16);
						ArrayList<Long> data = new ArrayList<>();
						ArrayList<String> word = new ArrayList<>();
						ArrayList<Integer> skipping = new ArrayList<>();
						long target = 0;
						boolean skip = false;
						if (isXml) {
							System.out.println("<matchedMemory>");
						}
						for (int j = 2; j < threeSegment.length; j++) {
							String str_target = threeSegment[j];
							//check if there is "?"
							if (str_target.contains("?")) {
								skip = true;

							}
							if (str_target.matches("0x[[0-9][a-f][A-F]]+")) {
								word.add(str_target);
							} else {
								for (int x = 0; x < str_target.length(); x++) {
									char ch = str_target.charAt(x);
									word.add(String.valueOf(ch));
								}
							}
						}
						if (skip) {
							for (int x = 0; x < word.size(); x++) {
								if (word.get(x).equals("?")) {
									skipping.add(1);
								} else {
									skipping.add(0);
								}
							}
						}
						for (int j = 0; j < word.size(); j++) {
							String str_target = word.get(j);
							if (str_target.matches("0x[[0-9][a-f][A-F]]+")) {
								target = Long.parseLong(str_target.substring(2), 16);
								data.add(target);
							} else {
								target = str_target.charAt(0);
								data.add(target);
							}
						}
						for (int i = 0; i < end - start + 1; i++) {
							int j = 0;
							boolean boo = false;
							int address = (int) (start + i);
							if (skip) {
								do {
									if (skipping.get(j) == 1) {

										j++;
										boo = true;
										continue;
									}
									long temp_addrvalue = (memory.read(address + j, true, false) & 0xff);
									long temp_listvalue = data.get(j);
									if (temp_addrvalue == temp_listvalue) {
										boo = true;
									} else {
										boo = false;
									}
									j++;
								} while (j < data.size() && boo == true);
							} else {
								do {
									long temp_addrvalue = (memory.read(address + j, true, false) & 0xff);
									long temp_listvalue = data.get(j);
									if (temp_addrvalue == temp_listvalue) {
										boo = true;
									} else {
										boo = false;
									}
									j++;
								} while (j < data.size() && boo == true);
							}
							if (boo) {
								for (j = 0; j < data.size(); j++) {
									if (isXml) {
										System.out.println(String.format("<address target=\"%d\">%s</address>", (data.get(j)), "0x" + Integer.toHexString(address + j)));
									} else {
										System.out.println("0x" + Integer.toHexString(address + j));
									}
								}
							}
						}
						if (isXml) {
							System.out.println("</matchedMemory>");
						}
					} else if (command.startsWith("paging")) {
						command = command.replaceAll("[ ,]+", " ");
						String[] split_text = command.split(" ");
						long satp = registers.get("satp").getValue().longValue();
						long mode = satp >>> 60, asid = (satp & 0xffff00000000000L) >> 44, physicalPageNumber = satp & 0xfffffffffffL;
						if (command.equals("paging")) {
							System.out.println(String.format("satp: 0x%016x\n  +------+------+----------------------+\n  |63  60|59  44|43                   0|\n  | mode | ASID | physical page number |\n  | 0x%x  | 0x%x  | 0x%011x        |\n  +------+------+----------------------+\n", satp, mode, asid, physicalPageNumber));
							if (satp != 0) { //command "check paging" will return paging mode and status
								System.out.print("paging: on\nmode  : ");
								if (mode == 8) {
									System.out.println("sv39");
								} else if (mode == 9) {
									System.out.println("sv48");
								} else if (mode == 0) {
									System.out.println("No protection or no translation");
								} else {
									System.out.println("Unrecognized");
								}
							} else {
								System.out.println("paging is off\nTurn on paging by \"set satp [value]\". [hint: mode 8 = sv39, mode 9 = sv48, mode 0 = no translation]");
							}
						} else if (command.matches("paging sv39 address (0x)?[0-9a-fA-F]+")) {
							long va_Value;
							if (split_text[3].startsWith("0x")) {
								va_Value = new BigInteger(split_text[3].substring(2), 16).longValue();
							} else {
								va_Value = new BigInteger(split_text[3], 10).longValue();
							}
							long pageOffset = va_Value & 0xFFF, vpn2 = (va_Value >> 30) & 0x1FF, vpn1 = (va_Value >> 21) & 0x1FF, vpn0 = (va_Value >> 12) & 0x1FF;
							System.out.println(String.format("VPN[2] | VPN[1] | VPN[0] | page offset\n0x%03x  | 0x%03x  | 0x%03x  | 0x%03x", vpn2, vpn1, vpn0, pageOffset));
						} else if (command.matches("paging sv39 dump .*")) {
							if (mode == 8) { //must be in sv39 mode to do the following commands
								long start_address = (split_text[3].startsWith("0x")) ? new BigInteger(split_text[3].substring(2), 16).longValue() : physicalPageNumber << 12;
								int start_index = (split_text[3].startsWith("0x")) ? 4 : 3;
								System.out.println(String.format("Page table starting address: 0x%x", start_address));
								for (int i = start_index; i < split_text.length; i++) {
									long line;
									if (split_text[i].matches("[0-9]+") && (line = Long.valueOf(split_text[i])) < 512) {
										long pte = memory.readGodMode(start_address + line * 8, 8);
										long pte_res = (pte >> 54) & 0x3ff, pte_ppn2 = (pte >> 28) & 0x3ffffff, pte_ppn1 = (pte >> 19) & 0x1ff, pte_ppn0 = (pte >> 10) & 0x1ff, pte_rsw = (pte >> 8) & 0x3, pte_d = (pte >> 7) & 1, pte_a = (pte >> 6) & 1, pte_g = (pte >> 5) & 1, pte_u = (pte >> 4) & 1, pte_x = (pte >> 3) & 1, pte_w = (pte >> 2) & 1, pte_r = (pte >> 1) & 1, pte_v = pte & 1;
										if (i == start_index) {
											System.out.println("                                |63      54|53       28|27   19|18   10|9   8|7|6|5|4|3|2|1|0|\n                                | reserved |   PPN2    | PPN1  | PPN0  | RSW |D|A|G|U|X|W|R|V|");
										}
										System.out.println(String.format("entry%3d: 0x%016x -> |  0x%03x   | 0x%07x | 0x%03x | 0x%03x | 0x%x |%d|%d|%d|%d|%d|%d|%d|%d|", Long.valueOf(split_text[i]), pte, pte_res, pte_ppn2, pte_ppn1, pte_ppn0, pte_rsw, pte_d, pte_a, pte_g, pte_u, pte_x, pte_w, pte_r, pte_v));
									} else {
										System.out.println("Error Page entry " + split_text[i]);
									}
								}
							} else {
								System.out.println("Turn on sv39 paging mode to continue. Type \"check paging\" for more.");
							}
						} else if (command.matches("paging sv39 translate (0x)?[0-9a-fA-F]+")) {
							long trans_value;
							if (mode == 8) {
								trans_value = split_text[3].startsWith("0x") ? new BigInteger(split_text[3].substring(2), 16).longValue() : new BigInteger(split_text[3], 16).longValue();
								System.out.println(String.format("VA: %016x\nPA: %016x ", trans_value, memory.getMappedAddress(trans_value, true)));
							} else {
								System.out.println("Turn on sv39 paging mode to continue. Type \"check paging\" for more.");
							}
						} else {
							System.out.println("Unrecognized Command. Try again by:\n"
									+ "\tpaging sv39 address [virtual address]\n"
									+ "\tpaging sv39 dump [entry number(s)]\n"
									+ "\tpaging sv39 dump [starting address] [entry number(s)]\n"
									+ "\tpaging sv39 translate [virtual address]");
						}
					} else if (command.startsWith("pmp")) {
						if (command.equals("pmp")) {
							PMP.printAllPMPSettings();
						} else {
							String[] split_text = command.replaceAll("[ ,]+", " ").split(" ");
							for (int i = 1; i < split_text.length; i++) {
								int index;
								if (split_text[i].matches("\\d+") && (index = Integer.parseInt(split_text[i])) >= 0 && Integer.parseInt(split_text[i]) < 16) {
									PMP.printPMP(index);
								} else {
									System.out.println("Unrecognized register pmpaddr" + split_text[i] + ". Try again with pmp [0-15]...");
								}
							}
						}
					} else if (command.matches("line( (on|off))?")) {
						if (command.equals("line")) {
							printDisassembleLine = !printDisassembleLine;
						} else if (command.split(" ")[1].equals("on")) {
							printDisassembleLine = true;
						} else {
							printDisassembleLine = false;
						}
						System.out.println(printDisassembleLine ? "on" : "off");
					} else if (command.equals("memoryhistory")) {
						if (memory.operands.isEmpty()) {
							System.out.println("memory.operands is empty");
						} else {
							for (MemoryOperand temp : memory.operands) {
								System.out.println(temp);
							}
						}
					} else if (command.equals("time")) {
						System.out.println(sdf.format(new Date()));
					} else if (command.equals("log")) {
						if (logThread == null) {
							System.out.println("log is off");
						} else {
							System.out.println("log to " + logThread);
						}
					} else if (command.equals("log off")) {
						logThread = null;
						System.out.println("ok");
					} else if (command.equals("cputick")) {
						System.out.println(cpuTick);
					} else if (command.equals("mem host")) {
						long heapSize = Runtime.getRuntime().totalMemory() / 1024 / 1024 / 1024;
						long freeHeapSize = Runtime.getRuntime().freeMemory() / 1024 / 1024 / 1024;
						System.out.println((heapSize - freeHeapSize) + " / " + heapSize + " = " + (float) (heapSize - freeHeapSize) / heapSize);
					} else if (command.equals("mem guest")) {
						for (MemoryHandler handler : this.memoryHandlers) {
							System.out.println(handler);
						}
					} else if (command.equals("gc")) {
						System.gc();
						long heapSize = Runtime.getRuntime().totalMemory() / 1024 / 1024 / 1024;
						long freeHeapSize = Runtime.getRuntime().freeMemory() / 1024 / 1024 / 1024;
						System.out.println((heapSize - freeHeapSize) + " / " + heapSize + " = " + (float) (heapSize - freeHeapSize) / heapSize);
					} else if (command.equals("help")) {
						System.out.println("RISC-V Simulator || Commands\n-------------------------------------------------");
						System.out.println("Basic Usage: ");
						commandHelper("q", "Exit Simulator.");
						commandHelper("v", "Version of simulator.");
						commandHelper("s [num]?", "Single step num/1 instruction(s).");
						commandHelper("c / cont", "Continue simulation.");
						commandHelper("p / pause", "Pause simulation.");
						commandHelper("line [on/off]?", "Print out disassemble information during step simulation");
						commandHelper("xml [on/off]?", "Print output in xml format.");
						commandHelper("r [regs]?", "Print register values. eg: r a1,x3");
						commandHelper("r info [csr]", "Print specific information on CSR registers. e.g. r info mstatus");
						commandHelper("x /[n]?[u]?[f]? 0x[addr]?", "Print memory according to user specified formats.");
						commandHelperUsage("n    - Number of units to display [integer].");
						commandHelperUsage("u    - Unit size. [b (byte), h (halfword/2 bytes), w (words/4 bytes), g (giant words/8 bytes)]");
						commandHelperUsage("f    - Printing format. [x (hexadecimal), d (decimal), u (unsigned decimal), o (octal), t (binary)]");
						commandHelperUsage("addr - Starting address in hexadecimal or defaulted as pc if not specified.");
						commandHelperUsage("eg: [ x /10bx 0x1000 ] will result in printing 10 bytes in hexadecimal format starting from 0x1000 ");
						commandHelper("set [reg] [val]", "Set value for a register.");
						commandHelper("set [mem] [vals]", "Set values for memory.");
						commandHelperUsage("eg: [ set 0x1000 0x12345678, 0x55, 0x44 ] will result in storing 0x445512345678 in 0x1000");
						commandHelper("fill", "Dont know what it do.");
						commandHelper("compare [A] [B] [len]?", "Compare memory region A and memory region B in terms of len/64 bits.");
						commandHelper("find", "Dont know what it do.");
						commandHelper("paging", "Check status and mode of paging.");
						commandHelper("paging sv39 address [va]", "Get virtual page number and offset of specified Virtual Address at mode sv39.");
						commandHelper("paging sv39 dump [addr]? [entries]", "Get page table entries at given page table starting address at mode sv39.");
						commandHelperUsage("If [addr] isn't specified, starting address will be default to physical address stated at satp register.");
						commandHelper("paging sv39 translate [va]", "Translate Virtual Address to Physical Address at mode sv39.");
						commandHelper("pmp [entries]?", "Print specific/all Physical Memory Protection entries.");
						commandHelper("priv", "Print current Privilege mode.");
						commandHelper("break [addr]", "Set a Breakpoint at specified address");
						commandHelper("info break", "List information of all existing breakpoints");
						commandHelper("delete [bp_num]", "Delete the breakpoint with index of bp_num");
						commandHelper("disasm/d [addr]?, [num]?", "Disassembles num/1 instruction(s) starting from addr/pc.");
						commandHelper("time", "Print current time.");
						commandHelper("log", "Print log db path.");
						commandHelper("log off", "Turn off log to db.");
						commandHelper("cputick", "show current cputick.");
						commandHelper("mem host", "show memory info.");
						commandHelper("mem guest", "show memory info.");
						commandHelper("gc", "run gc.");
					} else {
						System.out.println("Unrecognized Command.");
					}
				}
			} catch (PageFaultException ex) {
				ex.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		if (dumpJson != null && new File(dumpJson).exists()) {
			FileUtils.writeStringToFile(new File(dumpJson), "]", "utf8", true);
			zip(new File(dumpJson));
		}
		if (logThread != null) {
			logThread.stop();
		}
		System.out.println("quit");
		if (!startCont) {
			System.exit(0);
		}
	}

	public void commandHelper(String command, String help) {
		System.out.println(String.format("%-37s : %s", command, help));
	}

	public void commandHelperUsage(String help) {
		System.out.println(String.format("%-37s  -> %s", "", help));
	}

//	public long getSpecificBits(long val, int startingBit, int length) {
//		return (length == 64) ? val & 0xffffffffffffffffL : (val >>> startingBit) & ((1L << length) - 1);
//	}
	public static String centerString(int width, String s) {
		return String.format("%-" + width + "s", String.format("%" + (s.length() + (width - s.length()) / 2) + "s", s));
	}

	public void generateString(long value, String bits, String names) {
		String[] bitArray = bits.split("\\|");
		String[] nameArray = names.split("\\|");
		String bitLine = "\t|", nameLine = "\t|", valLine = "\t|";
		for (int i = 0; i < bitArray.length; i++) {
			String bitStr = bitArray[i], name = nameArray[i];
			int s = (bitStr.contains("-")) ? Integer.parseInt(bitStr.split("-")[1]) : Integer.parseInt(bitStr);
			int e = (bitStr.contains("-")) ? Integer.parseInt(bitStr.split("-")[0]) : s;
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			int length = e - s + 1;
			int bitLength = CommonLib.ceilDiv(length, 4);
			int nameLength = name.length();
			int clamp = Math.max(bitStr.length(), Math.max((bitLength != 1) ? bitLength + 2 : bitLength, (nameLength != 1) ? nameLength + 2 : nameLength));
			if (clamp == 1) {
				bitLine += String.format("%d|", s);
				nameLine += String.format("%s|", name);
				valLine += String.format("%x|", CommonLib.getValue(value, s, s + length - 1));
			} else {
				bitLine += (s != e) ? String.format("%-" + clamp + "s|", String.format("%d %" + (clamp - Integer.toString(e).length() - 1) + "d", e, s)) : String.format("%-" + clamp + "s|", centerString(clamp, Integer.toString(s)));
				nameLine += String.format("%-" + clamp + "s|", centerString(clamp, name));
				valLine += String.format("%-" + clamp + "s|", centerString(clamp, String.format("%0" + bitLength + "x", CommonLib.getValue(value, s, s + length - 1))));
			}
		}
		System.out.println(bitLine + "\n" + nameLine + "\n" + valLine);
	}

	public void printRegisterInfo(String reg) {
		long value = registers.get(reg).getValue().longValue();
		if (reg.equals("mstatus")) {
			generateString(value, "63|62-38|37|36|35-34|33-32|31-23|22|21|20|19|18|17|16-15|14-13|12-11|10-9|8|7|6|5|4|3|2|1|0", "SD|WPRI|MBE|SBE|SXL|UXL|WPRI|TSR|TW|TVM|MXR|SUM|MPRV|XS|FS|MPP|VS|SPP|MPIE|UBE|SPIE|WPRI|MIE|WPRI|SIE|WPRI");
		} else if (reg.equals("misa")) {
			generateString(value, "63-62|61-26|25-0", "MXL|0");
		}
	}

	public void disassembleAddress(long address, int[] ins) {
		DecodeType type;
		Line line;
		RISCVDisassembler riscvDisassembler = new RISCVDisassembler();
		try {
			type = RISCVDisassembler.getDecodeType(memory.read(address, true, false) & 0x7F);

			if (type == DecodeType.decodeTypeB) {
				line = riscvDisassembler.decodeTypeB(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.decodeTypeI) {
				line = riscvDisassembler.decodeTypeI(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.decodeTypeS) {
				line = riscvDisassembler.decodeTypeS(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.decodeTypeR) {
				line = riscvDisassembler.decodeTypeR(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.decodeTypeU) {
				line = riscvDisassembler.decodeTypeU(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU) {
				line = riscvDisassembler.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.magic) {
				line = riscvDisassembler.decodeMagic(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.jal) {
				line = riscvDisassembler.jal(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.jalr) {
				line = riscvDisassembler.jalr(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.fence) {
				line = riscvDisassembler.fence(ins);
				System.out.print(line.code);
			} else if (type == DecodeType.compressed) {
				if (ins[0] == 0 && ins[1] == 0) {
					System.out.print("unimp");
				} else {
					line = riscvDisassembler.decodeCompressed(ins);
					System.out.print(line.code);
				}
			} else if (type == DecodeType.unimp) {
				line = riscvDisassembler.decodeUnimp(ins);
				System.out.print(line.code);
			}
		} catch (PageFaultException e) {
			System.out.println("Page Fault");
		} catch (WrongInstructionException e) {
			System.out.println("Unrecognized Instruction");
		}
	}

	public void handleManipulatingMemory(long address, int x_n, char x_u, char x_f) {
		String output = null;
		int numBits = x_u == 'b' ? 8 : x_u == 'h' ? 16 : x_u == 'w' ? 32 : x_u == 'g' ? 64 : 0;
		int base = x_f == 'x' ? 16 : x_f == 'd' ? 10 : x_f == 'o' ? 8 : x_f == 't' ? 2 : 0;
		int digits = x_f == 'x' ? numBits / 4 : x_f == 'o' ? (int) Math.ceil(numBits / 3.0) : x_f == 't' ? numBits : (x_f == 'd' || x_f == 'u') ? Long.toString(1L << (numBits - 1)).length() : 0;
		int noPerLine = isXml ? 8 : 32 / (numBits / 8);
		if (isXml) {
			System.out.println("<memory>");
		}
		for (int i = 0; i < x_n; i++) { //For number of times
			if (i % noPerLine == 0) { //Line formatter
				if (i != 0) {
					address = isXml ? address + numBits : address + 32;
				}
				if (isXml) {
					if (i > 0) {
						System.out.println("</address>");
					}
					System.out.print(String.format("<address value=\"0x%08x\">", address));
				} else {
					if (i > 0) {
						System.out.println();
					}
					System.out.print(String.format("0x%08x: ", address));
				}
			}
			long val = 0;
			try { //Get Value from Memory
				val = memory.readGodMode(address + ((i % noPerLine) * numBits / 8), numBits / 8);
			} catch (IRQException | PageFaultException ex) {
				return;
			}
			if (x_f == 't') {
				output = Long.toBinaryString(val);
			} else if (x_f == 'o') {
				output = Long.toOctalString(val);
			} else if (x_f == 'x') {
				output = Long.toHexString(val);
			} else if (x_f == 'u') {
				output = new BigInteger(Long.toHexString(val), 16).toString();
			} else if (x_f == 'd') {
				output = Long.toString(val);
			}
			if (x_f == 'd' || x_f == 'u') {
				System.out.print(" ".repeat(digits - output.length()) + output + " ");
			} else {
				System.out.print("0".repeat(digits - output.length()) + output + " ");
			}
		}
		if (isXml) {
			System.out.println("</address>\n</memory>");
		}
	}

	public char getUnitsize(String str) {
		char c = str.charAt(0);
		for (int i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			if (c == 'b' || c == 'h' || c == 'w' || c == 'g') {
				return c;
			}
		}
		return c;
	}

	public char getPrintformat(String str) {
		char c = str.charAt(0);
		for (int i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			if (c == 'x' || c == 'd' || c == 'u' || c == 'o' || c == 't') {
				return c;
			}
		}
		return c;
	}

	private void modifyPC() throws NoOfByteException, PageFaultException {
		long pc = registers.get("pc").getValue().longValue();
		int noOfByte = RISCVDisassembler.getNoOfByte(memory.read(memory.getMappedAddress(pc, false), true, false) & 0x7f);

		if (noOfByte == 4) {
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 4);
		} else {
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 2);
		}
	}

	public void executeCPU() throws EndSimulatorException {
//		System.out.println(cpuTick+ " = "+registers.get("pc").getHexString());
		if (cpuTick % 100000 == 0) {
			long heapSize = Runtime.getRuntime().totalMemory() / 1024 / 1024 / 1024;
			long freeHeapSize = Runtime.getRuntime().freeMemory() / 1024 / 1024 / 1024;
//			System.out.println("running cpuTick " + sdf.format(new Date()) + ", " + decimalFormatter.format(cpuTick) + ", mem= " + (heapSize - freeHeapSize) + " / " + heapSize + "GB = " + (Math.round((float) (heapSize - freeHeapSize) / heapSize * 100)) + "%");
		}
		try {

			Line line = null;
			long pc = registers.get("pc").getValue().longValue();
			if (cpuTick == 20582471l) {
				int useless = 1234;
			}
			if (cpuTick == 11875571l) {
				int useless = 1234;
			}
			if (cpuTick == 11875612l) {
				int useless = 1234;
			}
			if (cpuTick == 136) {
				int useless = 123;
			}
			if (pc == 0x80000000l) {
				System.out.println("what is going on");
			}

//			FireInterrupt fireInterrupt = handleInterrupt();
//			if (fireInterrupt != null) {
//				handleInterrupt(fireInterrupt);
//			}
			pc = registers.get("pc").getValue().longValue();

			// don't fuck this
			riscvDisassembler.setCurrentOffset(pc);
//			System.out.println(Long.toHexString(pc) + " = " + bp.get(Long.toHexString(pc)));
			if (bp.get(Long.toHexString(pc)) != null) {
				if (bp.get(Long.toHexString(pc)) == 1) {
					System.out.println("Breakpoint Hit!");
					bp_flag = 1;
					bp.put(Long.toHexString(pc), 0);
				}
			}
			int[] opcodes = null;
			int noOfByte = RISCVDisassembler.getNoOfByte(memory.read(memory.getMappedAddress(pc, false), true, false) & 0x7f);
			if (noOfByte == 4) {
				opcodes = new int[4];
				opcodes[0] = memory.read(memory.getMappedAddress(pc, false), true, false) & 0xff;
				opcodes[1] = memory.read(memory.getMappedAddress(pc + 1, false), true, false) & 0xff;
				opcodes[2] = memory.read(memory.getMappedAddress(pc + 2, false), true, false) & 0xff;
				opcodes[3] = memory.read(memory.getMappedAddress(pc + 3, false), true, false) & 0xff;
			} else if (noOfByte == 2) {
				opcodes = new int[2];
				opcodes[0] = memory.read(memory.getMappedAddress(pc, false), true, false) & 0xff;
				opcodes[1] = memory.read(memory.getMappedAddress(pc + 1, false), true, false) & 0xff;
			} else {
				opcodes = new int[1];
				opcodes[0] = 99;
			}
			// end don't fuck this

			DecodeType type = RISCVDisassembler.getDecodeType(memory.read(memory.getMappedAddress(pc, false), true, false) & 0x7F);

			if (type == DecodeType.decodeTypeB) {
				line = riscvDisassembler.decodeTypeB(opcodes);
			} else if (type == DecodeType.decodeTypeI) {
				line = riscvDisassembler.decodeTypeI(opcodes);
			} else if (type == DecodeType.decodeTypeS) {
				line = riscvDisassembler.decodeTypeS(opcodes);
			} else if (type == DecodeType.decodeTypeR) {
				line = riscvDisassembler.decodeTypeR(opcodes);
			} else if (type == DecodeType.decodeTypeU) {
				line = riscvDisassembler.decodeTypeU(opcodes);
			} else if (type == DecodeType.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU) {
				line = riscvDisassembler.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU(opcodes);
			} else if (type == DecodeType.jal) {
				line = riscvDisassembler.jal(opcodes);
			} else if (type == DecodeType.jalr) {
				line = riscvDisassembler.jalr(opcodes);
			} else if (type == DecodeType.fence) {
				line = riscvDisassembler.fence(opcodes);
			} else if (type == DecodeType.magic) {
				line = riscvDisassembler.decodeMagic(opcodes);
			} else if (type == DecodeType.compressed) {
				if (opcodes[0] == 0 && opcodes[1] == 0) {
					System.out.println("Bad bytes, exit, pc=" + registers.get("pc").getHexString());
					return;
				} else {
					line = riscvDisassembler.decodeCompressed(opcodes);
				}
			} else if (type == DecodeType.decodeTypeV) {
				line = riscvDisassembler.decodeTypeV(opcodes);
			}
//			handleMemoryHiJack(cpuTick);
			if (dumpJson != null) {
				dumpToJson();
			}
			if (logThread != null) {
				dumpToLog(dwarfArrayList, pc, opcodes, line, cpuTick, mem, memOperation, memRead, memAddr, memValue, memSize);
			}

			// clear last mem
			mem = false;
			memOperation = null;
			memRead = false;
			memAddr = 0;
			memValue = 0;
			memSize = 0;
			// end clear last mem

			if (type == DecodeType.decodeTypeB) {
				if (bp_flag == 0) {
					handleTypeB(line);
				}
			} else if (type == DecodeType.decodeTypeI) {
				if (bp_flag == 0) {
					ImmutablePair pair = handleTypeI(opcodes, line);
					if (pair != null) {
						mem = true;
						memRead = true;
						memAddr = (long) pair.left;
						memValue = (long) pair.right;
						memSize = 0;
					}
				}
			} else if (type == DecodeType.decodeTypeS) {
				if (bp_flag == 0) {
					ImmutablePair pair = handleTypeS(line);
					mem = true;
					memRead = false;
					memAddr = (long) pair.left;
					memValue = (long) pair.right;
					memSize = 0;
					modifyPC();
				}
			} else if (type == DecodeType.decodeTypeR) {
				if (bp_flag == 0) {
					ImmutablePair pair = handleTypeR(line);
					if (pair != null) {
						mem = true;
						memRead = true;
						memAddr = (long) pair.left;
						memValue = (long) pair.right;
						memSize = 0;
					}
				}
			} else if (type == DecodeType.decodeTypeU) {
				if (bp_flag == 0) {
					handleTypeU(line);
					modifyPC();
				}
			} else if (type == DecodeType.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU) {
				if (bp_flag == 0) {
					handleEcallOrTrap(line);
				}
			} else if (type == DecodeType.jal) {
				if (bp_flag == 0) {
					String rd = Registers.getRegXNum32(line.rd);
					long long_val = registers.get("pc").getValue().longValue() + 4;
					if (!rd.equals("zero")) {
						registers.get(rd).setValue(long_val);
					}
					registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				}
			} else if (type == DecodeType.jalr) {
				if (bp_flag == 0) {
					String rs1 = Registers.getRegXNum32(line.rs1);
					long rsValue = registers.get(rs1).getValue().longValue();
					if (line.rd != 0) {
						String rd = Registers.getRegXNum32(line.rd);
						registers.get(rd).setValue(registers.get("pc").getValue().longValue() + 4);
					}
					registers.get("pc").setValue(rsValue + line.imm);
				}
			} else if (type == DecodeType.fence) {
				if (bp_flag == 0) {
					modifyPC();
				}
			} else if (type == DecodeType.magic) {
				bp_flag = 1;
				startCont = true;
				modifyPC();
			} else if (type == DecodeType.compressed) {
				if (opcodes[0] == 0 && opcodes[1] == 0) {
					System.out.println("Bad bytes, exit, pc=" + registers.get("pc").getHexString());
					return;
				} else {
					if (bp_flag == 0) {
//						if (line.rd + line.imm >= 0x80021ec0l && line.rd + line.imm <= 0x80021d00l) {
//							System.out.println("haha");
//						}
						ImmutablePair pair = handleCompressed(line);
						if (pair != null) {
							mem = true;
							memRead = false;
							memAddr = (long) pair.left;
							memValue = (long) pair.right;
							memSize = 0;
						}
					}
				}
			} else if (type == DecodeType.decodeTypeV) {
				if (bp_flag == 0) {
					handleTypeV(opcodes, line);
				}
			}

			if (printDisassembleLine) {
				if (dwarfArrayList == null) {
					System.out.println(String.format("%5d, 0x%08x, [%-19s]: %s", cpuTick, pc, CommonLib.arrayToHexString(opcodes), line.code));
				} else {
					ArrayList<String> s = QuantrDwarf.getCCode(dwarfArrayList, BigInteger.valueOf(pc), false);
					if (s != null) {
						for (String temp : s) {
							System.out.println(ConsoleColor.blue + temp);
						}
					}
					System.out.println(String.format("%5d, 0x%08x, [%-19s]: %s", cpuTick, pc, CommonLib.arrayToHexString(opcodes), line.code));
				}
			}
			mtime += 1;
			memory.write(cpuTick, 0x200bff8, mtime, 8, true, false);

			if (pendingInterrupt != null) {
//				System.out.println("fire pendingInterrupt, " + registers.get("mstatus").getHexString());
				boolean b = handleInterrupt(pendingInterrupt);
				if (b) {
					pendingInterrupt = null;
				}
			}
		} catch (PageFaultException | IRQException ex) {
			Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
			registers.get("mepc").setValue(registers.get("pc").getValue().longValue());
			registers.get("pc").setValue(registers.get("mtvec").getValue().longValue() & 0xfffffffffffffffcL);
			registers.get("mcause").setValue(0xd); //this is wrong
			CPUState.setPriv(3);
		} catch (WrongInstructionException ex) {
			Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(6000);
		} catch (NoOfByteException ex) {
			Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(6000);
		} catch (AccessFaultException ex) {
			Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(6000);
		} catch (IllegalInstructionException ex) {
			registers.get("mcause").setValue(0x2);
			registers.get("mepc").setValue(registers.get("pc").getValue());
			try {
				modifyPC();
			} catch (NoOfByteException ex1) {
				Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex1);
			} catch (PageFaultException ex1) {
				Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex1);
			}
		}
		cpuTick++;
//		}
	}

	private FireInterrupt handleInterrupt() {
		FireInterrupt fireInterrupt = Setting.getInstance().getFireInterrupt(cpuTick);
		if (fireInterrupt != null) {
//			long cause = fireInterrupt.cause & 0x7fffffffffffffffl;
//			long mask = 1 << cause;
//			if ((CPUState.priv == CPUState.MACHINE_MODE && (registers.get("mie").getValue().longValue() & mask) == mask && (registers.get("mip").getValue().longValue() & mask) == 0x0)
//					|| (CPUState.priv == CPUState.SUPERVISOR_MODE && (registers.get("sie").getValue().longValue() & mask) == mask && (registers.get("sip").getValue().longValue() & mask) == 0x0)) {
//			if (mtime > mtimecmp || Setting.getInstance().fireInterrupts.contains(cpuTick)) {
			return fireInterrupt;
//			}
		}
		return null;
	}

//public void checkPendingInterrupt() throws RiscvInterrupt, RiscvException {
//		if (CPUState.priv == CPUState.MACHINE_MODE && ((registers.get("mstatus").getValue().longValue() >> 3) & 1) == 0) {
//			return;
//		} else if (CPUState.priv == CPUState.SUPERVISOR_MODE && ((registers.get("sstatus").getValue().longValue() >> 1) & 1) == 0) {
//			return;
//		}
////		long irq;
////		do {
////			if (bus.uart.isInterrupting()) {
////				irq = bus.uart.UART_IRQ;
////			} else if (bus.virtio.isInterrupting()) {
////				irq = bus.virtio.VIRTIO_IRQ;
////				bus.diskAccess();
////			} else {
////				break;
////			}
////			try {
////				bus.write(PLIC.SCLAIM, irq, 4);
////				registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 0x200);
////			} catch (RiscvException ex) {
////				/* Nothing can go wrong here */ }
////		} while (false);
//
//		if ((registers.get("mstatus").getValue().longValue() & 0x8L) == 0) {
//			return;
//		}
//		long pending = registers.get("mie").getValue().longValue() | registers.get("mip").getValue().longValue();
//		if ((pending & 0x800) != 0) { //MIP_MEIP
//			registers.get("mip").setValue(registers.get("mip").getValue().longValue() & ~0x800);
//			throw new RiscvInterrupt(RiscvInterrupt.MACHINE_EXTERNAL_INTERRUPT, "Machine External Interrupt");
//		}
//		if ((pending & 0x8) != 0) { //MIP_MSIP
//			registers.get("mip").setValue(registers.get("mip").getValue().longValue() & ~0x8);
//			throw new RiscvInterrupt(RiscvInterrupt.MACHINE_SOFTWARE_INTERRUPT, "Machine Software Interrupt");
//		}
//		if ((pending & 0x80) != 0) { //MIP_MTIP
//			if (Long.compareUnsigned(
//					memory.read(0x200bff8L, 8, false, true), // timer interrupt
//					memory.read(0x2004000L, 8, false, true))
//					>= 0) {
//				handleInterrupt(RiscvInterrupt.MACHINE_TIMER_INTERRUPT);
//
////				mtime = 0;
////				memory.write(cpuTick, 0x200bff8L, mtime, 8, true, false);
////				throw new RiscvInterrupt(RiscvInterrupt.MACHINE_TIMER_INTERRUPT, "Machine Timer Interrupt");
//			}
//		}
//		if ((pending & 0x200) != 0) { //MIP_SEIP
//			registers.get("mip").setValue(registers.get("mip").getValue().longValue() & ~0x200);
//			throw new RiscvInterrupt(RiscvInterrupt.SUPERVISOR_EXTERNAL_INTERRUPT, "Supervisor External Interrupt");
//		}
//		if ((pending & 0x2) != 0) { //MIP_SSIP
//			registers.get("mip").setValue(registers.get("mip").getValue().longValue() & ~0x2);
//			throw new RiscvInterrupt(RiscvInterrupt.SUPERVISOR_SOFTWARE_INTERRUPT, "Supervisor Software Interrupt");
//		}
//		if ((pending & 0x20) != 0) { //MIP_STIP
//			registers.get("mip").setValue(registers.get("mip").getValue().longValue() & ~0x20);
//			throw new RiscvInterrupt(RiscvInterrupt.SUPERVISOR_TIMER_INTERRUPT, "Supervisor Timer Interrupt");
//		}
//	}
	private boolean handleInterrupt(FireInterrupt interrupt) {
		/*
		var & ~(0x10) : turn bit 0x10 to 0
		 */
		if (interrupt.desc != null && interrupt.desc.equals("user_ecall")) {
			return false;
		}
		if (interrupt.cause == 7) {
			if ((registers.get("mstatus").getValue().longValue() & 0x80) != 0x80) {
				return false;
			}
		} else if (CPUState.priv == CPUState.MACHINE_MODE) {
			if ((registers.get("mstatus").getValue().longValue() & 0x8) != 0x8) {
				return false;
			}
		} else if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
			if ((registers.get("mstatus").getValue().longValue() & 0x2) != 0x2) { //2873138  && (interrupt.cause & 0x3) == 1
				return false;
			}
		}
		switch ((int) interrupt.cause) {  // to be verified
			case RiscvInterrupt.SUPERVISOR_SOFTWARE_INTERRUPT, RiscvInterrupt.SUPERVISOR_TIMER_INTERRUPT, RiscvInterrupt.SUPERVISOR_EXTERNAL_INTERRUPT, RiscvInterrupt.USER_EXTERNAL_INTERRUPT -> {  // to be verified
				registers.get("sip").setValue(registers.get("sip").getValue().longValue() | 1 << interrupt.cause);
				registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 1 << interrupt.cause);
//				registers.get("mstatus").setValue((registers.get("mstatus").getValue().longValue() & ~(CPUState.SUPERVISOR_MODE << 11)) | (CPUState.SUPERVISOR_MODE << 11));
//				registers.get("mstatus").setValue((registers.get("mstatus").getValue().longValue() & ~(0x88L)) | 0x80L);
				if (((registers.get("mstatus").getValue().longValue() >> 1) & 1) == 1) {
					registers.get("mstatus").setValue(registers.get("mstatus").getValue().longValue() | 0x20);
					registers.get("mstatus").setValue(registers.get("mstatus").getValue().longValue() & ~0x2);
					registers.get("mstatus").setValue(registers.get("mstatus").getValue().longValue() | 0x100);
				}
				registers.get("sepc").setValue(registers.get("pc").getValue().longValue());
				registers.get("pc").setValue(registers.get("stvec").getValue().longValue());
				registers.get("scause").setValue(0x8000000000000000L + interrupt.cause);
				return true;
			}
//			case RiscvInterrupt.USER_EXTERNAL_INTERRUPT -> {  // to be verified
//				registers.get("sip").setValue(registers.get("sip").getValue().longValue() | 1 << interrupt.cause);
//				registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 1 << interrupt.cause);
////				registers.get("mstatus").setValue((registers.get("mstatus").getValue().longValue() & ~(CPUState.SUPERVISOR_MODE << 11)) | (CPUState.SUPERVISOR_MODE << 11));
////				registers.get("mstatus").setValue((registers.get("mstatus").getValue().longValue() & ~(0x88L)) | 0x80L);
//				if (((registers.get("mstatus").getValue().longValue() >> 1) & 1) == 1) {
//					registers.get("mstatus").setValue(registers.get("mstatus").getValue().longValue() | 0x20);
//					registers.get("mstatus").setValue(registers.get("mstatus").getValue().longValue() & ~0x2);
//					registers.get("mstatus").setValue(registers.get("mstatus").getValue().longValue() | 0x100);
//				}
////				registers.get("sepc").setValue(registers.get("pc").getValue().longValue());
//				registers.get("pc").setValue(registers.get("stvec").getValue().longValue());
//				registers.get("scause").setValue(interrupt.cause);
//			}
			case RiscvInterrupt.MACHINE_SOFTWARE_INTERRUPT, RiscvInterrupt.MACHINE_TIMER_INTERRUPT, RiscvInterrupt.MACHINE_EXTERNAL_INTERRUPT -> {
				registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 1 << interrupt.cause);
				registers.get("mstatus").setValue(((registers.get("mstatus").getValue().longValue() & ~(0x80L)) | ((registers.get("mstatus").getValue().longValue() & 0x8L) << 4))); //store MIE to MPIE
				registers.get("mstatus").setValue(registers.get("mstatus").getValue().longValue() & ~(0x8L)); // set MIE to 0
				registers.get("mstatus").setValue((registers.get("mstatus").getValue().longValue() & ~(0x1800L)) | (CPUState.priv << 11));
				registers.get("mepc").setValue(registers.get("pc").getValue().longValue());
				registers.get("pc").setValue(registers.get("mtvec").getValue().longValue());
				registers.get("mcause").setValue(0x8000000000000000L + interrupt.cause);
				CPUState.setPriv(3);
				return true;
			}
		}
		return false;
	}

//	private void handlemip(int interruptNo) { // for panel
//		switch (interruptNo) {
//			case RiscvInterrupt.SUPERVISOR_SOFTWARE_INTERRUPT, RiscvInterrupt.SUPERVISOR_TIMER_INTERRUPT -> {  // to be verified
//				registers.get("sip").setValue(registers.get("sip").getValue().longValue() | 1 << interruptNo);
//				registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 1 << interruptNo);
//			}
//			case RiscvInterrupt.MACHINE_SOFTWARE_INTERRUPT, RiscvInterrupt.MACHINE_TIMER_INTERRUPT, RiscvInterrupt.MACHINE_EXTERNAL_INTERRUPT -> { // to be verified
//				registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 1 << interruptNo);
//			}
//		}
//	}
	private ImmutablePair<Long, Integer> handleTypeI(int arr[], Line line) throws PageFaultException, NoOfByteException, IRQException, AccessFaultException {
		String rd = Registers.getRegXNum32(line.rd);
		String rs1 = Registers.getRegXNum32(line.rs1);
		int shamt5 = (arr[3] & 0x1) << 4 | (arr[2] & 0xf0) >> 4;
		int shamt6 = (arr[3] & 0x3) << 4 | (arr[2] & 0xf0) >> 4;
		long long_val;
		ImmutablePair pair = null;
		if (line.instructionType == InstructionType.addi) {
			if (line.rs1 == 0) {
				registers.get(rd).setValue(signExtend64(line.imm, 11));
			} else {
				long_val = registers.get(rs1).getValue().longValue() + signExtend64(line.imm, 11);
				registers.get(rd).setValue(long_val);
			}
		} else if (line.instructionType == InstructionType.andi) {
			long_val = registers.get(rs1).getValue().longValue() & signExtend64(line.imm, 11);
			registers.get(rd).setValue(long_val);
		} else if (line.instructionType == InstructionType.ori) {
			long_val = registers.get(rs1).getValue().longValue() | signExtend64(line.imm, 11);
			registers.get(rd).setValue(long_val);
		} else if (line.instructionType == InstructionType.xori) {
			long_val = registers.get(rs1).getValue().longValue() ^ line.imm;
			registers.get(rd).setValue(long_val);
		} else if (line.instructionType == InstructionType.slli) {
			long_val = registers.get(rs1).getValue().longValue() << shamt6;
			registers.get(rd).setValue(long_val);
		} else if (line.instructionType == InstructionType.srli) {
			long_val = registers.get(rs1).getValue().longValue() >>> shamt6;
			registers.get(rd).setValue(long_val);
		} else if (line.instructionType == InstructionType.srai) {
			long_val = registers.get(rs1).getValue().longValue() >> shamt6;
			registers.get(rd).setValue(long_val);
		} else if (line.instructionType == InstructionType.slliw) {
			long_val = registers.get(rs1).getValue().longValue() << shamt5;
			registers.get(rd).setValue(long_val);
		} else if (line.instructionType == InstructionType.srliw) {
			long_val = registers.get(rs1).getValue().longValue() >>> shamt5;
			registers.get(rd).setValue(long_val);
		} else if (line.instructionType == InstructionType.sraiw) {
			long_val = registers.get(rs1).getValue().longValue() >> shamt5;
			registers.get(rd).setValue(long_val);
		} else if (line.instructionType == InstructionType.slti) {
			if (registers.get(rs1).getValue().longValue() < line.imm) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instructionType == InstructionType.sltiu) {
			if (registers.get(rs1).getValue().compareTo(new BigInteger(Long.toString(line.imm))) == -1) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instructionType == InstructionType.lb) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long b = memory.read(long_val, 1, true, false);
			registers.get(rd).setValue(b);
			pair = new ImmutablePair(long_val, b);
		} else if (line.instructionType == InstructionType.lh) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long b = memory.read(long_val, 2, true, false);
			registers.get(rd).setValue(b);
			pair = new ImmutablePair(long_val, b);
		} else if (line.instructionType == InstructionType.lbu) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long b = memory.read(long_val, 1, true, false);
			registers.get(rd).setValue(b);
			pair = new ImmutablePair(long_val, b);
		} else if (line.instructionType == InstructionType.lhu) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long b = memory.read(long_val, 2, true, false) & 0xFF;
			registers.get(rd).setValue(b);
			pair = new ImmutablePair(long_val, b);
		} else if (line.instructionType == InstructionType.lw) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long b = memory.read(long_val, 4, true, false);
			if (((b >> 31) & 1) == 1) {
				b = b | 0xffffffff00000000l;
			}
			registers.get(rd).setValue(b);
			pair = new ImmutablePair(long_val, b);
		} else if (line.instructionType == InstructionType.ld) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long b = memory.read(long_val, 8, true, false);
			registers.get(rd).setValue(b);
			pair = new ImmutablePair(long_val, b);
		} else if (line.instructionType == InstructionType.addiw) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			registers.get(rd).setValue(signExtend32To64(BigInteger.valueOf(long_val & 0xffffffffl)));
		}
		modifyPC();
		return pair;
	}

	private ImmutablePair<Long, Integer> handleTypeS(Line line) throws PageFaultException, NoOfByteException, IRQException, AccessFaultException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		long address = registers.get(rs2).getValue().longValue();
		if (address == 0x2004000L) { //mtimecmp
//							if (mtime < mtimecmp) { // if mtime already greater than mtimecmp then no need reset mtip
			registers.get("mip").setValue(registers.get("mip").getValue().longValue() & ~(0x80L));
//							}
		}
//		if (address + line.imm >= 0x80021ec0l && address + line.imm <= 0x80021d00l) {
//			System.out.println("haha");
//		}
		ImmutablePair pair = null;
		if (line.instructionType == InstructionType.sb) {
//			long address = registers.get(rs2).getValue().longValue();
			memory.write(cpuTick, address + line.imm, registers.get(rs1).getValue().longValue(), 1, false, true);
			pair = new ImmutablePair(address + line.imm, registers.get(rs1).getValue().longValue());
		} else if (line.instructionType == InstructionType.sh) {
//			long address = registers.get(rs2).getValue().longValue();
			memory.write(cpuTick, address + line.imm, registers.get(rs1).getValue().longValue(), 2, false, true);
			pair = new ImmutablePair(address + line.imm, registers.get(rs1).getValue().longValue());
		} else if (line.instructionType == InstructionType.sw) {
//			long address = registers.get(rs2).getValue().longValue();
			memory.write(cpuTick, address + line.imm, registers.get(rs1).getValue().longValue(), 4, false, true);
			pair = new ImmutablePair(address + line.imm, registers.get(rs1).getValue().longValue());
		} else if (line.instructionType == InstructionType.sd) {
//			long address = registers.get(rs2).getValue().longValue();
			memory.write(cpuTick, address + line.imm, registers.get(rs1).getValue().longValue(), 8, false, true);
			pair = new ImmutablePair(address + line.imm, registers.get(rs1).getValue().longValue());
		}
		return pair;
	}

	private void handleTypeB(Line line) throws NoOfByteException, PageFaultException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		boolean modPC = false;

		if (line.instructionType == InstructionType.beq || line.instructionType == InstructionType.beqz) {
//			System.out.println(" " + registers.get(rs1).getValue().longValue() + " " + registers.get(rs2).getValue().longValue() + " " + line.imm);
			if (registers.get(rs1).getValue().longValue() == registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instructionType == InstructionType.bne) {
			if (registers.get(rs1).getValue().longValue() != registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instructionType == InstructionType.bnez) {
			if (registers.get(rs1).getValue().longValue() != 0) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instructionType == InstructionType.blt || line.instructionType == InstructionType.bltz || line.instructionType == InstructionType.bgtz) {
			if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instructionType == InstructionType.bge || line.instructionType == InstructionType.bgez || line.instructionType == InstructionType.blez) {
			if (registers.get(rs1).getValue().longValue() >= registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instructionType == InstructionType.bltu) {
			if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instructionType == InstructionType.bgeu) {
			if (registers.get(rs1).getValue().longValue() >= registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		}
		if (!modPC) {
			modifyPC();
		}
	}

	private ImmutablePair<Long, Integer> handleTypeR(Line line) throws PageFaultException, NoOfByteException, IRQException, AccessFaultException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		String rd = Registers.getRegXNum32(line.rd);
		ImmutablePair pair = null;
		if (line.instructionType == InstructionType.sll) {
			long val = registers.get(rs1).getValue().longValue() << registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.srl) {
			long val = registers.get(rs1).getValue().longValue() >>> registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.sra) {
			long val = registers.get(rs1).getValue().longValue() >> registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.add) {
			long val = (registers.get(rs1).getValue().longValue() + registers.get(rs2).getValue().longValue());// & 0xffffffffL;
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.sub) {
			long val;
//			if (registers.get(rs1).getValue().longValue() > registers.get(rs2).getValue().longValue()) {
			val = registers.get(rs1).getValue().longValue() - registers.get(rs2).getValue().longValue();
//			} else {
//				val = 0;
//			}
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.xor) {
			long val = registers.get(rs1).getValue().longValue() ^ registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.or) {
			long val = registers.get(rs1).getValue().longValue() | registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.and) {
			long val = registers.get(rs1).getValue().longValue() & registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.slt) {
			if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instructionType == InstructionType.sltu) {
			if (line.rs1 == 0) { // instruction snez
				if (registers.get(rs2).getValue().longValue() == 0) {
					registers.get(rd).setValue(0);
				} else {
					registers.get(rd).setValue(1);
				}
			} else if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instructionType == InstructionType.mul) {
			long val = (registers.get(rs1).getValue().longValue() * registers.get(rs2).getValue().longValue()) & 0xffffffff;
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.mulh) {
			long val = (registers.get(rs1).getValue().longValue() * registers.get(rs2).getValue().longValue()) & 0xffffffff00000000L;
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.mulhsu) {
			long val = (registers.get(rs1).getValue().multiply(registers.get(rs2).getValue())).and(new BigInteger("0xffffffff00000000", 16)).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.mulhu) {
			long val = (registers.get(rs1).getValue().multiply(registers.get(rs2).getValue())).and(new BigInteger("0xffffffff00000000", 16)).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.div) {
			long val = registers.get(rs1).getValue().longValue() / registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.divu) {
			long val = registers.get(rs1).getValue().divide(registers.get(rs2).getValue()).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.rem) {
			long val = registers.get(rs1).getValue().longValue() % registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.remu) {
			long val = registers.get(rs1).getValue().mod(registers.get(rs2).getValue()).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.lr_w) { //32A Standard Extension

		} else if (line.instructionType == InstructionType.sc_w) {

		} else if (line.instructionType == InstructionType.amoswap_w_aq) {
			long long_val = registers.get(rs1).getValue().longValue();
			long val = registers.get(rs2).getValue().longValue();
			long_val = memory.read(long_val, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), val);
			if (line.rd != 0) {
				registers.get(rd).setValue(long_val);
			}
			memory.write(cpuTick, registers.get(rs1).getValue().longValue(), val, 1, false, true);
		} else if (line.instructionType == InstructionType.amoswap_w) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			long rs2Value = registers.get(rs2).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs2Value);
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
			}
			memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
		} else if (line.instructionType == InstructionType.amoadd_w) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = registers.get(rs2).getValue().longValue() + rs1Value;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amoxor_w) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = registers.get(rs2).getValue().longValue() ^ rs1Value;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amoand_w) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = registers.get(rs2).getValue().longValue() & rs1Value;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amoor_w) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = registers.get(rs2).getValue().longValue() | rs1Value;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amomin_w) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amomax_w) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amominu_w) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amomaxu_w) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.lr_d) { //64A Standard Extension

		} else if (line.instructionType == InstructionType.sc_d) {

//		} else if (line.instructionType == InstructionType.amoswap.d) {
//			long rs1Value = registers.get(rs1).getValue().longValue();
//			long rs2Value = registers.get(rs2).getValue().longValue();
//			rs1Value = memory.read(rs1Value, 8, true, false);
//			if (!rd.equals("zero")) {
//				registers.get(rd).setValue(rs1Value);
//				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
//			}
		} else if (line.instructionType == InstructionType.amoadd_d) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = registers.get(rs2).getValue().longValue() + rs1Value;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amoxor_d) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = registers.get(rs2).getValue().longValue() ^ rs1Value;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amoand_d) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = registers.get(rs2).getValue().longValue() & rs1Value;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amoor_d) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = registers.get(rs2).getValue().longValue() | rs1Value;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amomin_d) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amomax_d) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amominu_d) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.amomaxu_d) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			pair = new ImmutablePair(registers.get(rs1).getValue().longValue(), rs1Value);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value);
				memory.write(cpuTick, registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instructionType == InstructionType.remuw) {
			long rs1Value = registers.get(rs1).getValue().longValue() & 0xffffffffl;
			long rs2Value = registers.get(rs2).getValue().longValue() & 0xffffffffl;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value % rs2Value);
			}
//		} else if (line.instructionType == InstructionType.muluw) {
//			long rs1Value = registers.get(rs1).getValue().longValue() & 0xffffffffl;
//			long rs2Value = registers.get(rs2).getValue().longValue() & 0xffffffffl;
//			if (!rd.equals("zero")) {
//				registers.get(rd).setValue(rs1Value * rs2Value);
//			}
		} else if (line.instructionType == InstructionType.divuw) {
			long rs1Value = registers.get(rs1).getValue().longValue() & 0xffffffffl;
			long rs2Value = registers.get(rs2).getValue().longValue() & 0xffffffffl;
			if (line.rd != 0) {
				registers.get(rd).setValue(rs1Value / rs2Value);
			}
		} else if (line.instructionType == InstructionType.subw) {
			long val;
			val = registers.get(rs1).getValue().and(BigInteger.valueOf(0xffffffffl)).longValue() - registers.get(rs2).getValue().and(BigInteger.valueOf(0xffffffffl)).longValue();
			registers.get(rd).setValue(val);
			registers.get(rd).setValue(signExtend32To64(registers.get(rd).getValue()));
		} else if (line.instructionType == InstructionType.addw) {
			long val;
			val = registers.get(rs1).getValue().and(BigInteger.valueOf(0xffffffffl)).longValue() + registers.get(rs2).getValue().and(BigInteger.valueOf(0xffffffffl)).longValue();
			registers.get(rd).setValue(val);
			registers.get(rd).setValue(signExtend32To64(registers.get(rd).getValue()));
		} else {
			System.out.println("FUCK, no way to execute");
			System.out.println(line);
			System.exit(1000);
		}

		modifyPC();
		return pair;
	}

	private void handleTypeU(Line line) throws NoOfByteException, PageFaultException, IRQException {
		String rd = Registers.getRegXNum32(line.rd);
		if (line.instructionType == InstructionType.lui) {
			long value = signExtend64(line.imm << 12, 31);
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.auipc) {
			//registers.get(rd).value = registers.et("pc").value + value1;
			long value = registers.get("pc").getValue().longValue() + (line.imm << 12);//pc relative address
			value = value & 0xffffffffl;
			registers.get(rd).setValue(value);
		}
	}

	private void handleCityu(Line line) throws PageFaultException, NoOfByteException, IRQException {
		String rd = Registers.getRegXNum32(line.rd);
		String rs1 = Registers.getRegXNum32(line.rs1);

		long value;
		value = registers.get(rs1).getValue().longValue();
		value *= value;
		registers.get(rd).setValue(value);

		modifyPC();
	}

	private void handleEcallOrTrap(Line line) throws NoOfByteException, PageFaultException, IRQException, EndSimulatorException, IllegalInstructionException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rd = Registers.getRegXNum32(line.rd);
		String csr = Registers.getCsrNum12(line.csr);

		boolean isStatus = false, isSip = false;
		if (csr.equals("sstatus")) {
			csr = "mstatus";
			isStatus = true;
		} else if (csr.equals("sip")) {
			csr = "mip";
			isSip = true;
		}
		if (csr.equals("sie")) {
			csr = "mie";
		}

		if (line.instructionType == InstructionType.ecall) {
			if (registers.get("a7").getValue().longValue() == 93) {
				if (registers.get("a0").getValue().longValue() == 0) {
					System.out.println("\n---------------------");
					System.out.println("riscv-tests pass");
					System.out.println("---------------------\n");
				} else {
					System.out.println("riscv-tests failed : " + registers.get("a0").getValue().longValue() / 2);
				}
				throw new EndSimulatorException();
//				System.exit((int) registers.get("a0").getValue().longValue() / 2);
			}
			if (CPUState.priv == CPUState.USER_MODE) {
				registers.get("sepc").setValue(registers.get("pc").getValue().longValue());
				registers.get("pc").setValue(registers.get("mtvec").getValue().longValue() & 0xfffffffffffffffcL);
				registers.get("scause").setValue(0x8); //machine external interrupt

				//resume sie from pie
				int sie = (int) registers.get("mstatus").getValue().longValue() & 0b10;
				long csr_val = registers.get("mstatus").getValue().longValue();
				registers.get("mstatus").setValue(csr_val & ~0b100000 | sie << 4);
				registers.get("mstatus").setValue(registers.get("mstatus").getValue().longValue() & ~0b10); // clear sie

				CPUState.setPriv(CPUState.SUPERVISOR_MODE);
			} else if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
				registers.get("sepc").setValue(registers.get("pc").getValue().longValue());
				registers.get("pc").setValue(registers.get("stvec").getValue().longValue() & 0xfffffffffffffffcL);
				registers.get("scause").setValue(0x8); //machine external interrupt
			} else {
				registers.get("mepc").setValue(registers.get("pc").getValue().longValue());
				registers.get("pc").setValue(registers.get("mtvec").getValue().longValue() & 0xfffffffffffffffcL);
				registers.get("mcause").setValue(0x8); //machine external interrupt
			}
		} else if (line.instructionType == InstructionType.ebreak) {

		} else if (line.instructionType == InstructionType.wfi) {
			int mie = (int) registers.get("mstatus").getValue().longValue() & 0b1000;
			int hie = (int) registers.get("mstatus").getValue().longValue() & 0b0100;
			int sie = (int) registers.get("mstatus").getValue().longValue() & 0b0010;
			int uie = (int) registers.get("mstatus").getValue().longValue() & 0b0001;

			if (mie == 8 || hie == 4 || sie == 2 || uie == 1) {
				registers.get("mepc").setValue(registers.get("pc").getValue().longValue() + 4);
			}
			modifyPC();
		} else if (line.instructionType == InstructionType.csrrw) {
			/* The CSRRW (Atomic Read/Write CSR) instruction atomically swaps values in the CSRs and integer registers. 
			CSRRW reads the old value of the CSR, zero-extends the value to XLEN bits, then writes it to integer 
			register rd. The initial value in rs1 is written to the CSR. If rd=x0, then the instruction shall not 
			read the CSR and shall not cause any of the side effects that might occur on a CSR read. 
			A CSRRW with rs1=x0 will attempt to write zero to the destination CSR.*/
//			csr = CSR.csrToUse(csr);
			long csr_val = registers.get(csr).getValue().longValue();
			long rs1_val = registers.get(rs1).getValue().longValue();
			if (line.rd != 0) {
				registers.get(rd).setValue(csr_val);
			}
			if (csr.equals("mideleg")) { //temporarily, should be wrong becuz too weird
				registers.get(csr).setValue((csr_val & ~CSR.MIDELEG_MASK) | (rs1_val & CSR.MIDELEG_MASK));
			} else if (csr.equals("mip")) {
				/* Spec V2 P.68
				The sip and sie registers are subsets of the mip and mie registers. 
				Reading any field, or writing any writable field, of sip/sie effects a read or write of the homonymous field of mip/mie.
				 */
//				CSR.checkWritePermission(this, csr);
//				registers.get(csr).setValue(rs1_val & CSR.getMask(csr));
				CSR.checkWritePermission(this, csr);
				long shiftBits = (3 - (long) Math.ceil(Math.log(rs1_val) / Math.log(16))) * 4;
				if (rs1_val == 0) {
					if (isSip) {
						registers.get(csr).setValue(csr_val & CSR.getMask("sip"));
					} else {
						registers.get(csr).setValue(rs1_val);
					}
				} else if (isSip) {
					registers.get(csr).setValue((csr_val & 0x200) | (rs1_val & ~CSR.getMask("sip")));
				} else {
					registers.get(csr).setValue((rs1_val & (0xfffl >> shiftBits)) | (csr_val & (0xfff000l >> shiftBits))); //~CSR.getMask("sip") & 
				}

//				registers.get(csr).setValue((rs1_val & (0xfffl >> shiftBits)) | (csr_val & (0xfff000l >> shiftBits))); //~CSR.getMask("sip") & 
			} else if (csr.equals("sie")) {
				CSR.checkWritePermission(this, csr);
				registers.get(csr).setValue(rs1_val & CSR.getMask(csr));
				CSR.checkWritePermission(this, "mie");
				registers.get("mie").setValue(rs1_val & CSR.getMask(csr));
			} else {
				CSR.checkWritePermission(this, csr);
				registers.get(csr).setValue(rs1_val & CSR.getMask(csr));
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(this, csr);
				}
			}
			modifyPC();
		} else if (line.instructionType == InstructionType.csrrs) {
			/* The CSRRS (Atomic Read and Set Bits in CSR) instruction reads the value of the CSR, zero-extends 
			the value to XLEN bits, and writes it to integer register rd. The initial value in integer register 
			rs1 is treated as a bit mask that specifies bit positions to be set in the CSR. Any bit that is high 
			in rs1 will cause the corresponding bit to be set in the CSR, if that CSR bit is writable. Other bits 
			in the CSR are not explicitly written. 
			For both CSRRS and CSRRC, if rs1=x0, then the instruction will not write to the CSR at all, and so shall 
			not cause any of the side effects that might otherwise occur on a CSR write, nor raise illegal instruction 
			exceptions on accesses to read-only CSRs. Both CSRRS and CSRRC always read the addressed CSR and cause any 
			read side effects regardless of rs1 and rd fields. Note that if rs1 specifies a register holding a zero value 
			other than x0, the instruction will still attempt to write the unmodified value back to the CSR and will cause 
			any attendant side effects.
			 */
//			csr = CSR.csrToUse(csr);
			long csr_val = registers.get(csr).getValue().longValue();
			long rs1_val = registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(csr_val & (isStatus ? ~0x3c00721888l : ~0x0l) & (isSip ? ~0x888l : ~0x0l));
			if (line.rs1 != 0) {
				CSR.checkWritePermission(this, csr);
				registers.get(csr).setValue((csr_val | rs1_val) & CSR.getMask(csr));
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(this, csr);
				}
			}
			modifyPC();
		} else if (line.instructionType == InstructionType.csrrc) {
			/* The CSRRC (Atomic Read and Clear Bits in CSR) instruction reads the value of the CSR, zero-extends the value to XLEN
			bits, and writes it to integer register rd. The initial value in integer register rs1 is treated as a bit mask that specifies
			bit positions to be cleared in the CSR. Any bit that is high in rs1 will cause the corresponding bit to be cleared in the CSR,
			if that CSR bit is writable. Other bits in the CSR are not explicitly written. If rs1 is x0, check conditions in above CSRRS. */
//			csr = CSR.csrToUse(csr);
			long csr_val = registers.get(csr).getValue().longValue(), rs1_val = registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(csr_val);
			if (line.rs1 != 0) {
				CSR.checkWritePermission(this, csr);
				registers.get(csr).setValue(csr_val & (~rs1_val) & CSR.getMask(csr));
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(this, csr);
				}
			}
			modifyPC();
		} else if (line.instructionType == InstructionType.csrrwi) {
			if (line.rd != 0) {
				registers.get(rd).setValue(registers.get(csr).getValue().longValue());
			}
			if (registers.get(csr) == null) {
				throw new IllegalInstructionException();
			}
			if (registers.get(csr).getName().equals("read_only")) {
				System.out.println("Error: Writing to a Read only Register");
			} else {
				registers.get(csr).setValue(line.imm);
			}
			if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
				PMP.updatePmp(this, csr);
			}
			modifyPC();
		} else if (line.instructionType == InstructionType.csrrsi) {
			registers.get(rd).setValue(registers.get(csr).getValue().longValue());
			if (line.imm != 0) {
				if (registers.get(csr).getName().equals("read_only")) {
					System.out.println("Error: Writing to a Read only Register");
				} else {
					registers.get(csr).setValue(registers.get(csr).getValue().longValue() | line.imm);
				}
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(this, csr);
				}
			}
			modifyPC();
		} else if (line.instructionType == InstructionType.csrrci) {
			registers.get(rd).setValue(registers.get(csr).getValue().longValue());
			if (line.imm != 0) {
				if (registers.get(csr).getName().equals("read_only")) {
					System.out.println("Error: Writing to a Read only Register");
				} else {
					registers.get(csr).setValue(registers.get(csr).getValue().longValue() & (~line.imm));
				}
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(this, csr);
				}
			}
			modifyPC();
		} else if (line.instructionType == InstructionType.mret) {
			long mpp = CommonLib.getValue(registers.get("mstatus").getValue().longValue(), 11, 12);
			if (mpp == 0) {
				CPUState.setPriv(0);
			} else if (mpp == 1) {
				CPUState.setPriv(1);
			} else if (mpp == 2) {
				CPUState.setPriv(2);
			} else if (mpp == 3) {
				CPUState.setPriv(3);
			}
			registers.get("pc").setValue(registers.get("mepc").getValue().longValue());
			long mstatus = registers.get("mstatus").getValue().longValue();
			//0x808-->0x800 
			long mpie = CommonLib.getValue(registers.get("mstatus").getValue().longValue(), 7, 7);
			long mpie_if_0 = 0xfffffff7;
			long mpie_if_1 = 0x8;

			if (mpie == 1) {
				mstatus = mstatus | mpie_if_1;
			} else if (mpie == 0) {
				mstatus = mstatus & mpie_if_0;
			}
			//mpie-->1
			mstatus = mstatus | 0x00000080;
			//mpp-->prv_u(0);
			mstatus = mstatus & 0xffffffffffffE7ffl;
			//mpv-->0
			mstatus = mstatus & 0xffffff7fffffffffl;
			registers.get("mstatus").setValue(BigInteger.valueOf(mstatus));
		} else if (line.instructionType == InstructionType.sret) {
			long spp = CommonLib.getValue(registers.get("mstatus").getValue().longValue(), 8, 8);
			if (spp == 0) {
				CPUState.setPriv(CPUState.USER_MODE);
			} else if (spp == 1) {
				CPUState.setPriv(CPUState.SUPERVISOR_MODE);
			}

			registers.get("pc").setValue(registers.get("sepc").getValue().longValue());
//			long sstatus = registers.get("mstatus").value.longValue();
//			long spie = CommonLib.getValue(registers.get("mstatus").value.longValue(), 5, 5);
//			long sie = spie;
//			sie = sie & 0xfL;
//			sstatus = sstatus | sie;
//			spie = 0x20L;
//			sstatus = sstatus | spie;
//			registers.get("mstatus").setValue(BigInteger.valueOf(sstatus));

			long mstatus = registers.get("mstatus").getValue().longValue();
			mstatus = mstatus & 0xfffffffffffffeffl;
			mstatus = mstatus | 2;
			registers.get("mstatus").setValue(BigInteger.valueOf(mstatus));

			long sstatus = registers.get("mstatus").getValue().longValue();
			sstatus = sstatus & 0xfffffffffffffeffl;
			sstatus = sstatus | 2;
			registers.get("mstatus").setValue(BigInteger.valueOf(sstatus));

//          should change the mstatus too (havn't finish);
		} else if (line.instructionType == InstructionType.sfence_vm) {
			modifyPC();
		} else {
			logger.log(Level.SEVERE, "unhandle : " + line.code);
		}
	}

	private ImmutablePair<Long, Integer> handleCompressed(Line line) throws NoOfByteException, ArrayIndexOutOfBoundsException, PageFaultException, IRQException, AccessFaultException {
		boolean branched = false;
		ImmutablePair pair = null;

		if ((line.instructionType == InstructionType.c_lui && line.rd == 2) || line.instructionType == InstructionType.c_addi16sp) {
			if (line.imm != 0) {
				registers.get("sp").setValue(registers.get("sp").getValue().longValue() + line.imm);
			}
		} else if (line.instructionType == InstructionType.c_addi4spn) {
//			if (line.imm != 0) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get("sp").getValue().longValue() + imm;
			registers.get(rd).setValue(value);
//			}

		} else if (line.instructionType == InstructionType.c_add) {
			String rd = Registers.getRegXNum32(line.rd);
			String rs1 = Registers.getRegXNum32(line.rs1);
			long value = registers.get(rd).getValue().longValue() + registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_addi) {
			String rd = Registers.getRegXNum32(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() + imm;
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_and) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rd).getValue().longValue() & registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_andi) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() & imm;
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_or) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rd).getValue().longValue() | registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_xor) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rd).getValue().longValue() ^ registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_sub) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rd).getValue().longValue() - registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_mv) {
			String rd = Registers.getRegXNum32(line.rd);
			String rs1 = Registers.getRegXNum32(line.rs1);
			long value = registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_li) {
			String rd = Registers.getRegXNum32(line.rd);
			long imm = line.imm;
			long value = registers.get("zero").getValue().longValue() + imm;
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_lui) {
			String rd = Registers.getRegXNum32(line.rd);
			registers.get(rd).setValue(signExtend64(line.imm << 12, 31));
		} else if (line.instructionType == InstructionType.c_slli) {
			String rd = Registers.getRegXNum32(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() << imm;
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_srai) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() >> imm;
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_srli) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() >>> imm;
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_beqz) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			if (registers.get(rs1).getValue().longValue() == 0) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				branched = true;
			}
		} else if (line.instructionType == InstructionType.c_bnez) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			if (registers.get(rs1).getValue().longValue() != 0) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				branched = true;
			}
		} else if (line.instructionType == InstructionType.c_sw) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
//			registers.get(rs1).value = (long) memory.get(value) & 0xffffffff;
			memory.write(cpuTick, value, registers.get(rs2).getValue().longValue(), 4, false, true);
			pair = new ImmutablePair(value, registers.get(rs2).getValue().longValue());
		} else if (line.instructionType == InstructionType.c_swsp) {
			String rs2 = Registers.getRegXNum32(line.rs2);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			System.out.println("write" + Long.toHexString((registers.get(rs2).getValue().longValue() & 0xff)) + "to" + Long.toHexString(value));
//			registers.get(rs2).value = (long) memory.get(value) & 0xffffffff;  System.out.println("write" + Integer.toHexString((int) (registers.get(rs2).value & 0xff)) + "to" + Integer.toHexString(int_val));
			// System.out.println("write" + Integer.toHexString((int) (registers.get(rs2).value & 0xff00)) + "to" + Integer.toHexString(int_val));
			memory.write(cpuTick, value, registers.get(rs2).getValue().longValue(), 4, false, true);
			pair = new ImmutablePair(value, registers.get(rs2).getValue().longValue());
		} else if (line.instructionType == InstructionType.c_fsw) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegFNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
//			registers.get(rs1).value = (long) memory.get(value) & 0xffffffff;
			memory.write(cpuTick, value, registers.get(rs2).getValue().longValue(), 4, false, true);
			pair = new ImmutablePair(value, registers.get(rs2).getValue().longValue());
//		} else if (line.instruction.equals("c.fswsp")) {
//			rs2 = Registers.getRegFNum32(line.rs2);
//			long_val = registers.get("sp").value.longValue() + ((int) line.imm);
////			registers.get(rs2).value = (long) memory.get(value) & 0xffffffff;
//			rd = Registers.getRegFNum32(line.rd);
//			if(rd.equals("ft1")){
//				memory.writeMemory(long_val, registers.get(rs2).getValue().longValue(), 4);			
//			}
		} else if (line.instructionType == InstructionType.c_fsd) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegFNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
//			registers.get(rs1).value = (long) memory.get(value) & 0xffffffff;
			memory.write(cpuTick, value, registers.get(rs2).getValue().longValue(), 8, false, true);
			pair = new ImmutablePair(value, registers.get(rs2).getValue().longValue());
		} else if (line.instructionType == InstructionType.c_sdsp) {
			String rs2 = Registers.getRegXNum32(line.rs2);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			memory.write(cpuTick, value, registers.get(rs2).getValue().longValue(), 8, false, true);
			pair = new ImmutablePair(value, registers.get(rs2).getValue().longValue());
		} else if (line.instructionType == InstructionType.c_sd) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			memory.write(cpuTick, value, registers.get(rs2).getValue().longValue(), 8, false, true);
			pair = new ImmutablePair(value, registers.get(rs2).getValue().longValue());
			if (value == 0x2004000L) {
				registers.get("mip").setValue(registers.get("mip").getValue().longValue() & ~0x80);
			}
		} else if (line.instructionType == InstructionType.c_j) {
			//registers.get().value = registers.get(rd).value >> imm;
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
			branched = true;
		} else if (line.instructionType == InstructionType.c_jr) {
			String rd = Registers.getRegXNum32(line.rd);
			registers.get("pc").setValue(registers.get(rd).getValue().longValue());
			if (line.code.equals("sret")) {
				if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
					long mstatus = registers.get("mstatus").getValue().longValue();
					mstatus = mstatus & 0xfffffeffl;
					mstatus = mstatus | 2;
					registers.get("mstatus").setValue(BigInteger.valueOf(mstatus));

					long sstatus = registers.get("mstatus").getValue().longValue();
					sstatus = sstatus & 0xfffffeffl;
					sstatus = sstatus | 2;
					registers.get("mstatus").setValue(BigInteger.valueOf(sstatus));
				}
			}
			branched = true;
		} else if (line.instructionType == InstructionType.c_lw) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long address = registers.get(rs1).getValue().longValue() + line.imm;
			long value = memory.read(address, 4, true, false);
			pair = new ImmutablePair(address, value);
			registers.get(rd).setValue(signExtend32To64(BigInteger.valueOf(value)));
		} else if (line.instructionType == InstructionType.c_lwsp) {
			String rd = Registers.getRegXNum32(line.rd);
			long address = registers.get("sp").getValue().longValue() + line.imm;
			if (!rd.equals("zero") && (line.imm % 4 == 0)) {
				long value = memory.read(address, 4, true, false);
				pair = new ImmutablePair(address, value);
				registers.get(rd).setValue(value);
			}
		} else if (line.instructionType == InstructionType.c_flw) {
			String rd = Registers.getRegFNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long address = registers.get(rs1).getValue().longValue() + line.imm;
			if (line.imm % 4 == 0) {
				long value = memory.read(address, 4, true, false);
				pair = new ImmutablePair(address, value);
				registers.get(rd).setValue(value);
			}

		} else if (line.instructionType == InstructionType.c_ld) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long address = registers.get(rs1).getValue().longValue() + line.imm;
			long value = memory.read(address, 8, true, false);
			pair = new ImmutablePair(address, value);
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_ldsp) {
			String rd = Registers.getRegXNum32(line.rd);
			long address = registers.get("sp").getValue().longValue() + line.imm;
			long value = memory.read(address, 8, true, false); //(memory.read(long_val + 3) & 0xff) << 24 | (memory.read(long_val + 2) & 0xff) << 16 | (memory.read(long_val + 1) & 0xff) << 8 | memory.read(long_val) & 0xff;
			pair = new ImmutablePair(address, value);
			registers.get(rd).setValue(value);
		} else if (line.instructionType == InstructionType.c_fld) {
			String rd = Registers.getRegFNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long address = registers.get(rs1).getValue().longValue() + line.imm;
			if (line.imm % 8 == 0) {
				long value = memory.read(address, 8, true, false);
				pair = new ImmutablePair(address, value);
				registers.get(rd).setValue(value);
			}
//		} else if (line.instructionType == InstructionType.c_fldsp) {
//			String rd = Registers.getRegFNum32(line.rd);
//			long value = registers.get("sp").getValue().longValue() + line.imm;
//
//			if (line.imm % 8 == 0) {
//				value = memory.read(value, 8, true, false);
//				registers.get(rd).setValue(value);
//			}
		} else if (line.instructionType == InstructionType.c_addw) {
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 2);
			String rd = Registers.getRegXNum16(line.rd);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rd).getValue().longValue() + registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(value);
			branched = true;
		} else if (line.instructionType == InstructionType.c_addiw) {
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 2);
			String rd = Registers.getRegXNum32(line.rd);
			long value = registers.get(rd).getValue().longValue() + line.imm;
			registers.get(rd).setValue(value);
			branched = true;
		} else if (line.instructionType == InstructionType.c_jalr) {
			String rd = Registers.getRegXNum32(line.rd);
			registers.get("ra").setValue(registers.get("pc").getValue().longValue() + 2);
			registers.get("pc").setValue(registers.get(rd).getValue().longValue() + line.imm);
			branched = true;
		} else if (line.instructionType == InstructionType.c_ebreak) {
		} else if (line.instructionType == InstructionType.c_subw) {
			String rs2 = Registers.getRegXNum16(line.rs2);
			String rd = Registers.getRegXNum16(line.rd);
			long val = registers.get(rd).getValue().longValue() - registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instructionType == InstructionType.c_nop) {
		} else {
			System.err.println("unhandled instruction " + line.instructionType + ", cpuTick=" + cpuTick + ", pc=" + registers.get("pc").getHexString());
			System.exit(3);
		}

		if (!branched) {
			modifyPC();
		}
		return pair;
	}

	private void handleTypeV(int arr[], Line line) throws ArrayIndexOutOfBoundsException, PageFaultException, NoOfByteException, IRQException {
		String rd = Registers.getRegXNum32(line.rd);
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		String vd = Registers.getVregNum32(line.vd);
		String vs1 = Registers.getVregNum32(line.vs1);
		String vs2 = Registers.getVregNum32(line.vs2);
		String vs3 = Registers.getVregNum32(line.vs3);
		String vma = Registers.getVregNum32(line.vma);
		String vta = Registers.getVregNum32(line.vta);
		String vsew = Registers.getVregNum32(line.vsew);
		String vlmul = Registers.getVregNum32(line.vlmul);
		int imm = (arr[3] << 4) | ((arr[2] & 0xf0) >> 4);
		int shamt = (arr[3] & 0x01) << 4 | (arr[2] & 0xf0) >> 4;
		long long_val;
		int int_val;
		if (line.instructionType == InstructionType.vadd_vv) {
//            System.out.println("vs1 = " + vs1);
//            System.out.println(vs1 + " = " + registers.get(vs1).getValue());
//            System.out.println("vs2 = " + vs2);
//            System.out.println(vs2 + " = " + registers.get(vs2).getValue());
//            System.out.println("vd = " + vd);
//            System.out.println(vd + " = " + registers.get(vd).getValue());
			long_val = registers.get(vs2).getValue().longValue() + registers.get(vs1).getValue().longValue();
			registers.get(vd).setValue(long_val);
//		} else if (line.instructionType == InstructionType.vle32_v) {
		}
		modifyPC();
	}

	public long Intpow2(int b) {
		long sum = 1;
//		System.out.println("b" + b);
		for (int i = 0; i < b; i++) {
			sum *= 2;
//			System.out.println(sum);
		}

		return sum;
	}

	public long Sign32ToUnsign32(String s) {
		long a = 0;

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '1') {

				a += Intpow2(s.length() - i - 1);

			}
		}
		return a;
	}

	public long signExtend64(long val, int signbit) { //Input the value, and the bit to sign extend.
		return (((val >> signbit) & 1) == 1) ? val | (0xffffffffffffffffL << signbit) : val & (0xffffffffffffffffL >>> (63 - signbit));
	}

	private String serializeHashmap(HashMap<String, Object> map, boolean appendComma) {
		StringBuilder sb = new StringBuilder();
		if (appendComma) {
			sb.append(",");
		}
		sb.append("{");

		sb.append("\"line\": \"" + map.get("line") + "\",");

		sb.append("\"bytes\": [");
		int[] bytes = (int[]) map.get("bytes");
		for (int x = 0; x < bytes.length; x++) {
			int b = bytes[x];
			if (x > 0) {
				sb.append(",");
			}
			sb.append("" + b + "");
		}
		sb.append("],");

		sb.append("\"registers\": {");
		LinkedHashMap<String, GeneralRegister> registers = (LinkedHashMap<String, GeneralRegister>) map.get("registers");
		int x = 0;
		for (String key : registers.keySet()) {
			if (x > 0) {
				sb.append(",");
			}
			sb.append("\"" + key + "\":{");
			sb.append("\"name\":\"" + key + "\",");
			sb.append("\"value\":" + registers.get(key).getValue() + "");
			sb.append("}");
			x++;
		}
		sb.append("},");

		sb.append("\"memoryOperands\": [");
		ArrayList<MemoryOperand> memoryOperands = (ArrayList<MemoryOperand>) map.get("memoryOperands");
		synchronized (memoryOperands) {
			x = 0;
			for (MemoryOperand memoryOperand : memoryOperands) {
//			System.out.println(memoryOperand);
				if (x > 0) {
					sb.append(",");
				}
				sb.append(" {");
				sb.append("\"operand\": \"" + memoryOperand.operand + "\",");
				sb.append("\"offset\": " + memoryOperand.offset + ",");
				sb.append("\"offsetAfterMapping\": " + memoryOperand.offsetAfterMapping + ",");
				sb.append("\"b\": " + memoryOperand.b + "");
				sb.append("}");
				x++;
			}
		}
		sb.append("]");
		sb.append("}");
		return sb.toString();
	}

	private void dumpToJson() {
		try {
			long pc = registers.get("pc").getValue().longValue();
			int[] ins;
			int noOfByte = RISCVDisassembler.getNoOfByte(memory.read(pc, true, false));
			if (noOfByte == 4) {
				ins = new int[4];
				ins[0] = memory.read(pc, true, false) & 0xff;
				ins[1] = memory.read(pc + 1, true, false) & 0xff;
				ins[2] = memory.read(pc + 2, true, false) & 0xff;
				ins[3] = memory.read(pc + 3, true, false) & 0xff;
			} else if (noOfByte == 2) {
				ins = new int[2];
				ins[0] = memory.read(pc, true, false) & 0xff;
				ins[1] = memory.read(pc + 1, true, false) & 0xff;
			} else {
				ins = new int[1];
				ins[0] = 99;
			}

			// get line object
			Line line = null;
			DecodeType type = RISCVDisassembler.getDecodeType(memory.read(pc, true, false) & 0x7F);
			if (type == DecodeType.decodeTypeB) {
				line = riscvDisassembler.decodeTypeB(ins);
			} else if (type == DecodeType.decodeTypeI) {
				line = riscvDisassembler.decodeTypeI(ins);
			} else if (type == DecodeType.decodeTypeS) {
				line = riscvDisassembler.decodeTypeS(ins);
			} else if (type == DecodeType.decodeTypeR) {
				line = riscvDisassembler.decodeTypeR(ins);
			} else if (type == DecodeType.decodeTypeU) {
				line = riscvDisassembler.decodeTypeU(ins);
			} else if (type == DecodeType.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU) {
				line = riscvDisassembler.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU(ins);
			} else if (type == DecodeType.jal) {
				line = riscvDisassembler.jal(ins);
			} else if (type == DecodeType.jalr) {
				line = riscvDisassembler.jalr(ins);
			} else if (type == DecodeType.fence) {
				line = riscvDisassembler.fence(ins);
			} else if (type == DecodeType.magic) {
				line = riscvDisassembler.decodeMagic(ins);
			} else if (type == DecodeType.compressed) {
				if (ins[0] == 0 && ins[1] == 0) {
					System.out.println("Bad bytes, exit, pc=" + registers.get("pc").getHexString());
					return;
				} else {
					line = riscvDisassembler.decodeCompressed(ins);
				}
			}
			// end get line object

			HashMap<String, Object> map = new HashMap();
			map.put("line", line.code);
			map.put("bytes", ins);
			map.put("registers", registers);
			map.put("memoryOperands", memory.lastOperands);
			FileUtils.writeStringToFile(new File(dumpJson), serializeHashmap(map, cpuTick > 0), "utf8", true);
			memory.resetLastOperands();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dumpToLog(ArrayList<Dwarf> dwarfArrayList, long pc, int[] opcodes, Line line, long cpuTick, boolean mem, String memOperation, boolean memRead, long memAddr, long memValue, int memSize) {
		try {
			memory.resetLastOperands();
			logThread.add(dwarfArrayList, pc, cpuTick, line.code, CommonLib.getHexString(opcodes, " "), mem, memOperation, memRead, memAddr, memValue, memSize, CPUState.priv, registers);
		} catch (SQLException ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}

	public void initRegistersAndMemory() throws FileNotFoundException, IOException, PageFaultException, IRQException {
		System.out.println("init registers and memory");
		registers.put("pc", new GeneralRegister("pc", startAddress));

		//General Purpose Registers
		registers.put("x0", new GeneralRegister("x0", 0x0) {
			@Override
			public BigInteger getValue() {
				return BigInteger.ZERO;
			}
		});

		for (int x = 1; x <= 31; x++) {
			registers.put("x" + x, new GeneralRegister("x" + x, 0x0));
		}
		registers.put("zero", registers.get("x0"));
		registers.put("ra", registers.get("x1"));
		registers.put("sp", registers.get("x2"));
		registers.put("gp", registers.get("x3"));
		registers.put("tp", registers.get("x4"));
		registers.put("t0", registers.get("x5"));
		registers.put("t1", registers.get("x6"));
		registers.put("t2", registers.get("x7"));
		registers.put("s0", registers.get("x8"));
		registers.put("s1", registers.get("x9"));
		registers.put("a0", registers.get("x10"));
		registers.put("a1", registers.get("x11"));
		registers.put("a2", registers.get("x12"));
		registers.put("a3", registers.get("x13"));
		registers.put("a4", registers.get("x14"));
		registers.put("a5", registers.get("x15"));
		registers.put("a6", registers.get("x16"));
		registers.put("a7", registers.get("x17"));
		registers.put("s2", registers.get("x18"));
		registers.put("s3", registers.get("x19"));
		registers.put("s4", registers.get("x20"));
		registers.put("s5", registers.get("x21"));
		registers.put("s6", registers.get("x22"));
		registers.put("s7", registers.get("x23"));
		registers.put("s8", registers.get("x24"));
		registers.put("s9", registers.get("x25"));
		registers.put("s10", registers.get("x26"));
		registers.put("s11", registers.get("x27"));
		registers.put("t3", registers.get("x28"));
		registers.put("t4", registers.get("x29"));
		registers.put("t5", registers.get("x30"));
		registers.put("t6", registers.get("x31"));

		//Floating Point Registers
		for (int x = 0; x <= 31; x++) {
			registers.put("f" + x, new GeneralRegister("f" + x, 0x0));
		}
		for (int x = 0; x <= 7; x++) {
			registers.put("ft" + x, registers.get("f" + x));
		}
		for (int x = 0; x <= 1; x++) {
			registers.put("fs" + x, registers.get("f" + (x + 8)));
		}
		for (int x = 0; x <= 7; x++) {
			registers.put("fa" + x, registers.get("f" + (x + 10)));
		}
		for (int x = 2; x <= 11; x++) {
			registers.put("fs" + x, registers.get("f" + (x + 16)));
		}
		for (int x = 8; x <= 11; x++) {
			registers.put("ft" + x, registers.get("f" + (x + 20)));
		}

		//CSR Registers. doc from https://five-embeddev.com/riscv-isa-manual/latest/priv-csrs.html#csrrwpriv
		registers.put("ustatus", new CSRRegister(0x000, 0x0));
		registers.put("uie", new CSRRegister(0x004, 0x0));
		registers.put("utvec", new CSRRegister(0x005, 0x0));
		registers.put("uscratch", new CSRRegister(0x040, 0x0));
		registers.put("uepc", new CSRRegister(0x041, 0x0));
		registers.put("ucause", new CSRRegister(0x042, 0x0));
		registers.put("utval", new CSRRegister(0x043, 0x0));
		registers.put("uip", new CSRRegister(0x044, 0x0));
		registers.put("fflags", new CSRRegister(0x001, 0x0));
		registers.put("frm", new CSRRegister(0x002, 0x0));
		registers.put("fcsr", new CSRRegister(0x003, 0x0));
		registers.put("cycle", new CSRRegister(0xC00, 0x0));
		registers.put("time", new CSRRegister(0xC01, 0x0));
		registers.put("instret", new CSRRegister(0xC02, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("hpmcounter" + x, new CSRRegister(0xC0 + x, 0x0));
		}
		registers.put("cycleh", new CSRRegister(0xC80, 0x0));
		registers.put("timeh", new CSRRegister(0xC81, 0x0));
		registers.put("instreth", new CSRRegister(0xC82, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("hpmcounter" + x + "h", new CSRRegister(0xC8 + x, 0x0));
		}
//		registers.put("sstatus", new CSRRegister(0x100, 0x0));
		registers.put("sedeleg", new CSRRegister(0x102, 0x0));
		registers.put("sideleg", new CSRRegister(0x103, 0x0));
//		registers.put("sie", new CSRRegister(0x104, 0x0));
		registers.put("stvec", new CSRRegister(0x105, 0x0));
		registers.put("scounteren", new CSRRegister(0x106, 0x0));
		registers.put("sscratch", new CSRRegister(0x140, 0x0));
		registers.put("sepc", new CSRRegister(0x141, 0x0));
		registers.put("scause", new CSRRegister(0x142, 0x0));
		registers.put("stval", new CSRRegister(0x143, 0x0));
		registers.put("sip", new CSRRegister(0x144, 0x0));
		registers.put("satp", new CSRRegister(0x180, 0x0));
		registers.put("scontext", new CSRRegister(0x5A8, 0x0));
		registers.put("hstatus", new CSRRegister(0x600, 0x0));
		registers.put("hedeleg", new CSRRegister(0x602, 0x0));
		registers.put("hideleg", new CSRRegister(0x603, 0x0));
		registers.put("hie", new CSRRegister(0x604, 0x0));
		registers.put("hcounteren", new CSRRegister(0x606, 0x0));
		registers.put("hgeie", new CSRRegister(0x607, 0x0));
		registers.put("htval", new CSRRegister(0x643, 0x0));
		registers.put("hip", new CSRRegister(0x644, 0x0));
		registers.put("hvip", new CSRRegister(0x645, 0x0));
		registers.put("htinst", new CSRRegister(0x64A, 0x0));
		registers.put("hgeip", new CSRRegister(0xE12, 0x0));
		registers.put("hgatp", new CSRRegister(0x680, 0x0));
		registers.put("hcontext", new CSRRegister(0x6A8, 0x0));
		registers.put("htimedelta", new CSRRegister(0x605, 0x0));
		registers.put("htimedeltah", new CSRRegister(0x615, 0x0));
		registers.put("vsstatus", new CSRRegister(0x200, 0x0));
		registers.put("vsie", new CSRRegister(0x204, 0x0));
		registers.put("vstvec", new CSRRegister(0x205, 0x0));
		registers.put("vsscratch", new CSRRegister(0x240, 0x0));
		registers.put("vsepc", new CSRRegister(0x241, 0x0));
		registers.put("vscause", new CSRRegister(0x242, 0x0));
		registers.put("vstval", new CSRRegister(0x243, 0x0));
		registers.put("vsip", new CSRRegister(0x244, 0x0));
		registers.put("vsatp", new CSRRegister(0x280, 0x0));
		registers.put("mvendorid", new CSRRegister(0xF11, 0x0)); //fixed id for hart 
		registers.put("marchid", new CSRRegister(0xF12, 0x0)); // fixed id for hart
		registers.put("mimpid", new CSRRegister(0xF13, 0x0));// fixed value 
		registers.put("mhartid", new CSRRegister(0xF14, 0x0));
		registers.put("mstatus", new CSRRegister(0x300, 0x0)); // machine mode
		registers.put("misa", new CSRRegister(0x301, 0x0));
		registers.put("medeleg", new CSRRegister(0x302, 0x0));
		registers.put("mideleg", new CSRRegister(0x303, 0x0));
		registers.put("mie", new CSRRegister(0x304, 0x0));
		registers.put("mtvec", new CSRRegister(0x305, 0x0));
		registers.put("mcounteren", new CSRRegister(0x306, 0x0));
		registers.put("mstatush", new CSRRegister(0x310, 0x0));
		registers.put("mscratch", new CSRRegister(0x340, 0x0));
		registers.put("mepc", new CSRRegister(0x341, 0x0));
		registers.put("mcause", new CSRRegister(0x342, 0x0));
		registers.put("mtval", new CSRRegister(0x343, 0x0));
		registers.put("mip", new CSRRegister(0x344, 0x0));
		registers.put("mtinst", new CSRRegister(0x34A, 0x0));
		registers.put("mtval2", new CSRRegister(0x34B, 0x0));
		for (int x = 0; x <= 3; x++) {
			registers.put("pmpcfg" + x, new CSRRegister(0x3A + x, 0x0));
		}
		for (int x = 0; x <= 15; x++) {
			registers.put("pmpaddr" + x, new CSRRegister(0x3B0 + x, 0x0));
		}
		registers.put("mcycle", new CSRRegister(0xB00, 0x0));
		registers.put("minstret", new CSRRegister(0xB02, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("mhpmcounter" + x, new CSRRegister(0xB00 + x, 0x0));
		}
		registers.put("mcycleh", new CSRRegister(0xB80, 0x0));
		registers.put("minstreth", new CSRRegister(0xB82, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("mhpmcounter" + x + "h", new CSRRegister(0xB80 + x, 0x0));
		}
		registers.put("mcountinhibit", new CSRRegister(0x320, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("mhpmevent" + x, new CSRRegister(0x320 + x, 0x0));
		}

//		registers.put("mnscratch", new CSRRegister(0x740, 0x0));
//		registers.put("mnepc", new CSRRegister(0x741, 0x0));
//		registers.put("mncause", new CSRRegister(0x742, 0x0));
//		registers.put("mnstatus", new CSRRegister(0x744, 0x0));
		registers.put("tselect", new CSRRegister(0x7A0, 0x0));
		registers.put("tdata1", new CSRRegister(0x7A1, 0x0));
		registers.put("tdata2", new CSRRegister(0x7A2, 0x0));
		registers.put("tdata3", new CSRRegister(0x7A3, 0x0));
		registers.put("mcontext", new CSRRegister(0x7A8, 0x0));
		registers.put("dcsr", new CSRRegister(0x7B0, 0x0));
		registers.put("dpc", new CSRRegister(0x7B1, 0x0));
		registers.put("dscratch0", new CSRRegister(0x7B2, 0x0));
		registers.put("dscratch1", new CSRRegister(0x7B3, 0x0));

		//Vector registers
		for (int x = 0; x <= 31; x++) {
			registers.put("v" + x, new GeneralRegister("read_and_write", 0x0));
		}

		//VCSR
		registers.put("vtype", new GeneralRegister("read_and_write", 0x0));
		registers.put("vsew", new GeneralRegister("read_and_write", 0x0));
		registers.put("vlmul", new GeneralRegister("read_and_write", 0x0));
		registers.put("vta", new GeneralRegister("read_and_write", 0x0));
		registers.put("vma", new GeneralRegister("read_and_write", 0x0));
		registers.put("vl", new GeneralRegister("read_and_write", 0x0));
		registers.put("vlenb", new GeneralRegister("read_and_write", 0x0));
		registers.put("vstart", new GeneralRegister("read_and_write", 0x0));
		registers.put("vxrm", new GeneralRegister("read_and_write", 0x0));
		registers.put("vxsat", new GeneralRegister("read_and_write", 0x0));
		registers.put("vcsr", new GeneralRegister("read_and_write", 0x0));

		if (Setting.getInstance() == null) {
			System.err.println("Setting's Instance (xml) is null");
			System.exit(123);
		}

		for (GeneralRegister register : Setting.getInstance().registers) {
			registers.get(register.getName()).setValue(register.getValue());
		}

//		((CSRRegister) registers.get("mstatus")).linkedRegister = (CSRRegister) registers.get("sstatus");
//		((CSRRegister) registers.get("sstatus")).linkedRegister = (CSRRegister) registers.get("mstatus");
//		((CSRRegister) registers.get("sie")).linkedRegister = (CSRRegister) registers.get("mie");
		//Memory Init
		for (MemoryMap memoryMap : Setting.getInstance().memoryMaps) {
			memoryMap.ram = new byte[memoryMap.size];
			memory.memoryMaps.put(memoryMap.start, memoryMap);
		}
		for (hk.quantr.riscv_simulator.setting.Memory m : Setting.getInstance().memory) {
			System.out.println(" - init memory " + m.name);
			String arr[] = m.value.split(",");
			int address = 0x1000;
			for (int x = 0; x < arr.length; x++) {
				String s = arr[x].trim();
				byte bytes[] = CommonLib.string2bytes(s);
				for (byte b : bytes) {
					memory.writeByte(address, b, true, false);
					address++;
				}
			}
		}
		PMP.init();
	}

	private void initKernel() throws IOException, PageFaultException, IRQException {
		System.out.println("init kernel");
		if (cmd.hasOption("binary")) {
			String temp = cmd.getOptionValue("binary");
			String binary = temp.split(",")[0];
			int loadAddress = (int) CommonLib.convertFilesize(temp.split(",")[1]);
			// load binary to memory
			byte[] bytes = Files.readAllBytes(Paths.get(binary));
			int offset = loadAddress;
			for (byte b : bytes) {
				memory.writeByte(offset, b, true, false);
				offset++;
			}
		} else if (cmd.hasOption("e")) {
			String elf = cmd.getOptionValue("e");

//			dwarf = new Dwarf();
//			dwarf.initElf(new File(elf), new File(elf).getName(), 0, false);
			File elfFile = new File(elf);
			if (!elfFile.exists()) {
				System.out.println(elfFile + " not exist");
				System.exit(9000);
			}
			dwarfArrayList = DwarfLib.init(elfFile, 0, false);
			RandomAccessFile rf = new RandomAccessFile(new File(elf), "r");
			for (Dwarf dwarf : dwarfArrayList) {
				for (Elf_Phdr programHeader : dwarf.programHeaders) {
					if (programHeader.getP_type() == 1) {
						System.out.printf("load file from offset 0x%x to memory offset 0x%x\n", programHeader.getP_offset(), programHeader.getP_paddr());
						rf.seek(programHeader.getP_offset().longValue());
						for (int x = 0; x < programHeader.getP_filesz().longValue(); x++) {
							byte b = rf.readByte();
//						System.out.printf("%8x = %x\n", programHeader.getP_paddr().longValue() + x, b);
							memory.writeByte(programHeader.getP_paddr().longValue() + x, b, true, false);
						}
					}
				}
			}
		}
	}

	private void initMemoryHandler() throws FileNotFoundException {
		System.out.println("init memory handler");
		UART uart = new UART(true, true, 0x10000000L, 8);
		memoryHandlers.add(uart);

		PLIC plic = new PLIC(true, true, 0xc000000L, 0x210000);
		memoryHandlers.add(plic);

		if (cmd.hasOption("file")) {
			String fsImgPath = cmd.getOptionValue("file");
			File file = new File(fsImgPath);
			if (!file.exists() || !file.isFile() || !file.canRead() || !file.canWrite()) {
				System.err.println(fsImgPath + " not exist/not file/can't read/can't write");
				System.exit(100);
			}
			Virtio virtio = new Virtio(this, true, true, 0x10001000L, 0xff, memory, file);
			memoryHandlers.add(virtio);
		}
	}

	private void zip(File file) {
		try {
			File archive = new File(file.getName() + ".zip");
			ZipArchiveOutputStream outputStream = new ZipArchiveOutputStream(archive);
			ZipArchiveEntry entry = new ZipArchiveEntry(file, file.getName());
			outputStream.setLevel(5);
			outputStream.setMethod(ZipEntry.DEFLATED);
			entry.setMethod(ZipEntry.DEFLATED);
			outputStream.putArchiveEntry(entry);

			InputStream is = new BufferedInputStream(new FileInputStream(file));
			byte[] buffer = new byte[1024 * 5];
			int len = -1;
			while ((len = is.read(buffer)) != -1) {
				outputStream.write(buffer, 0, len);
			}
			outputStream.closeArchiveEntry();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private BigInteger signExtend32To64(BigInteger value) {
		if (value.testBit(31)) {
			return value.or(BigInteger.valueOf(0xffffffff80000000l));
		} else {
			return value;
		}
	}

//	private boolean memoryHiJack(long cpuTick) {
//		if (hijack >= Setting.getInstance().memoryHiJacks.size()) {
//			return false;
//		} else {
//			return (Setting.getInstance().memoryHiJacks.get((int) hijack).getSequence() - 1 == cpuTick);
//		}
//	}
	long hijack = 0;

	private void handleMemoryHiJack(long cpuTick) throws PageFaultException, IRQException, AccessFaultException {
		long limit = Setting.getInstance().memoryHiJacks.size();
		MemoryHiJack temp;
		while (hijack < limit) {
			temp = Setting.getInstance().memoryHiJacks.get((int) hijack);
//			if (cpuTick == Setting.getInstance().memoryHiJacks.get(Math.max(0, (int) hijack - 1)).sequence) {
//				if (Setting.getInstance().memoryHiJacks.get(Math.max(0, (int) hijack - 1)).memAddr == 0xc201000l) { // read PLIC
//					registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 0x200);
//				} else if (Setting.getInstance().memoryHiJacks.get(Math.max(0, (int) hijack - 1)).memAddr == 0xc201004l) {
//					registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 0x200);
//				}
//			}
			// sequence is matched
			if (cpuTick == temp.sequence - 1) {
				// if instruction is loading from memory
				if (temp.memRead) {
					// side effect
					if (temp.memAddr == 0xc201004L) { // PLIC
						resetRegBit("mip", 9);
					} else if (temp.memAddr == 0xc202004L) {
						resetRegBit("mip", 11);
					}
					// block hijack to the specific addr
					if (temp.memAddr == 0x10000005L) {
					} else {
//						if (temp.memAddr == 0xc201004L) {
//							System.out.println("writting to PLIC c201004");
//						}
						memory.write(cpuTick, temp.memAddr, temp.memValue, temp.memSize, true, false);
					}
					hijack++;
				} else {
					// if instruction is writing to memory
					// side effect
					if (temp.memAddr == 0xc201000L) { // PLIC
						setRegBit("mip", 9);
					} else if (temp.memAddr == 0xc202000L) {
						setRegBit("mip", 11);
					} else if (temp.memAddr == 0x10000000L) {
						if ((registers.get("mip").getValue().longValue() & 0x2) == 0x2) {
							setRegBit("mip", 9);
						}
					}
//					else if (temp.memAddr == ADDRESS){}
					hijack++;
				}
			}
			if (temp.sequence > cpuTick + 1) {
				return;
			}
		}
	}

	public void setRegBit(String reg, int b) {// set single bit in register
		setRegBits(reg, new int[]{b});
	}

	public void setRegBits(String reg, int[] b) { // set multiple bits in register
		long bit = 0;
		for (int i = 0; i < b.length; i++) {
			bit = bit | (1 << b[i]);
		}
		registers.get(reg).setValue(registers.get(reg).getValue().longValue() | bit);
	}

	public void resetRegBit(String reg, int b) { // reset single bit in register
		resetRegBits(reg, new int[]{b});
	}

	public void resetRegBits(String reg, int[] b) { // reset multiple bits in register
		long bit = 0;
		for (int i = 0; i < b.length; i++) {
			bit = bit | (1 << b[i]);
		}
		registers.get(reg).setValue(registers.get(reg).getValue().longValue() & ~bit);
	}

	public void moveRegBit(String reg, int b) {
		// pending to implement
	}

	public long getCPUTick() {
		return this.cpuTick;
	}
}
