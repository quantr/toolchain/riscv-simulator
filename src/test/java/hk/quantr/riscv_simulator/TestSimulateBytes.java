//package hk.quantr.riscv_simulator;
//
//import hk.quantr.assembler.exception.NoOfByteException;
//import hk.quantr.riscv_simulator.cpu.MemoryMap;
//import java.io.ByteArrayInputStream;
//import org.junit.Test;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
//public class TestSimulateBytes {
//	
//	@Test
//	public void test() throws InterruptedException, NoOfByteException, Exception {
//		String data = ("s\n").repeat(4);
//		System.setIn(new ByteArrayInputStream(data.getBytes()));
//		Simulator simulator = new Simulator(null);
//		
//		simulator.initRegistersAndMemory();
//		
//		byte temp[] = new byte[1000];
//		MemoryMap memoryMap = new MemoryMap("code", 0x400000, 1000, temp);
//		simulator.memory.memoryMaps.put(0x400000l, memoryMap);
//		
//		byte temp2[] = new byte[0x10100];
//		MemoryMap dataMemory = new MemoryMap("data", 0x10000000l, 0x10100, temp2);
//		simulator.memory.memoryMaps.put(0x10000000l, dataMemory);
//		
//		simulator.memory.write(0x400000, (byte) 0x97, true);
//		simulator.memory.write(0x400001, (byte) 0x03, true);
//		simulator.memory.write(0x400002, (byte) 0xc1, true);
//		simulator.memory.write(0x400003, (byte) 0x0f, true);
//		
//		System.out.println("t2=" + simulator.registers.get("t2").getHexString());
//		
//		simulator.memory.write(0x400004, (byte) 0x93, true);
//		simulator.memory.write(0x400005, (byte) 0x83, true);
//		simulator.memory.write(0x400006, (byte) 0x03, true);
//		simulator.memory.write(0x400007, (byte) 0x00, true);
//		
//		simulator.memory.write(0x400008, (byte) 0x93, true);
//		simulator.memory.write(0x400009, (byte) 0x0e, true);
//		simulator.memory.write(0x40000a, (byte) 0x10, true);
//		simulator.memory.write(0x40000b, (byte) 0x00, true);
//		
//		simulator.memory.write(0x40000c, (byte) 0x23, true);
//		simulator.memory.write(0x40000d, (byte) 0x81, true);
//		simulator.memory.write(0x40000e, (byte) 0xd3, true);
//		simulator.memory.write(0x40000f, (byte) 0x01, true);
//		
//		simulator.registers.get("pc").setValue(0x400000);
//		try {
//			simulator.start_simulation();
//		} catch (Exception ex) {
//		}
//		System.out.println("t2=" + simulator.registers.get("t2").value);
//		System.out.printf("memory at 0x2=%x\n", simulator.memory.read(0x10010002, true, false));
//		
//	}
//}
