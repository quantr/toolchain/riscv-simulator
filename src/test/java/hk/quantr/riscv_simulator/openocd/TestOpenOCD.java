package hk.quantr.riscv_simulator.openocd;

import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.riscv.il.DisasmStructure;
import hk.quantr.assembler.riscv.il.Line;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.javalib.CommonLib;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.logging.Logger;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */

/*
 * Notes : run "make burn" in the example directory, to burn the a.elf to the board before run this program
 */
public class TestOpenOCD {

	public static final Logger logger = Logger.getLogger(TestOpenOCD.class.getName());
	static boolean runInPeterMac = false;

	static String mysql_username;
	static String mysql_password;
	static String mysql_host;
	static String mysql_database;
	static BasicDataSource ds = new BasicDataSource();

	static {
		if (TestOpenOCD.runInPeterMac) {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "localhost";
			mysql_database = "assembler";
		} else {
			mysql_username = "assembler";
			mysql_password = "assembler";
			mysql_host = "192.168.0.174";
			mysql_database = "assembler";
		}

		TestOpenOCD.ds.setUrl("jdbc:mysql://" + TestOpenOCD.mysql_host + "/" + TestOpenOCD.mysql_database + "?rewriteBatchedStatements=true");
		TestOpenOCD.ds.setUsername(TestOpenOCD.mysql_username);
		TestOpenOCD.ds.setPassword(TestOpenOCD.mysql_password);
		TestOpenOCD.ds.setMinIdle(5);
		TestOpenOCD.ds.setMaxIdle(10);
		TestOpenOCD.ds.setMaxOpenPreparedStatements(100000);
	}

	String elfFile = "/hk/quantr/riscv_simulator/example_code6/GD32VF103C_START.elf";
//	String textSectionFile = "/hk/quantr/riscv_simulator/example_code5/Running_Led.text";
	HashMap<Long, Long> memoryMapping = new HashMap() {
		{
			put(0x0L, 0x8000000L);
		}
	};

	@Test
	public void test() throws SocketException, IOException, Exception {
//		InputStream inputStream = TestOpenOCD.class.getResourceAsStream(textSectionFile);
		RISCVDisassembler disassembler = new RISCVDisassembler();
		File elf = new File(TestOpenOCD.class.getResource(elfFile).getPath());
//		FileInputStream input = new FileInputStream(elf);
		DisasmStructure struct = disassembler.disasm(elf, new String[]{".init", ".text"});

//		for (Line line : struct.lines) {
//			System.out.println(line);
//		}
//		if (1<2)
//			System.exit(-1);
		Dwarf dwarf = new Dwarf();
		dwarf.initElf(elf, elf.getName(), 0, true);
//		System.out.println(dwarf.ehdr.e_entry);

		new Thread() {
			@Override
			public void run() {
				try {
//		Process process = Runtime.getRuntime().exec("run.bat", null, new File("testbench/longan"));
//		Process process = Runtime.getRuntime().exec("dir", null, new File("testbench/longan"));
					Process process = Runtime.getRuntime().exec("openocd -f openocd_gdlink.cfg", null, new File("testbench/longan"));
					InputStream is = process.getErrorStream();
					BufferedReader br = new BufferedReader(new InputStreamReader(is));
					String str;
					while ((str = br.readLine()) != null) {
//						System.out.println("\nERR > " + str);
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}.start();

		System.out.println("start socket");

		Socket client = new Socket("localhost", 6666);
		OutputStream os = client.getOutputStream();
		InputStream is = client.getInputStream();

		System.out.println("waiting tcl reply");
		OpenOCDLib.write(os, "reset halt");
		System.out.println(OpenOCDLib.read(is));

		Class.forName("com.mysql.cj.jdbc.Driver");
//		Connection conn = DriverManager.getConnection("jdbc:mysql://" + mysql_host + "/" + mysql_database + "?rewriteBatchedStatements=true", mysql_username, mysql_password);
		Connection conn = ds.getConnection();

		String guid = UUID.randomUUID().toString();
		NamedPreparedStatement pstmt = NamedPreparedStatement.prepareStatement(conn, "INSERT INTO `cpuRecorder` VALUES (0, :guid, now(), :computer, :code, :zero,:ra,:sp,:gp,:tp,:t0,:t1,:t2,:fp,:s1,:a0,:a1,:a2,:a3,:a4,:a5,:a6,:a7,:s2,:s3,:s4,:s5,:s6,:s7,:s8,:s9,:s10,:s11,:t3,:t4,:t5,:t6,:pc,:mstatus,:misa,:mie,:mtvec,:mcounteren,:mhpmevent3,:mhpmevent4,:mhpmevent5,:mhpmevent6,:mhpmevent7,:mhpmevent8,:mhpmevent9,:mhpmevent10,:mhpmevent11,:mhpmevent12,:mhpmevent13,:mhpmevent14,:mhpmevent15,:mhpmevent16,:mhpmevent17,:mhpmevent18,:mhpmevent19,:mhpmevent20,:mhpmevent21,:mhpmevent22,:mhpmevent23,:mhpmevent24,:mhpmevent25,:mhpmevent26,:mhpmevent27,:mhpmevent28,:mhpmevent29,:mhpmevent30,:mhpmevent31,:mscratch,:mepc,:mcause,:mtval,:mip,:pmpcfg0,:pmpcfg1,:pmpcfg2,:pmpcfg3,:pmpaddr0,:pmpaddr1,:pmpaddr2,:pmpaddr3,:pmpaddr4,:pmpaddr5,:pmpaddr6,:pmpaddr7,:pmpaddr8,:pmpaddr9,:pmpaddr10,:pmpaddr11,:pmpaddr12,:pmpaddr13,:pmpaddr14,:pmpaddr15,:tselect,:tdata1,:tdata2,:tdata3,:dcsr,:dpc,:dscratch,:mcycle,:minstret,:mhpmcounter3,:mhpmcounter4,:mhpmcounter5,:mhpmcounter6,:mhpmcounter7,:mhpmcounter8,:mhpmcounter9,:mhpmcounter10,:mhpmcounter11,:mhpmcounter12,:mhpmcounter13,:mhpmcounter14,:mhpmcounter15,:mhpmcounter16,:mhpmcounter17,:mhpmcounter18,:mhpmcounter19,:mhpmcounter20,:mhpmcounter21,:mhpmcounter22,:mhpmcounter23,:mhpmcounter24,:mhpmcounter25,:mhpmcounter26,:mhpmcounter27,:mhpmcounter28,:mhpmcounter29,:mhpmcounter30,:mhpmcounter31,:mcycleh,:minstreth,:mhpmcounter3h,:mhpmcounter4h,:mhpmcounter5h,:mhpmcounter6h,:mhpmcounter7h,:mhpmcounter8h,:mhpmcounter9h,:mhpmcounter10h,:mhpmcounter11h,:mhpmcounter12h,:mhpmcounter13h,:mhpmcounter14h,:mhpmcounter15h,:mhpmcounter16h,:mhpmcounter17h,:mhpmcounter18h,:mhpmcounter19h,:mhpmcounter20h,:mhpmcounter21h,:mhpmcounter22h,:mhpmcounter23h,:mhpmcounter24h,:mhpmcounter25h,:mhpmcounter26h,:mhpmcounter27h,:mhpmcounter28h,:mhpmcounter29h,:mhpmcounter30h,:mhpmcounter31h,:cycle,:time,:instret,:hpmcounter3,:hpmcounter4,:hpmcounter5,:hpmcounter6,:hpmcounter7,:hpmcounter8,:hpmcounter9,:hpmcounter10,:hpmcounter11,:hpmcounter12,:hpmcounter13,:hpmcounter14,:hpmcounter15,:hpmcounter16,:hpmcounter17,:hpmcounter18,:hpmcounter19,:hpmcounter20,:hpmcounter21,:hpmcounter22,:hpmcounter23,:hpmcounter24,:hpmcounter25,:hpmcounter26,:hpmcounter27,:hpmcounter28,:hpmcounter29,:hpmcounter30,:hpmcounter31,:cycleh,:timeh,:instreth,:hpmcounter3h,:hpmcounter4h,:hpmcounter5h,:hpmcounter6h,:hpmcounter7h,:hpmcounter8h,:hpmcounter9h,:hpmcounter10h,:hpmcounter11h,:hpmcounter12h,:hpmcounter13h,:hpmcounter14h,:hpmcounter15h,:hpmcounter16h,:hpmcounter17h,:hpmcounter18h,:hpmcounter19h,:hpmcounter20h,:hpmcounter21h,:hpmcounter22h,:hpmcounter23h,:hpmcounter24h,:hpmcounter25h,:hpmcounter26h,:hpmcounter27h,:hpmcounter28h,:hpmcounter29h,:hpmcounter30h,:hpmcounter31h,:mvendorid,:marchid,:mimpid,:mhartid,:priv)");

		dumpCPUToMysql(pstmt, is, os, guid, dwarf.ehdr.getE_entry().longValue(), struct);
		for (int x = 0; x < 5000; x++) {
			logger.info(String.format("%d", x));
			OpenOCDLib.write(os, "step");
			OpenOCDLib.read(is);

			dumpCPUToMysql(pstmt, is, os, guid, dwarf.ehdr.getE_entry().longValue(), struct);

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select * 1;");
		}
		pstmt.executeBatch();

		System.out.println("socket end");
		client.close();
	}

	private void dumpCPUToMysql(NamedPreparedStatement pstmt, InputStream is, OutputStream os, String guid, long e_entry, DisasmStructure struct) throws IOException, SQLException {
		TreeMap<String, Long> registers = allRegisters(is, os);
		pstmt.setString("guid", guid);
		pstmt.setString("computer", System.getenv().get("COMPUTERNAME"));

		Line line = struct.findLine(e_entry, registers.get("pc"));
		pstmt.setString("code", null);
		if (line != null) {
			pstmt.setString("code", line.code);
		}

		for (Long key : memoryMapping.keySet()) {
			long pc = registers.get("pc");
			if (pc == 0x8000182) {
				System.out.println("debug");
			}
			if (pc > key) {
				line = struct.findLine(e_entry, pc);
				if (line == null) {
					line = struct.findLine(e_entry, pc + memoryMapping.get(key));
				}
				if (line != null) {
					pstmt.setString("code", line.code);
//					break;
				}
			}
		}
		for (String key : registers.keySet()) {
			pstmt.setLong(key, registers.get(key));
//				System.out.println(key + " = " + registers.get(key));
		}
//		System.out.println("pc=" + registers.get("pc"));
		pstmt.addBatch();
	}

	private TreeMap<String, Long> allRegisters(InputStream is, OutputStream os) throws IOException {
		OpenOCDLib.write(os, "ocd_reg");
		String temp = OpenOCDLib.read(is);
		TreeMap<String, Long> registers = new TreeMap<>();
		for (String line : temp.split("\n")) {
			if (line.contains("(")) {
				String registerId = line.split(" ")[0].replaceAll("\\D+", "");
				String registerName = line.split(" ")[1];
				OpenOCDLib.write(os, "ocd_reg " + registerName + " force");
				String temp2 = OpenOCDLib.read(is);
				long value = CommonLib.convertFilesize(temp2.split(":")[1]);
				registers.put(registerName, value);
			}
		}
		return registers;
	}
}
