package hk.quantr.riscv_simulator.openocd;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.TreeMap;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestOpenOCDGetAllRegisters {

	public static final Logger logger = Logger.getLogger(TestOpenOCDGetAllRegisters.class.getName());

	@Test
	public void test() throws SocketException, IOException, Exception {
		new Thread() {
			public void run() {
				try {
//		Process process = Runtime.getRuntime().exec("run.bat", null, new File("testbench/longan"));
//		Process process = Runtime.getRuntime().exec("dir", null, new File("testbench/longan"));
					Process process = Runtime.getRuntime().exec("openocd -f openocd_gdlink.cfg", null, new File("testbench/longan"));
					InputStream is = process.getErrorStream();
					BufferedReader br = new BufferedReader(new InputStreamReader(is));
					String str;
					while ((str = br.readLine()) != null) {
//						System.out.println("\nERR > " + str);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}.start();

		System.out.println("start socket");

		Socket client = new Socket("localhost", 6666);
//		client.setSoTimeout(100000);
		OutputStream os = client.getOutputStream();
		InputStream is = client.getInputStream();

		System.out.println("waiting tcl reply");
//		OpenOCDLib.write(os, "capture \"reset halt\"");
		OpenOCDLib.write(os, "reset halt");
		System.out.println(OpenOCDLib.read(is));

		TreeMap<String, Long> registers = allRegisters(is, os);

		System.out.println("socket end");
		client.close();
	}

	private TreeMap<String, Long> allRegisters(InputStream is, OutputStream os) throws IOException {
		OpenOCDLib.write(os, "ocd_reg");
		String temp = OpenOCDLib.read(is);
//		System.out.println(temp);
		TreeMap<String, Long> registers = new TreeMap<String, Long>();
		for (String line : temp.split("\n")) {
			if (line.contains("(")) {
				String registerId = line.split(" ")[0].replaceAll("\\D+", "");
				String registerName = line.split(" ")[1];
				System.out.printf("alter table cpuRecorder add column %s bigint default null;\n", registerName);
//				System.out.printf(":%s,", registerName);
			}
		}
		return registers;
	}
}
