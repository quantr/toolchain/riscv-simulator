package hk.quantr.riscv_simulator.timer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestTimer {

	int counter = 0;
	SimpleDateFormat sdf = new SimpleDateFormat("s.S");

	@Test
	public void test() throws InterruptedException {
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				counter++;
				if (counter % 1000 == 0) {
					System.out.println("> " + sdf.format(new Date()));
				}
			}
		};
		Timer timer = new Timer("Timer");
		System.out.println(sdf.format(new Date()));
		timer.schedule(task, 0L, 1L);
		while (true) {
			if (counter >= 2000) {
				System.out.println("close +" + counter);
				System.out.flush();
				timer.cancel();
			}
			Thread.sleep(10);

		}
	}
}
