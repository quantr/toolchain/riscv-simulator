package hk.quantr.riscv_simulator;

import hk.quantr.dwarf.dwarf.CompileUnit;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfDebugLineHeader;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.dwarf.DwarfLine;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestDwarf {

	@Test
	public void test() {
		File file = new File(TestFile1.class.getResource("/hk/quantr/riscv_simulator/xv6/kernel").getPath());
		final ArrayList<Dwarf> dwarfArrayList = DwarfLib.init(file, 0, false);
		for (Dwarf dwarf : dwarfArrayList) {
			System.out.println(dwarf);
			for (CompileUnit cu : dwarf.compileUnits) {
				System.out.println(cu);
				DwarfDebugLineHeader header = cu.dwarfDebugLineHeader;
				for (DwarfLine line : header.lines) {
					System.out.printf("%d, 0x%08x, %s\n", line.line_num, line.address, header.filenames.get((int) line.file_num).file.getAbsolutePath());
				}
			}
		}

		System.out.println(dwarfArrayList.size());
//		HashMap<Integer, BigInteger> ht = DwarfLib.getAddresses(dwarfArrayList, "kernel");
//		for (Integer key : ht.keySet()) {
//			System.out.println("Value of " + key + " is: " + ht.get(key));
//		}
	}
}
