/*
 * Copyright 2020 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.riscv_simulator;

import hk.quantr.assembler.exception.NoOfByteException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestFileMac {

	@Test
	public void test() throws FileNotFoundException, IOException, InterruptedException, NoOfByteException, Exception {
//		File file = new File("~/workspace/xv6-riscv/kernel/kernel".replaceFirst("^~", System.getProperty("user.home")));
		File file = new File("src/test/resources/hk/quantr/riscv_simulator/elf/csr_example0/a");

		String data = ("s\n").repeat(200000) + "q\n";
		InputStream stdin = System.in;
		try {
			System.setIn(new ByteArrayInputStream(data.getBytes()));
//			Simulator.main(new String[]{"--help"});
//			Simulator.main(new String[]{"--binary", file.getAbsolutePath() + ",0x000000" + ",0x8000000","-m", "1GB", "-s", "0x0000000", "-d", "dump.json"});
//			Simulator.main(new String[]{"--elf", file.getAbsolutePath(), "-m", "1GB", "-s", "0x0000000", "-d", "dump.json", "--map=0x0-0x2fffff=0x8000000", "--init", "init.txt"});
			Simulator.main(new String[]{"--elf", file.getAbsolutePath(), "-s", "0x1000", "-d", "dump.json", "--init", "quantr.xml"});
		} finally {
			System.setIn(stdin);
		}
	}
}
