/*
 * Copyright 2020 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.riscv_simulator;

import hk.quantr.assembler.exception.NoOfByteException;
import hk.quantr.assembler.RISCVDisassembler;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestDisasmString {

	@Test
	public void test() throws NoOfByteException, Exception {
		RISCVDisassembler disassembler = new RISCVDisassembler();
		int[] test = new int[4];
		test[0] =0x10;
		test[1] =0x2;
		test[2] =0x1;
		test[3] =0x13;
		
		//System.out.println(disassembler.decodeCompressed(test).code);
		System.out.println(disassembler.decodeCompressed(test));
	}
}
