package hk.quantr.riscv_simulator;

import com.thoughtworks.xstream.XStream;
import hk.quantr.riscv_simulator.cpu.Memory;
import hk.quantr.riscv_simulator.cpu.MemoryMap;
import hk.quantr.riscv_simulator.setting.FireInterrupt;
import hk.quantr.riscv_simulator.setting.Setting;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestSerialize {

	@Test
	public void test() {
		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.registerConverter(new LongConverter());
		xstream.alias("Setting", Setting.class);
		xstream.alias("MemoryMap", MemoryMap.class);
		xstream.alias("Memory", Memory.class);
		Setting setting = new Setting();
		String xml = xstream.toXML(setting);
		System.out.println(xml);
	}
}
