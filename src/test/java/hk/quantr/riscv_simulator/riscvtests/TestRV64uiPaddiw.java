package hk.quantr.riscv_simulator.riscvtests;

import hk.quantr.riscv_simulator.Simulator;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestRV64uiPaddiw {

//	@Test
//	public void test() throws Exception {
	public static void main(String... args) throws Exception {
		Simulator simulator = new Simulator(new String[]{
			"--elf",
			"/opt/riscv-tests/share/riscv-tests/isa/rv64ui-p-addiw",
			"-s",
			"0x1000",
			"--init=quantr.xml",
//			"-file=../xv6-riscv/fs.img",
			"--h2",
			"./riscv-tests/rv64ui-p-addi/database.mv.db"
		});
	}
}
