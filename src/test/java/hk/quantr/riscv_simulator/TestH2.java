package hk.quantr.riscv_simulator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestH2 {

	@Test
	public void test() throws SQLException {
		String connectionString = "jdbc:h2:./test.mv.db;PAGE_SIZE=4096000;CACHE_SIZE=1310720;";
		Connection conn = DriverManager.getConnection(connectionString, "sa", "");

		conn.createStatement().execute("drop table `quantr` if exists;");
		conn.createStatement().execute("create table `quantr`(id bigint auto_increment primary key, date datetime);");

		conn.close();
	}
}
