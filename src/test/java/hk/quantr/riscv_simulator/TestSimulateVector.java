//package hk.quantr.riscv_simulator;
//
//import hk.quantr.assembler.exception.NoOfByteException;
//import hk.quantr.riscv_simulator.cpu.MemoryMap;
//import java.io.ByteArrayInputStream;
//import org.junit.Test;
////importing awt class
//import java.awt.*;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// * copied and edited by Hansel to work on Vector Goal: Run Vector-vector add example from v extension spec
// *
// */
//public class TestSimulateVector {
//
//	@Test
//	public void test() throws InterruptedException, NoOfByteException, Exception {
//		//repeat(?) is how many instructions are run
//		String data = ("s\n").repeat(5);
//		System.setIn(new ByteArrayInputStream(data.getBytes()));
//		Simulator simulator = new Simulator(null);
//
//		simulator.initRegistersAndMemory();
//
//		byte temp[] = new byte[1000];
//		MemoryMap memoryMap = new MemoryMap("code", 0x400000, 1000, temp);
//		simulator.memory.memoryMaps.put(0x400000l, memoryMap);
//
//		byte temp2[] = new byte[0x10100];
//		MemoryMap dataMemory = new MemoryMap("data", 0x10000000l, 0x10100, temp2);
//		simulator.memory.memoryMaps.put(0x10000000l, dataMemory);
//
//		//instructions
//		simulator.memory.write(0x400000, (byte) 0x97, true);
//		simulator.memory.write(0x400001, (byte) 0x03, true);
//		simulator.memory.write(0x400002, (byte) 0xc1, true);
//		simulator.memory.write(0x400003, (byte) 0x0f, true);
//
////		System.out.println("t2=" + simulator.registers.get("t2").getHexString());
//		simulator.memory.write(0x400004, (byte) 0x93, true);
//		simulator.memory.write(0x400005, (byte) 0x83, true);
//		simulator.memory.write(0x400006, (byte) 0x03, true);
//		simulator.memory.write(0x400007, (byte) 0x00, true);
//
//		simulator.memory.write(0x400008, (byte) 0x93, true);
//		simulator.memory.write(0x400009, (byte) 0x0e, true);
//		simulator.memory.write(0x40000a, (byte) 0x10, true);
//		simulator.memory.write(0x40000b, (byte) 0x00, true);
//
//		simulator.memory.write(0x40000c, (byte) 0x23, true);
//		simulator.memory.write(0x40000d, (byte) 0x81, true);
//		simulator.memory.write(0x40000e, (byte) 0xd3, true);
//		simulator.memory.write(0x40000f, (byte) 0x01, true);
//
//		//vector instruction, vadd.vv v2, v0, v1
//		simulator.memory.write(0x400010, (byte) 0x57, true);
//		simulator.memory.write(0x400011, (byte) 0x81, true);
//		simulator.memory.write(0x400012, (byte) 0x00, true);
//		simulator.memory.write(0x400013, (byte) 0x02, true);
//
//		//demo
//		System.out.println("vadd.vv v2, v0, v1");
//		System.out.println("v2 = v0 + v1");
//		simulator.registers.get("v2").setValue(100);
//		System.out.println("v2=" + simulator.registers.get("v2").value);
//		simulator.registers.get("v1").setValue(100);
//		System.out.println("v1=" + simulator.registers.get("v1").value);
//		simulator.registers.get("v0").setValue(100);
//		System.out.println("v0=" + simulator.registers.get("v0").value);
//
//		//frame
//		Frame preFrame = new Frame("preFrame");
//		preFrame.setLayout(null);
//		preFrame.setSize(400, 600);
//		preFrame.setVisible(true);
//
//		Canvas preCanvas = new Canvas();
//		preCanvas.setSize(400, 300);
//		Color preColor = new Color((int) simulator.registers.get("v2").value.longValue(), (int) simulator.registers.get("v2").value.longValue(), (int) simulator.registers.get("v2").value.longValue());
//		preCanvas.setBackground(preColor);
//		preCanvas.setVisible(true);
//		preFrame.add(preCanvas);
//
//		simulator.registers.get("pc").setValue(0x400000);
//		try {
//			simulator.start_simulation();
//		} catch (Exception ex) {
//		}
//
//		Frame postFrame = new Frame("postFrame");
//		postFrame.setLayout(null);
//		postFrame.setSize(400, 600);
//		postFrame.setVisible(true);
//
//		Canvas postCanvas = new Canvas();
//		postCanvas.setSize(400, 300);
//		Color postColor = new Color((int) simulator.registers.get("v2").value.longValue(), (int) simulator.registers.get("v2").value.longValue(), (int) simulator.registers.get("v2").value.longValue());
//		postCanvas.setBackground(postColor);
//		postCanvas.setVisible(true);
//		postCanvas.setBackground(postColor);
//		postFrame.add(postCanvas);
//
////        System.out.println("t2=" + simulator.registers.get("t2").getValue().longValue());
//		System.out.println("v2=" + simulator.registers.get("v2").value.longValue());
//		System.out.printf("memory at 0x2=%x\n", simulator.memory.read(0x10010002, true, false));
//	}
//}
