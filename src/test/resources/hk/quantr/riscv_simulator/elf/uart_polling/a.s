.section .text
.globl _start
.option	norelax
.equ THR,		0x10000000
.equ IER,		0x10000001
.equ ISR,		0x10000002
.equ LCR,		0x10000003
.equ MCR,		0x10000004
.equ LSR,		0x10000005
.equ MSR,		0x10000006
.equ SPR,		0x10000007


_start:
		li	t1, 0x80
		li	t2, LCR
		sb	t1, 0(t2)

#DLL
		li	t1, 1
		li	t2, THR
		sb	t1, 0(t2)

#DLM
		li	t1, 0
		li	t2, THR
		sb	t1, 0(t2)


# set LCR to 0
		li	t1, 0x0
		li	t2, LCR
		sb	t1, 0(t2)

# UAT send

		li	t1,THR
		li	t4,0

label1:
		la	t2, data1
		add	t2, t2, t4
		lb	t3, (t2)

		sb	t3, 0(t1)
		add t4, t4, 1

		li	t5,12
		beq t4, t5, reset
		j label1

reset:
		li	t4,0
		j label1
		
.section .data
data1:
		.string "0023456789"
		.byte 0xa
