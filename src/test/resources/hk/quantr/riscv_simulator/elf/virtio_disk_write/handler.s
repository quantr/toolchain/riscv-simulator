.equ PLIC_SCLAIM,		0xc200004

label1:

	li	t2, PLIC_SCLAIM
	lw  t1, 0(t2)

	sw	t1, 0(t2)

	mret
