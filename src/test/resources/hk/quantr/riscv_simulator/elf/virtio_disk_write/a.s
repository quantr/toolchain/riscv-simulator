.section .text
.globl _start
.option	norelax

.equ PLIC,							0xc000000
#.equ PLIC_PRIORITY,				PLIC
.equ PLIC_MENABLE,					0xc002000
.equ PLIC_MTHRESHOLD,				0xc200000
.equ PLIC_UART0_IRQ,				0xc000028
.equ PLIC_VIRTIO0_IRQ,				0xc000004
.equ VIRTIO0_IRQ,					1
.equ VIRTIO_MMIO_MAGIC_VALUE,		0x000
.equ VIRTIO_MMIO_VERSION,			0x004
.equ VIRTIO_MMIO_DEVICE_ID,			0x008 
.equ VIRTIO_MMIO_VENDOR_ID,			0x00c 
.equ VIRTIO_MMIO_DEVICE_FEATURES,	0x010
.equ VIRTIO_MMIO_DRIVER_FEATURES,	0x020
# .equ VIRTIO_MMIO_GUEST_PAGE_SIZE,	0x028
.equ VIRTIO_MMIO_QUEUE_SEL,			0x030 
.equ VIRTIO_MMIO_QUEUE_NUM_MAX,		0x034 
.equ VIRTIO_MMIO_QUEUE_NUM,			0x038 
# .equ VIRTIO_MMIO_QUEUE_ALIGN,		0x03c 
#.equ VIRTIO_MMIO_QUEUE_PFN,			0x040 
.equ VIRTIO_MMIO_QUEUE_READY,		0x044 
.equ VIRTIO_MMIO_QUEUE_NOTIFY,		0x050 
.equ VIRTIO_MMIO_INTERRUPT_STATUS,	0x060
.equ VIRTIO_MMIO_INTERRUPT_ACK,		0x064 
.equ VIRTIO_MMIO_STATUS,			0x070 
.equ VIRTIO_MMIO_QUEUE_DESC_LOW,	0x080 # physical address for descriptor table, write-only
.equ VIRTIO_MMIO_QUEUE_DESC_HIGH,	0x084
.equ VIRTIO_MMIO_DRIVER_DESC_LOW,	0x090 # physical address for available ring, write-only
.equ VIRTIO_MMIO_DRIVER_DESC_HIGH,	0x094
.equ VIRTIO_MMIO_DEVICE_DESC_LOW,	0x0a0 # physical address for used ring, write-only
.equ VIRTIO_MMIO_DEVICE_DESC_HIGH,	0x0a4
.equ VIRTIO0,						0x10001000
.equ VIRTIO_BLK_REQ_ADDR,			0x80009000
.equ BUF_ADDR,						0x8000a000
.equ VRING_DESC_F_NEXT,				1 # chained with another descriptor
.equ VRING_DESC_F_WRITE,			2 # This marks a buffer as write-only (otherwise read-only) 
.equ VRING_DESC_F_INDIRECT,			4 # This means the buffer contains a list of buffer descriptors
.equ VRING_HIDDEN_IS_CHAIN,			256
.equ VIRTIO_BLK_T_IN,				0 # read the disk
.equ VIRTIO_BLK_T_OUT,				1 # write the disk

_start:
	li t0,0x80005000
	csrrs t1, mtvec, t0

	li	t1, 1
	li	t2, PLIC_VIRTIO0_IRQ
	sw	t1, 0(t2)

	li	t1, 0x2
	li	t2, PLIC_MENABLE
	sw	t1, 0(t2)

	li	t1, 0
	li	t2, PLIC_MTHRESHOLD
	lw	t1, 0(t2)

	# init virtio
	# la t2, VIRTIO0+VIRTIO_MMIO_VERSION
	# lw t1, 0(t2)

	# la t2, VIRTIO0+VIRTIO_MMIO_DEVICE_ID
	# lw t1, 0(t2)
 
	# reset device
	li t1, 0x0
	li t2, VIRTIO0+VIRTIO_MMIO_STATUS
	sw t1,0(t2)
	
	# set ACKNOWLEDGE status bit
	li t1, 0x1
	li t2, VIRTIO0+VIRTIO_MMIO_STATUS
	sw t1,0(t2)

	# set DRIVER status bit
	li t1, 0x3
	li t2, VIRTIO0+VIRTIO_MMIO_STATUS
	sw t1,0(t2)

	# negotiate features
	li t1, 0x6654
	li t2, VIRTIO0+VIRTIO_MMIO_DRIVER_FEATURES
	sw t1,0(t2)

	# tell device that feature negotiation is complete.
	li t1, 0xb
	li t2, VIRTIO0+VIRTIO_MMIO_STATUS
	sw t1,0(t2)

	# initialize queue 0
	li t1, 0x0
	li t2, VIRTIO0+VIRTIO_MMIO_QUEUE_SEL
	sw t1,0(t2)

	# set queue size
	li t1, 0x8
	li t2, VIRTIO0+VIRTIO_MMIO_QUEUE_NUM
	sw t1,0(t2)

	# set all addresses
	## desc
	li t1, 0x80007000
	li t2, VIRTIO0+VIRTIO_MMIO_QUEUE_DESC_LOW
	sw t1,0(t2)

	## avail
	li t1, 0x80007080
	li t2, VIRTIO0+VIRTIO_MMIO_DRIVER_DESC_LOW
	sw t1,0(t2)

	## used
	li t1, 0x80007300
	li t2, VIRTIO0+VIRTIO_MMIO_DEVICE_DESC_LOW
	sw t1,0(t2)

	# li t1, 0x9
	# li t2, VIRTIO0+VIRTIO_MMIO_STATUS
	# sw t1,0(t2)

	# li t1, 0x1000
	# li t2, VIRTIO0+VIRTIO_MMIO_GUEST_PAGE_SIZE
	# sw t1,0(t2)

	# li t1, 0x8	
	# li t2, VIRTIO0+VIRTIO_MMIO_QUEUE_NUM
	# sw t1,0(t2)

	# queue is ready.
  	#*R(VIRTIO_MMIO_QUEUE_READY) = 0x1;
	li t1, 0x1	
	li t2, VIRTIO0+VIRTIO_MMIO_QUEUE_READY
	sw t1,0(t2)

	# tell device we're completely ready.
	#status |= VIRTIO_CONFIG_S_DRIVER_OK;
	#*R(VIRTIO_MMIO_STATUS) = status;
	li t1, 0xf
	li t2, VIRTIO0+VIRTIO_MMIO_STATUS
	sw t1,0(t2)
	
	# setup virtio_blk_req
	#struct virtio_blk_req {
	#	uint32 type; // VIRTIO_BLK_T_IN or ..._OUT
	#	uint32 reserved;
	#	uint64 sector;
	#};
	li t1, VIRTIO_BLK_T_OUT
	li t2, VIRTIO_BLK_REQ_ADDR+0
	sw t1, 0(t2)

	li t1, 0
	li t2, VIRTIO_BLK_REQ_ADDR+4
	sw t1, 0(t2)

	li t1, 1 # disk offset, so load the fs.img started at 0x200
	li t2, VIRTIO_BLK_REQ_ADDR+8
	sd t1, 0(t2)
	# end setup virtio_blk_req

	# Build descriptor table
	# descriptor[0]
	#struct virtq_desc {
	#	uint64 addr;
	#	uint32 len;
	#	uint16 flags;
	#	uint16 next;
	#};
	li t1, VIRTIO_BLK_REQ_ADDR
	li t2, 0x80007000+0
	sd t1, 0(t2)

	li t1, 16
	li t2, 0x80007000+0x8
	sw t1, 0(t2)

	li t1, VRING_DESC_F_NEXT
	li t2, 0x80007000+0xc
	sh t1, 0(t2)

	li t1, 1
	li t2, 0x80007000+0xe
	sh t1, 0(t2)

	# descriptor[1]
	li t1, 0x80006000		# data will be loaded into here
	li t2, 0x80007000+0x10
	sd t1, 0(t2)

	li t1, 512
	li t2, 0x80007000+0x18
	sw t1, 0(t2)

	li t1, VRING_DESC_F_NEXT
	li t2, 0x80007000+0x1c
	sh t1, 0(t2)

	li t1, 2
	li t2, 0x80007000+0x1e
	sh t1, 0(t2)
	
	# descriptor[2]
	li t1, 0x80006500
	li t2, 0x80007000+0x20
	sd t1, 0(t2)

	li t1, 0x1
	li t2, 0x80007000+0x28
	sw t1, 0(t2)

	li t1, VRING_DESC_F_WRITE
	li t2, 0x80007000+0x2c
	sh t1, 0(t2)

	li t1, 0
	li t2, 0x80007000+0x2e
	sh t1, 0(t2)

	# avail
	# struct virtq_avail {
	# 	uint16 flags; // always zero
	# 	uint16 idx;   // driver will write ring[idx] next
	# 	uint16 ring[NUM]; // descriptor numbers of chain heads
	# 	uint16 unused;
	# };

	li t1, 0x0
	li t2, 0x80007080+0
	sh t1, 0(t2)

	li t1, 0x1
	li t2, 0x80007080+2
	sh t1, 0(t2)

	li t1, 0x0
	li t2, 0x80007080+4
	sd t1, 0(t2)

	li t1, 0x0
	li t2, 0x80007080+12
	sd t1, 0(t2)

	li t1, 0x0
	li t2, 0x80007080+20
	sh t1, 0(t2)

	li t1, 0x0
	li t2, VIRTIO0+VIRTIO_MMIO_QUEUE_NOTIFY
	sw t1,0(t2)
	# end init virtio
	
	li t0,0x888
	csrrs t1, mie, t0

	li t0,0x1808
	csrrs t1, mstatus, t0

label2:
	j label2
		
.section .data
	.string "Peter Cheung"