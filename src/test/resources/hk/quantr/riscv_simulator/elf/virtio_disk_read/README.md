# Info

[](https://www.linux-kvm.org/page/Virtio)

[](https://www.redhat.com/en/blog/virtqueues-and-virtio-ring-how-data-travels)

![](https://blog.csdn.net/anyegongjuezjd/article/details/128500870)

# Debug

step 1: make

step 2: make debug

step 3: open new terminal and type riscv64-unknown-elf-gdb, it reads .gdbinit, otherwise: "riscv64-unknown-elf-gdb -x .gdbinit"

step 4: set breakpoint at 0x80005000, it should already did in .gdbinit

step 5: the bytes will be loaded from fs.img at offset 0x200 to memory 0x80006000, x /400bx 0x80006000

![](https://peter.quantr.hk/wp-content/uploads/2023/12/virtio_disk_read-screen1.png)
