.section .text
.global _start
.option	norelax

_start:
	la		t1, label1
	li		t2, 0x12345678abcdef99
	sb		t2, 0(t1) 
	
	la		t1, label2
	li		t2, 0x12345678abcdef99
	sh		t2, 0(t1) 
	
	la		t1, label3
	li		t2, 0x12345678abcdef99
	sw		t2, 0(t1) 
	
	la		t1, label4
	li		t2, 0x12345678abcdef99
	sd		t2, 0(t1) 
	
end1:
	j		end1

.align 12
.data
label1:
	.dword 0x0
label2:
	.dword 0x0
label3:
	.dword 0x0
label4:
	.dword 0x0

	