.equ PLIC_SCLAIM,		0xc200004
.equ THR,		0x10000000

label1:

	li	t3, THR
	lb  t4, 0(t3)

	li	t2, PLIC_SCLAIM
	lw  t1, 0(t2)

	sw	t1, 0(t2)

	mret
