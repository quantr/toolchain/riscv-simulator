.section .text
.globl _start
.option	norelax
.equ THR,		0x10000000
.equ IER,		0x10000001
.equ ISR,		0x10000002
.equ FCR,		0x10000002
.equ LCR,		0x10000003
.equ MCR,		0x10000004
.equ LSR,		0x10000005
.equ MSR,		0x10000006
.equ SPR,		0x10000007
.equ MTIMECMP, 	0x2004000
.equ MTIME, 	0x200bff8

.equ PLIC,		0xc000000
#.equ PLIC_PRIORITY,		PLIC
.equ PLIC_MENABLE,		0xc002000
.equ PLIC_MTHRESHOLD,	0xc200000
.equ PLIC_UART0_IRQ,	0xc000028
.equ UART0_IRQ,	10
.equ VIRTIO0_IRQ, 1

_start:
	li t0,0x80005000
	csrrs t1, mtvec, t0


	li	t1, 1
	li	t2, PLIC_UART0_IRQ
	sw	t1, 0(t2)



	li	t1, 0x400
	li	t2, PLIC_MENABLE
	sw	t1, 0(t2)

	#li	t1, 0
	#li	t2, PLIC_PRIORITY
	#sw	t1, 0(t2)

	li	t1, 0
	li	t2, PLIC_MTHRESHOLD
	sw	t1, 0(t2)


	li	t1, 0
	li	t2, IER
	sb	t1, 0(t2)


	li	t1, 0x80
	li	t2, LCR
	sb	t1, 0(t2)

	
	li	t1, 3
	li	t2, THR
	sb	t1, 0(t2)

	li	t1, 0
	li	t2, IER
	sb	t1, 0(t2)

	li	t1, 3
	li	t2, LCR
	sb	t1, 0(t2)


	li	t1, 0x7
	li	t2, FCR
	sb	t1, 0(t2)


	li	t1, 0x1
	li	t2, IER
	sb	t1, 0(t2)


	li t0,0x888
	csrrs t1, mie, t0

	li t0,0x1808
	csrrs t1, mstatus, t0

label2:
	j label2
		