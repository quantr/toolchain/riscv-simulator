.section .text
.globl _start
.option	norelax
_start:
	li x1, 0x80001000
	addi x2,x0,1
	sb x2,0(x1)

	srli	x3,x1,2
	csrw 	pmpaddr0, x3

	# set pmpcfg0, A = 1
	li		x4,0x8f
	csrw 	pmpcfg0, x4
	li x5, 0x200c000

store:
	sb x2,8(x5)
	sb x2,128(x5)
	sb x2,64(x5)
