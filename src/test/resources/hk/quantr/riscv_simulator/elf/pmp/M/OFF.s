.section .text
.globl _start
.option	norelax
_start:
	li x1, 0x80001000
	addi x2,x0,1
	sb x2,0(x1)

	# setup pmp first by setting pmpaddr0
	# to lock 0x80001000 to 0x80001128
	srli	x3,x1,2
	addi	x3,x3,0xf
	csrw 	pmpaddr0, x3

	# set pmpcfg0, A = 0
	li		x4,0x80
	csrw 	pmpcfg0, x4
	li x5, 0x80000ffe
	
repeat:
	sb x2,0(x5)
	sb x2,128(x1)
	sb x2,64(x1)
	#j  repeat
