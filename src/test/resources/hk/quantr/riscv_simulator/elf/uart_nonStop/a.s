.section .text
.global _start
.global os_loop
.option	norelax
.equ MTIMECMP,	0x2004000
.equ MTIME,		0x200bff8
.equ THR,		0x10000000
.equ IER,		0x10000001
.equ ISR,		0x10000002
.equ LCR,		0x10000003
.equ MCR,		0x10000004
.equ LSR,		0x10000005
.equ MSR,		0x10000006
.equ SPR,		0x10000007

_start:
	#li		t0, 0x1000
	#li		t1, MTIMECMP
	#sw		t0, 0(t1)

	#li		t0, 0x1808
	#csrrs	t1, mstatus, t0

	#la		t0, interrupt_handler
	#csrrs	t1, mtvec, t0
	
	#li		t0, 0x80
	#csrrs	t1, mie, t0

	li		t5, 0

os_loop:
	#wait uart
	li		t1, LSR
	li		t2, 0
wait_uart:
	lb		t0, 0(t1)
	and		t0, t0, 0b100000
	beq		t0, t2, wait_uart

	#write uart
	li		t1, THR
	li		t0, '1'
	sb		t0, 0(t1)

	#j	os_loop

	addi	t6,t5,0
	andi	t6,t6,1
	li		t0,0
	li		t1,1
	
	beq		t6, t0, task1_start
	beq		t6, t1, task2_start
	
	j		os_loop
		
		