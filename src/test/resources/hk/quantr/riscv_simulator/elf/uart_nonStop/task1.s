.section .text
.globl task1_start
.option	norelax
.equ THR,		0x10000000
.equ IER,		0x10000001
.equ ISR,		0x10000002
.equ LCR,		0x10000003
.equ MCR,		0x10000004
.equ LSR,		0x10000005
.equ MSR,		0x10000006
.equ SPR,		0x10000007


task1_start:
		li	t1,THR
		li	t0,'2'
		sb	t0, 0(t1)
		li t5, 1
		j os_loop
		
		