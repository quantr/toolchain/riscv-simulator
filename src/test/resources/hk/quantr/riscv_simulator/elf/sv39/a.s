.section .text
.globl _start
.option	norelax

_start:
li t2, 0x803
 
#将 mstatus 寄存器的 MPP 位域清为01, 01=supervisor mode
csrrw	x0, mstatus, t2

#将后面的标签 1 所在的 PC 地址赋值给 t0
la t0, label1

#将 t0 的值赋值给 CSR 寄存器 mepc
csrw mepc, t0

#执行 mret 指令,则会将模式切换到Supervisor Mode,并且从前的标签 1 处开始执行
#程序(标签 1 即为 mret 的下一条指令的位置)
mret

label1: 
	la		a5, page_start
	srli	a5,a5,12
	lui		a6, 0x80000
	slli	a6, a6, 32
	or		a5, a5, a6

	
	csrrw	zero, satp, a5
	sfence.vma zero,zero

	li		t2, 0x80009000

loop1:
	lw		t1,(t2)
	j		loop1

.section .data
.align 12

page_start:
		.dword 0x0000000000000000
		.dword 0x0000000000000000
		.dword 0x20000801
		#.dword 0x2000000b

.align 12

page2nd:
		.dword 0x20000C01

.align 12

page3rd:
		.dword 0x2000002b
		#.dword 0x0000000000000000
		.dword 0x0000000000000000
		.dword 0x0000000000000000
		.dword 0x0000000000000000
		.dword 0x0000000000000000
		.dword 0x0000000000000000
		.dword 0x0000000000000000
		.dword 0x0000000000000000
		.dword 0x0000000000000000
		#.dword 0x0000000000000000
		.dword 0x2000002b
