set confirm off
set architecture riscv:rv64
target remote 127.0.0.1:25501
symbol-file a
set disassemble-next-line auto
set riscv use-compressed-breakpoints yes
b *0x80000000
b *0x80005000
c
p /x $mstatus
p /x $sstatus
