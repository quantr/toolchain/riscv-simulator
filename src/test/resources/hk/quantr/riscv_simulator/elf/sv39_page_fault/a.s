.section .text
.globl _start
.option	norelax

_start:
	li		t0,0x80005000
	csrrs	t1, mtvec, t0

	li		t2, 0x803

	#Set the MPP bit field cleared by mstatus monitoring to 01, 01=supervisor mode
	csrrw	x0, mstatus, t2

	#Assign the PC address of the following label1 to t0
	la		t0, label1

	#Assign the value of t0 to the CSR register mepc
	csrw	mepc, t0

	#Execute the mret command, it will switch the mode to Supervisor Mode, and start execution from the previous label1.
	#Program (label1 is the position of the next instruction of mret)
	mret

label1:
	la		a5, page_start
	srli	a5,a5,12
	lui		a6, 0x80000
	slli	a6, a6, 32
	or		a5, a5, a6


	csrrw	zero, satp, a5
	sfence.vma zero,zero

	li		t2, 0x80009000
cd
loop1:
	lw		t1,(t2)
	j		loop1

.section .data
.align 12

page_start:
	.dword 0x0000000000000000
	.dword 0x0000000000000000
	.dword 0x20000801
	#.dword 0x2000000b

.align 12

page2nd:
	.dword 0x20000C01

.align 12

page3rd:
	.dword 0x2000002b
	.dword 0x0000000000000000
	.dword 0x0000000000000000
	.dword 0x0000000000000000
	.dword 0x0000000000000000
	.dword 0x0000000000000000
	.dword 0x0000000000000000
	.dword 0x0000000000000000
	.dword 0x0000000000000000
	.dword 0x2000002a
