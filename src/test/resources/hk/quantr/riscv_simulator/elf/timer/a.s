.section .text
.globl _start
.option	norelax
.option arch, +zicsr
.equ MTIMECMP, 0x2004000
.equ MTIME, 0x200bff8

_start:
		li t0, 0x20000
		li t1, MTIMECMP
		sw t0, 0(t1)

		li t0,0x1808
		csrrs t1, mstatus, t0

		li t0,0x80001000
		csrrs t1, mtvec, t0
		
		li t0,0x80
		csrrs t1, mie, t0

label1:
		j label1
