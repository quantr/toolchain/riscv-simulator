# Introduction

This example, the timer interrupt will keep firing. Remember these two things:
1. no need to clear MIE.MTIP, it is always wired to MTIME>MTIMECMP
2. without calling mret, timer interrupt won't fire again

step 1: make clean all debug
step 2: riscv64-unknown-elf-gdb --init-command=.gdbinit
