FROM maven:3.9.7 as builder
WORKDIR /riscv-simulator
COPY . .
RUN mvn -DskipTests package

FROM openjdk:24-jdk
WORKDIR /app
COPY --from=builder /riscv-simulator/target/riscv_simulator-*-dep*.jar .
ENTRYPOINT ["java", "-jar", "/app/riscv_simulator-1.0-jar-with-dependencies.jar"]

