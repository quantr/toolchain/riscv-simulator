# Introduction

This is a RISC-V simulator written in Java

# Build

```
mvn clean package
```

# Bug

```
mvn -Dexec.args="-classpath %classpath hk.quantr.riscv_simulator.Simulator" -Dexec.executable="C:\Program Files\Java\jdk-14.0.1\bin\java.exe" -Dfile.encoding=UTF-8 org.codehaus.mojo:exec-maven-plugin:1.5.0:exec
```

!!! mvn install this proect can fix no auto flush to console https://github.com/quantrpeter/exec-maven-plugin

# Gen dependency graph

```
mvn dependency:tree -DoutputType=dot -DoutputFile=a.dot
dot a.dot -Tpng -o a.png
```

# Run

java -Xmx100G -jar target/riscv_simulator-*-jar-with-dependencies.jar --elf ../xv6-riscv/kernel/kernel -s 0x1000 --init=quantr.xml -file=../xv6-riscv/fs.img --h2 ../xv6-riscv/database.mv.db

# Debug

## Virtio IO

--elf a -s 0x1000 --init=quantr.xml -file=fs.img --h2 ./database.mv.db

## XV6

--elf ../xv6-riscv/kernel/kernel -s 0x1000 --init=quantr.xml -file=../xv6-riscv/fs.img --h2 ../xv6-riscv/database.mv.db

--elf src/test/resources/hk/quantr/riscv_simulator/elf/virtio_disk_write/a -s 0x1000 --init=quantr.xml -file=src/test/resources/hk/quantr/riscv_simulator/elf/virtio_disk_write/fs.img


# Notes

# Netbeans run profile 1

Argument: --elf ../xv6-riscv/kernel/kernel -s 0x1000 --init=quantr.xml -file=../xv6-riscv/fs.img --h2 ../xv6-riscv/database.mv.db

Working Direcotry: <empty>

# Netbeans run profile 2

Argument: --elf a -s 0x1000 --init=quantr.xml -file=fs.img --h2 ./database.mv.db

Working Directory: /Users/peter/workspace/riscv-simulator/src/test/resources/hk/quantr/riscv_simulator/elf/virtio_disk_read


# Docker

## Build

docker image build -t riscv-simulator .

docker image tag riscv-assembler quantrpeter/riscv-simulator:1.0

docker image push quantrpeter/riscv-simulator:1.0

## Run

```
docker run --mount type=bind,source=$(pwd)/quantr.xml,target=/app/quantr.xml \
		   --mount type=bind,source=$(pwd)/../xv6-riscv,target=/app/xv6-riscv \
		   quantrpeter/riscv-simulator:1.0 --elf xv6-riscv/kernel/kernel -s 0x1000 --init=quantr.xml -file=xv6-riscv/fs.img --h2 xv6-riscv/database.mv.db
```

