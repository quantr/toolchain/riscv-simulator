@echo off
SET /P AREYOUSURE=Burn the program (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END
	openocd -f openocd_gdlink.cfg -c "program firmware.elf verify reset exit"
:END


start putty -telnet localhost 4444
openocd -f openocd_gdlink.cfg
